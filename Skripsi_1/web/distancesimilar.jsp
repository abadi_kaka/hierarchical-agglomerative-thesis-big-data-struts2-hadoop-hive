<%-- 
    Document   : main
    Created on : Apr 22, 2016, 2:39:06 AM
    Author     : hadoop
--%>

<%@page import="models.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <head>
        <title>Dashboard</title>
        <style>

            .node circle {
              fill: #fff;
              stroke: steelblue;
              stroke-width: 3px;
            }

            .node text { font: 12px sans-serif; }
            
            .node {
                font: 10px sans-serif;
            }
            .link {
              fill: none;
              stroke: #ccc;
              stroke-width: 2px;
            }
            
            .continuous {
                fill: none;
                stroke: black;
                stroke-width: 1.5px;
            }


        </style>
        <%@ include file="header.jsp" %>
    </head>
     
    <body>
        <%@ include file="body.jsp" %>
        
        <div class="ui grid height-100 no-margin padt-50">
            <div class="two wide column height-100 no-padding">
                <div class="ui left fixed inverted vertical pointing accordion menu inline-block min-width-150">
                    <a class="item" href="index.action">Input Data</a>
                    <a class="item" href="normal.action">Normalisasi</a>
                    <a class="item" href="pivot.action">Pivoting</a>
                    <a class="item active" href="similar.action">Similarity</a>
                    <a class="item" href="single.action">Single Linkage</a>
                    <a class="item" href="complete.action">Complete Linkage</a>
                    <a class="item" href="manual.action">Manual Cluster</a>
                    
                    <div class="ui dropdown item">
                        <i class="dropdown icon"></i>
                        Lihat Data
                        <div class="menu">
                            <a class="item" href="datapelanggan.action"><i class="edit icon"></i> Data Pelanggan</a>
                            <a class="item" href="databon.action"><i class="globe icon"></i> Data Bon</a>
                            <a class="item" href="datatunai.action"><i class="settings icon"></i> Data Tunai</a>
                            <a class="item" href="datastrip.action"><i class="settings icon"></i> Data Strip</a>                        
                        </div>
                    </div>

                    <a class="item" href="manajemen.action">Manajemen User</a>
                </div>
            </div>
            <div class="fourteen wide column zindex-1" style="z-index: 10">
                <div class="ui main">
                    <div class="one column">
                        <div class="column">
                            <h1>Similarity</h1>
                        </div>
                    </div>
                    <div class="ui divider"></div>
                    <div class="seven column stackable ui grid">
                    <form class="ui form" action="pageSimilarity">
                        <br>
                        <div class="four column ui grid">
                            <div class="column">
                                <select class="ui dropdown" id="periodselect" name="periodselect" >
                                    <option value="">Pilih Period</option>
                                    <s:iterator value="selectDataPivoting">
                                        <option value="<s:property value="id"></s:property>"><s:property value="nama"></s:property></option>    
                                    </s:iterator>
                                </select>
                            </div>
                            
                            
                            <div class="column" style="margin-left:60px">
                                <select class="ui dropdown" id="metodeselect" name="metodeselect" >
                                    <option value="">Pilih Metode</option>
                                    <option value="0">Euclidean</option>
                                    <option value="1">Cosine</option>                                        
                                </select>
                            </div>
                            
                            <div class="column" style="margin-left:50px">
                                    <button type="submit" value="PageSimilarity" class="ui orange button">Proses</button>
                            </div>
                        </div>
                        <br>
                        </form>
                        
                    </div>
                    
                    <div class="ui divider">

                    </div>

                    <div class="one column">
                        <div class="column">
                            <h1>Tabel Similarity</h1>
                        </div>
                    </div>
                    <br>
                    <br>
                    
                    <div class="over">
                    <display:table uid="itemsim" class="ui celled padded table modelsearch" name="distancespace" pagesize="10" requestURI="" export="true">                    
                    <display:setProperty name="basic.msg.empty_list" 
                    value='<table class="ui celled padded table" id="table-fact">
                        <thead>
                          <tr>
                          <th>No Bon</th>
                          <th>No Bon 1</th>
                          <th>No Bon 2</th>
                        </tr></thead>
                        <tbody id="fact-table">
                            <tr>
                                <td class="left aligned" colspan="7">No data to display in the table </td>
                                
                            </tr>
                        </tbody>
                    </table>' />
                    <display:column property="no_bon" title="No Bon" sortable="true"/>
                    <c:forEach var="testsim" items="${itemsim.distancebon}" varStatus="index">
                        <display:column title="${testsim.no_bon}" value="${testsim.jumlahdistance}" sortable="true">                            
                        </display:column>
                    </c:forEach>
                        
                    <display:setProperty name="export.pdf.filename" value="TableSimilar.pdf"/>
                    <display:setProperty name="export.excel.filename" value="TableSimilar.xls"/>
                    <display:setProperty name="export.pdf" value="true" />
                    <display:setProperty name="export.csv.filename" value="TableSimilar.csv"/>
                    <display:setProperty name="export.xml.filename" value="TableSimilar.xml"/>
                    
                    </display:table>
                    </div>
                    
                    <div class="ui divider">

                    </div>
                    
<!--                    <applet code="com.dendogram.MyApplet" archive="MyDendogram.jar" width="1000" height="2000"/>-->
<!--                    <img src="displayDendo"/>-->


                    <div class="ui small modal">
                        <div class="header">Tambah Periode Baru</div>
                
                        <div class="content">
                           <form class="ui form" action="addPeriod">
                            <div class="ui grid">
                                <div class="column">
                                    <div class="ui fluid input">
                                        <input type="text" name="nama" placeholder="Period Name">
                                    </div>
                                </div>
                            </div>
                            <div class="two column ui grid">
                                <div class="column">
                                    <div class="ui calendar" id="start-date">
                                        <div class="ui fluid input left icon">
                                            <i class="calendar icon"></i>
                                            <input type="text" name="start" placeholder="Date Start">
                                        </div>
                                    </div>
                                    
<!--                                    <div class="column">
                                        <div class="ui fluid input">
                                            <input type="text" name="start" placeholder="Period Name">
                                        </div>
                                    </div>-->
                                </div>
                                <div class="column">
                                    <div class="ui calendar" id="end-date">
                                        <div class="ui fluid input left icon">
                                            <i class="calendar icon"></i>
                                            <input type="text" name="end" placeholder="Date End">
                                        </div>
                                    </div>
                                    
<!--                                    <div class="column">
                                        <div class="ui fluid input">
                                            <input type="text" name="end" placeholder="Period Name">
                                        </div>
                                    </div>-->
                                </div>
                            </div>  
                            <div class="one column ui grid">
                                <div class="column">
                                    <select class="ui dropdown" id="jenis_kelamin" name="jenis_kelamin">
                                        <option value="">Jenis Kelamin</option>
                                        <option value="A">All</option>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="ui grid">
                                <div class="two wide column right">
                                    <button type="submit" value="AddPeriod" class="ui button primary" id="addPeriod">Save</button>
                                </div>

                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        <%@ include file="footer.jsp" %>
    </body>
</html>