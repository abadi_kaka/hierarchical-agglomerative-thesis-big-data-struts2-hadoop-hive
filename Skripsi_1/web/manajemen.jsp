<%-- 
    Document   : manajemen
    Created on : Jul 26, 2016, 10:36:05 PM
    Author     : hadoop
--%>


<%@page import="models.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <head>
        <title>Aggloromerative</title>
        <style>

            .node circle {
              fill: #fff;
              stroke: steelblue;
              stroke-width: 3px;
            }

            .node text { font: 12px sans-serif; }
            
            .node {
                font: 10px sans-serif;
            }
            .link {
              fill: none;
              stroke: #ccc;
              stroke-width: 2px;
            }
            
            .continuous {
                fill: none;
                stroke: black;
                stroke-width: 1.5px;
            }

/*            .node {
                cursor: pointer;
              }

              .overlay{
                  background-color:#EEE;
              }

              .node circle {
                fill: #fff;
                stroke: steelblue;
                stroke-width: 1.5px;
              }

              .node text {
                font-size:10px; 
                font-family:sans-serif;
              }

              .link {
                fill: none;
                stroke: #ccc;
                stroke-width: 1.5px;
              }

              .templink {
                fill: none;
                stroke: red;
                stroke-width: 3px;
              }

              .ghostCircle.show{
                  display:block;
              }

              .ghostCircle, .activeDrag .ghostCircle{
                   display: none;
              }*/

              #loadingNotif{
                    height:100px;
                    width:100%;
                    background-color:#CC0033;
                    color:white;
                    text-align:center;
                    font-size:30pt;
              }
        </style>
        <%@ include file="header.jsp" %>
    </head>
     
    <body>
        <%@ include file="body.jsp" %>
        
        <div class="ui grid height-100 no-margin padt-50" >
            <div class="two wide column height-100 no-padding" id="ignorePDF">
                <div class="ui left fixed inverted vertical pointing accordion menu inline-block min-width-150">
                    <a class="item" href="index.action">Input Data</a>
                    <a class="item" href="normal.action">Normalisasi</a>
                    <a class="item" href="pivot.action">Pivoting</a>
                    <a class="item" href="similar.action">Similarity</a>
                    <a class="item" href="single.action">Single Linkage</a>
                    <a class="item" href="complete.action">Complete Linkage</a>
                    <a class="item" href="manual.action">Manual Cluster</a>
                    
                    <div class="ui dropdown item">
                        <i class="dropdown icon"></i>
                        Lihat Data
                        <div class="menu">
                            <a class="item" href="datapelanggan.action"><i class="edit icon"></i> Data Pelanggan</a>
                            <a class="item" href="databon.action"><i class="globe icon"></i> Data Bon</a>
                            <a class="item" href="datatunai.action"><i class="settings icon"></i> Data Tunai</a>
                            <a class="item" href="datastrip.action"><i class="settings icon"></i> Data Strip</a>                        
                        </div>
                    </div>

                    <a class="item active" href="manajemen.action">Manajemen User</a>
                </div>
            </div>
            <div class="fourteen wide column zindex-1" style="z-index: 10">
                <div class="ui main">
                    <div class="one column" >
                        <div class="column">
                            <h1>Manajemen User</h1>
                        </div>
                    </div>
                    
                    <div class="ui divider">

                    </div>
                    <table class="ui celled padded table" id="table-fact">
                        <thead>
                          <tr>
                            <th class="left aligned">Username</th>
                            <th>Role</th>
                            <th>Action</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="fact-table">                            
                            <s:iterator value="selectDataUser">
                                <form class="ui form" action="deleteUser">
                                <tr>
                                    <input type="hidden" name="idUsr" class="idUsr" value="<s:property value="userId"></s:property>">
                                    <td class="left aligned"><input type="text" name="username" class="username" value="<s:property value="username"></s:property>"  readonly style="cursor:default;border:none"></td>
                                    <td class="left aligned"><input type="text" name="roleUser" class="roleUser" value="<s:property value="role"></s:property>" readonly style="cursor:default;border:none"></td>
                                    <td class="left aligned"><button class="ui primary button edituser">Edit</button></td>
                                    <td class="left aligned"><button class="ui red button deleteuser">Delete</button></td>
                                </tr>    
                                </form>
                            </s:iterator>
                        </tbody>
                    </table>
                    
                    </div>

                    
                    <div class="ui divider">

                    </div>
                    
                    <div class="one column" >
                        <div class="column" style="margin-left:57px">
                            <button class="ui primary button tambahuser">Add New</button>
                        </div>
                    </div>
                    
                    <div class="ui modal edit">
                        <div class="header">Edit User</div>
                        <div class="content">
                           <form class="ui form" action="editUser">
                            <div class="ui grid">
                                <div class="column">
                                    <div class="ui fluid input">
                                        <input type="text" name="userEdit" placeholder="Username" id="userEdit">
                                        <input type="hidden" name="userId" id="userId">
                                    </div>
                                </div>
                            </div>  
                            <div class="one column ui grid">
                                <div class="column">
                                    <select class="ui dropdown" id="jenis_role" name="jenis_role">
                                        <option value="A" id="sel1">Admin</option>
                                        <option value="U" id="sel2">User</option>
                                    </select>
                                </div>
                            </div>  
                            <div class="ui grid">
                                <div class="column">
                                    <div class="ui fluid input">
                                        <input type="password" name="passwordEdit" id="passwordEdit" placeholder="New Password">
                                    </div>
                                </div>
                            </div>  
                            <div class="ui grid">
                                <div class="two wide column right">
                                    <button type="submit" value="EditUser" class="ui button primary" id="editUser">Save</button>
                                </div>

                            </div>
                        </form>
                        </div>
                    </div>
                
                    <div class="ui modal tambah">
                        <div class="header">Tambah User</div>
                        <div class="content">
                           <form class="ui form" action="tambahUser">
                            <div class="ui grid">
                                <div class="column">
                                    <div class="ui fluid input">
                                        <input type="text" name="username" placeholder="Username" id="username">
                                    </div>
                                </div>
                            </div>  
                            <div class="one column ui grid">
                                <div class="column">
                                    <select class="ui dropdown" id="jenis_role_tambah" name="jenis_role_tambah">
                                        <option value="A">Admin</option>
                                        <option value="U">User</option>
                                    </select>
                                </div>
                            </div>  
                            <div class="ui grid">
                                <div class="column">
                                    <div class="ui fluid input">
                                        <input type="password" name="password" id="password" placeholder="New Password">
                                    </div>
                                </div>
                            </div>  
                            <div class="ui grid">
                                <div class="two wide column right">
                                    <button type="submit" value="TambahUser" class="ui button primary" id="editUser">Save</button>
                                </div>

                            </div>
                        </form>
                        </div>
                    </div>
                        
                </div>
            </div>
        <%@ include file="footer.jsp" %>
    </body>
</html>