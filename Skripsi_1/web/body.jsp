<%-- 
    Document   : body
    Created on : Apr 26, 2016, 3:30:00 AM
    Author     : hadoop
--%>

<%@page import="models.User"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<div class="ui menu inverted" style="z-index: 1000">
            <div class="ui header item">
                Hierarchical Agglomerative Clustering
            </div>
            <div class="right menu">
                <div class="item">
                    <div class="ui transparent inverted icon input">
                        <i class="search icon"></i>
                        <input type="text" placeholder="Search">
                    </div>
                </div>
                <div class="ui dropdown item" tabindex="0">
                    <i class="user icon"></i>
                    <div class="ui label">
                        <s:property value="#session['USER']['username']" />
                    </div>
                    <i class="dropdown icon"></i>
                    <div class="menu transition hidden" tabindex="-">
                        <a class="item" href="logOut.action"> Logout</a>
                    </div>
                </div>
            </div>
        </div>

        