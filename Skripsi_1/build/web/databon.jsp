<%-- 
    Document   : main
    Created on : Apr 22, 2016, 2:39:06 AM
    Author     : hadoop
--%>

<%@page import="models.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <head>
        <title>Data Bon</title>

        <%@ include file="header.jsp" %>
    </head>
     
    <body>
        <%@ include file="body.jsp" %>
        
        <div class="ui grid height-100 no-margin padt-50">
            <div class="two wide column height-100 no-padding">
                <div class="ui left fixed inverted vertical pointing accordion menu inline-block min-width-150">
                    <a class="item" href="index.action">Input Data</a>
                    <a class="item" href="normal.action">Normalisasi</a>
                    <a class="item" href="pivot.action">Pivoting</a>
                    <a class="item" href="similar.action">Similarity</a>
                    <a class="item" href="single.action">Single Linkage</a>
                    <a class="item" href="complete.action">Complete Linkage</a>
                    <a class="item" href="manual.action">Manual Cluster</a>                    
                    <div class="ui dropdown item">
                        <i class="dropdown icon"></i>
                        Lihat Data
                        <div class="menu">
                            <a class="item" href="datapelanggan.action"><i class="edit icon"></i> Data Pelanggan</a>
                            <a class="item active" href="databon.action"><i class="globe icon"></i> Data Bon</a>
                            <a class="item" href="datatunai.action"><i class="settings icon"></i> Data Tunai</a>
                            <a class="item" href="datastrip.action"><i class="settings icon"></i> Data Strip</a>
                        </div>
                    </div>

                    <a class="item" href="manajemen.action">Manajemen User</a>
                </div>
            </div>
            <div class="fourteen wide column zindex-1" style="z-index: 10">
                <div class="ui main">
                        
                    <div class="ui divider">

                    </div>
                        
                    <div class="one column">
                        <div class="column">
                            <h1>Data Bon</h1>
                        </div>
                    </div>
                    <br>
                    <br>
                    
                    <display:table id="table-normal" class="ui celled padded table modelsearch" name="infonotabon" pagesize="10" requestURI="" export="true">
                    <display:setProperty name="basic.msg.empty_list" 
                    value='<table class="ui celled padded table" id="table-fact">
                        <thead>
                          <tr>
                          <th class="left aligned">Tanggal</th>
                          <th>No Bon</th>
                          <th>Kd_Customer</th>
                          <th>Umur</th>
                          <th>Id_toko</th>
                          <th>kd_strip</th>
                          <th>jumlah</th>
                        </tr></thead>
                        <tbody id="fact-table">
                            <tr>
                                <td class="left aligned" colspan="7">No data to display in the table </td>
                                
                            </tr>
                        </tbody>
                    </table>' />
                    <!--display:setProperty name="paging.banner.some_items_found" value="" /-->
                    <!--display:setProperty name="paging.banner.placement" value="bottom" /-->
                    <display:column property="tanggal" title="Tanggal" sortable="true"/>
                    <display:column property="no_bon" title="No Bon" sortable="true"/>
                    <display:column property="kd_customer" title="Kd Customer" sortable="true"/>
                    <display:column property="umur" title="Umur" sortable="true"/>
                    <display:column property="kd_lokasi" title="Id Toko" sortable="true"/>
                    <display:column property="kel_jenis" title="Kel Jenis" sortable="true"/>
                    <display:column property="jumlah" title="Jumlah" sortable="true"/>
                    <display:setProperty name="export.pdf.filename" value="TableNotaBon.pdf"/>
                    <display:setProperty name="export.excel.filename" value="TableNotaBon.xls"/>
                    <display:setProperty name="export.pdf" value="true" />
                    <display:setProperty name="export.csv.filename" value="TableNotaBon.csv"/>
                    <display:setProperty name="export.xml.filename" value="TableNotaBon.xml"/>
                    
                    </display:table>

                    <div class="ui divider">

                    </div>

                    <div class="ui small modal">
                        <div class="header">Tambah Periode Baru</div>
                
                        <div class="content">
                           <form class="ui form" action="addPeriod">
                            <div class="ui grid">
                                <div class="column">
                                    <div class="ui fluid input">
                                        <input type="text" name="nama" placeholder="Period Name">
                                    </div>
                                </div>
                            </div>
                            <div class="two column ui grid">
                                <div class="column">
                                    <div class="ui calendar" id="start-date">
                                        <div class="ui fluid input left icon">
                                            <i class="calendar icon"></i>
                                            <input type="text" name="start" placeholder="Date Start">
                                        </div>
                                    </div>
                                    
<!--                                    <div class="column">
                                        <div class="ui fluid input">
                                            <input type="text" name="start" placeholder="Period Name">
                                        </div>
                                    </div>-->
                                </div>
                                <div class="column">
                                    <div class="ui calendar" id="end-date">
                                        <div class="ui fluid input left icon">
                                            <i class="calendar icon"></i>
                                            <input type="text" name="end" placeholder="Date End">
                                        </div>
                                    </div>
                                    
<!--                                    <div class="column">
                                        <div class="ui fluid input">
                                            <input type="text" name="end" placeholder="Period Name">
                                        </div>
                                    </div>-->
                                </div>
                            </div>  
                            <div class="one column ui grid">
                                <div class="column">
                                    <select class="ui dropdown" id="jenis_kelamin" name="jenis_kelamin">
                                        <option value="">Jenis Kelamin</option>
                                        <option value="A">All</option>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="ui grid">
                                <div class="two wide column right">
                                    <button type="submit" value="AddPeriod" class="ui button primary" id="addPeriod">Save</button>
                                </div>

                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        <%@ include file="footer.jsp" %>
    </body>
</html>