<%-- 
    Document   : login
    Created on : Apr 16, 2016, 9:24:05 PM
    Author     : hadoop
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        
<!--        <link href="dist/components/grid.css" rel="stylesheet">-->
        <link href="dist/semantic.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <script src="js/jquery-2.2.2.js" rel="stylesheet"></script>
        <script src="dist/semantic.js" rel="stylesheet"></script>
        <script src="js/javascript.js" rel="stylesheet"></script>
        
    <body>
        <div class="ui one column center aligned grid">
            <div class="ui one column center aligned grid">
              <div class="column six wide form-holder">
                <h2 class="center aligned header form-head">Sign in</h2>
                <form class="ui form" action="logIn">
                    
                  <div class="field">
                    <input type="text" name="user" placeholder="username">
                  </div>
                  <div class="field">
                      <input type="password" name="password" placeholder="password">
                  </div>
                  <div class="field">
                    <input type="submit" value="Login" class="ui button large fluid green">
                  </div>
                  <div class="inline field">
<!--                    <div class="ui checkbox">
                      <input type="checkbox">
                      <label>Remember me</label>
                    </div>-->
                  </div>
                </form>
              </div>
            </div>
        </div>
    </body>
</html>
