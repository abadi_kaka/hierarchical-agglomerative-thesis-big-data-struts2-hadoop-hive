<%-- 
    Document   : dendrogramawal
    Created on : Jul 26, 2016, 10:36:05 PM
    Author     : hadoop
--%>


<%@page import="models.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <head>
        <title>Aggloromerative</title>
        <style>

            .node circle {
              fill: #fff;
              stroke: steelblue;
              stroke-width: 3px;
            }

            .node text { font: 12px sans-serif; }
            
            .node {
                font: 10px sans-serif;
            }
            .link {
              fill: none;
              stroke: #ccc;
              stroke-width: 2px;
            }
            
            .continuous {
                fill: none;
                stroke: black;
                stroke-width: 1.5px;
            }

/*            .node {
                cursor: pointer;
              }

              .overlay{
                  background-color:#EEE;
              }

              .node circle {
                fill: #fff;
                stroke: steelblue;
                stroke-width: 1.5px;
              }

              .node text {
                font-size:10px; 
                font-family:sans-serif;
              }

              .link {
                fill: none;
                stroke: #ccc;
                stroke-width: 1.5px;
              }

              .templink {
                fill: none;
                stroke: red;
                stroke-width: 3px;
              }

              .ghostCircle.show{
                  display:block;
              }

              .ghostCircle, .activeDrag .ghostCircle{
                   display: none;
              }*/

              #loadingNotif{
                    height:100px;
                    width:100%;
                    background-color:#CC0033;
                    color:white;
                    text-align:center;
                    font-size:30pt;
              }
        </style>
        <%@ include file="header.jsp" %>
    </head>
     
    <body>
        <%@ include file="body.jsp" %>
        
        <div class="ui grid height-100 no-margin padt-50" >
            <div class="two wide column height-100 no-padding" id="ignorePDF">
                <div class="ui left fixed inverted vertical pointing accordion menu inline-block min-width-150">
                    <a class="item" href="index.action">Input Data</a>
                    <a class="item" href="normal.action">Normalisasi</a>
                    <a class="item" href="pivot.action">Pivoting</a>
                    <a class="item" href="similar.action">Similarity</a>
                    <a class="item" href="single.action">Single Linkage</a>
                    <a class="item active" href="complete.action">Complete Linkage</a>
                    <a class="item" href="manual.action">Manual Cluster</a>
                    
                    <div class="ui dropdown item">
                        <i class="dropdown icon"></i>
                        Lihat Data
                        <div class="menu">
                            <a class="item" href="datapelanggan.action"><i class="edit icon"></i> Data Pelanggan</a>
                            <a class="item" href="databon.action"><i class="globe icon"></i> Data Bon</a>
                            <a class="item" href="datatunai.action"><i class="settings icon"></i> Data Tunai</a>
                            <a class="item" href="datastrip.action"><i class="settings icon"></i> Data Strip</a>                        
                        </div>
                    </div>


                    <a class="item" href="manajemen.action">Manajemen User</a>
                </div>
            </div>
            <div class="fourteen wide column zindex-1" style="z-index: 10">
                <div class="ui main">
                    <div class="one column" id="ignorePDF1" >
                        <div class="column">
                            <h1>Hirarkikal</h1>
                        </div>
                    </div>
                    <div class="ui divider"></div>
                    <div class="seven column stackable ui grid" id="ignorePDF2">
                    <form class="ui form ignorePDF" action="pageComplete">
                        <br>
                        <div class="three column ui grid">
                            <div class="column">
                                <select class="ui dropdown" id="periodselect" name="periodselect" >
                                    <option value="">Pilih Data</option>
                                    <s:iterator value="selectDataHirarki">
                                        <option value="<s:property value="id"></s:property>"><s:property value="nama"></s:property></option>    
                                    </s:iterator>
                                </select>
                            </div>
                            <div class="column" style="margin-top:0px; margin-left:80px;">
                                    <button type="submit" value="PageComplete" class="ui orange button">Proses</button>
                            </div>

                        </div>
                        <br>
                        </form>
                        
                        <div class="column" style="margin-top:6px; margin-left:-100px;">
                            <a href="<s:url action="dendrogram"/>" class="ui black button" id="dendrogram">Dendrogram</a>
                        </div>
                        
<!--                        <div class="column" style="margin-top:6px; margin-left:-10px;">
                            <a href="#" class="ui black button" id="pdf">Print PDF</a>
                        </div>-->
<!--                        <div class="column" style="margin-top:6px; margin-left:-10px;">
                            <a href="#" class="ui green button" id="pdfdend">PDF Dendrogram</a>
                        </div>-->
                   
                    </div>

                    
                    <div class="ui divider">

                    </div>
                        
                    <div class="ui divider">

                    </div>
                        
                    <table class="ui celled padded table" id="table-single">
                        <thead>
                          <tr>
                          <th class="left aligned">Jarak</th>
                          <th>Items</th>
                          </tr>
                        </thead>
                        <tbody id="fact-table">
                            <tr id="rowhapus">
                                <td class="left aligned" colspan="7">No data to display in the table </td>
                            </tr>
                        </tbody>
                    </table>
                        
                </div>
            </div>
        <%@ include file="footer.jsp" %>
    </body>
</html>