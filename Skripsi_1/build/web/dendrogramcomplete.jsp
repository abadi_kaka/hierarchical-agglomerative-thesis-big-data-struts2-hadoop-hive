<%-- 
    Document   : main
    Created on : Apr 22, 2016, 2:39:06 AM
    Author     : hadoop
--%>

<%@page import="models.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <head>
        <title>Aggloromerative</title>
        <style>

            .node circle {
              fill: #fff;
              stroke: steelblue;
              stroke-width: 3px;
            }

            .node text { font: 12px sans-serif; }
            
            .node {
                font: 10px sans-serif;
            }
            .link {
              fill: none;
              stroke: #ccc;
              stroke-width: 2px;
            }
            
            .continuous {
                fill: none;
                stroke: black;
                stroke-width: 1.5px;
            }

/*            .node {
                cursor: pointer;
              }

              .overlay{
                  background-color:#EEE;
              }

              .node circle {
                fill: #fff;
                stroke: steelblue;
                stroke-width: 1.5px;
              }

              .node text {
                font-size:10px; 
                font-family:sans-serif;
              }

              .link {
                fill: none;
                stroke: #ccc;
                stroke-width: 1.5px;
              }

              .templink {
                fill: none;
                stroke: red;
                stroke-width: 3px;
              }

              .ghostCircle.show{
                  display:block;
              }

              .ghostCircle, .activeDrag .ghostCircle{
                   display: none;
              }*/

              #loadingNotif{
                    height:100px;
                    width:100%;
                    background-color:#CC0033;
                    color:white;
                    text-align:center;
                    font-size:30pt;
              }
              
               #loader {
                    width: 220px;
                    height: 80px;
                    position: fixed;
                    top: 50%;
                    left: 50%;
                    z-index: -1;
                    opacity: 0;
                    background: url(css/images/default.gif) no-repeat center center;
                    transition: all .5s ease-in-out;
                    margin: -40px 0 0 -110px;
                }

                #loader img {position: relative; top: 50%; margin-top: -30px; left: 10px;}

                .loading #loader {z-index: 1000; opacity: 1.0}
        </style>
        <%@ include file="header.jsp" %>
    </head>
     
    <body>
        <%@ include file="body.jsp" %>
        <div id="loader"><img src="/css/images/default.gif" /></div>
        <div class="ui grid height-100 no-margin padt-50" >
            <div class="two wide column height-100 no-padding" id="ignorePDF">
                <div class="ui left fixed inverted vertical pointing accordion menu inline-block min-width-150">
                    <a class="item" href="index.action">Input Data</a>
                    <a class="item" href="normal.action">Normalisasi</a>
                    <a class="item" href="pivot.action">Pivoting</a>
                    <a class="item" href="similar.action">Similarity</a>
                    <a class="item" href="single.action">Single Linkage</a>
                    <a class="item active" href="complete.action">Complete Linkage</a>
                    <a class="item" href="manual.action">Manual Cluster</a>
                    
                    <div class="ui dropdown item">
                        <i class="dropdown icon"></i>
                        Lihat Data
                        <div class="menu">
                            <a class="item" href="datapelanggan.action"><i class="edit icon"></i> Data Pelanggan</a>
                            <a class="item" href="databon.action"><i class="globe icon"></i> Data Bon</a>
                            <a class="item" href="datatunai.action"><i class="settings icon"></i> Data Tunai</a>
                            <a class="item" href="datastrip.action"><i class="settings icon"></i> Data Strip</a>                        
                        </div>
                    </div>

                    <a class="item" href="manajemen.action">Manajemen User</a>
                </div>
            </div>
            <div class="fourteen wide column zindex-1" style="z-index: 10">
                <div class="ui main">
                    <div class="one column" id="ignorePDF1" >
                        <div class="column">
                            <h1>Hirarkikal</h1>
                        </div>
                    </div>
                    <div class="ui divider"></div>
                    <div class="seven column stackable ui grid" id="ignorePDF2">
                    <form class="ui form ignorePDF" action="pageComplete">
                        <br>
                        <div class="three column ui grid">
                            <div class="column">
                                <select class="ui dropdown" id="periodselect" name="periodselect" >
                                    <option value="">Pilih Data</option>
                                    <s:iterator value="selectDataHirarki">
                                        <option value="<s:property value="id"></s:property>"><s:property value="nama"></s:property></option>    
                                    </s:iterator>
                                </select>
                            </div>
                            <div class="column" style="margin-top:0px; margin-left:80px;">
                                    <button type="submit" value="PageComplete" class="ui orange button">Proses</button>
                            </div>

                        </div>
                        <br>
                        </form>
                        
                        <div class="column" style="margin-top:6px; margin-left:-100px;">
                            <a href="<s:url action="dendrogram"/>" class="ui black button" id="dendrogram">Dendrogram</a>
                        </div>
                        
                        <div class="column" style="margin-top:6px; margin-left:-10px;">
                            <a href="<s:url action="grafik"/>" class="ui green button" id="grafik">Grafik</a>
                        </div>
                        <div class="column" style="margin-top:6px; margin-left:-10px;">                        
                            <select class="ui dropdown" id="potong" name="potong" >
                                    <option value="">Pilih Potong</option>
                                    <option value="0">12</option>
                                    <option value="1">16</option>                                        
                                </select>
                        </div>
<!--                        <div class="column" style="margin-top:6px; margin-left:-10px;">
                            <a href="#" class="ui black button" id="pdf">Print PDF</a>
                        </div>-->
<!--                        <div class="column" style="margin-top:6px; margin-left:-10px;">
                            <a href="#" class="ui green button" id="pdfdend">PDF Dendrogram</a>
                        </div>-->
                   
                    </div>

                    
                    <div class="ui divider">

                    </div>
                    <div id="loadingNotif">
                        <div class="ui positive message">
                            <i class="close icon"></i>
                            <div class="header">
                              PROSES KLASTERISASI BERHASIL
                            </div>
                            <p>Tekan tombol <b>dendrogram</b> untuk melihat dendrogram.</p>
                        </div>
                    </div>
                        
                        
                    <div class="ui divider">

                    </div>
                    
                    <div id="tambahtabel1">
                        
                    </div>
                    
                        
                    
                    <div class="ui divider">

                    </div>
                    
                    
                    <div id="tambahtabel2">
                        
                    </div>    
                                            
                    <div class="ui divider">

                    </div>
                        
                    <div id="tambahdendo">
                        
                    </div>
                    
                    <div class="ui divider">

                    </div>
                    <div id="tambahgrafik">
                        
                    </div>
                        
                    <div class="ui divider">

                    </div>
                        
                    <div id="grafikcluster">
                        
                    </div>
                    
                    <br>

                </div>
            </div>
        <%@ include file="footer.jsp" %>
    </body>
</html>