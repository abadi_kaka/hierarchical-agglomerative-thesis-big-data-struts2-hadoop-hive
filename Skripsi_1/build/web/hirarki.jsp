<%-- 
    Document   : hirarki
    Created on : Apr 28, 2016, 2:38:23 AM
    Author     : hadoop
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Hirarki</title>
        <%@ include file="header.jsp" %>
    </head>
    
    <body>
        <%@ include file="body.jsp" %>
        <div class="ui grid height-100 no-margin padt-50">
            <div class="two wide column height-100 no-padding">
                <div class="ui left fixed inverted vertical pointing accordion menu inline-block min-width-150">
                    <a class="item" href="index.action">Input Data</a>
                    <a class="item" href="normalisasi.action">Normalisasi</a>
                    
                    <div class="ui dropdown item">
                        <i class="dropdown icon"></i>
                        Lihat Data
                        <div class="menu">
                            <a class="item" href="datapelanggan.action"><i class="edit icon"></i> Data Pelanggan</a>
                            <a class="item" href="databon.action"><i class="globe icon"></i> Data Bon</a>
                            <a class="item" href="datatunai.action"><i class="settings icon"></i> Data Tunai</a>
                            <a class="item" href="datastrip.action"><i class="settings icon"></i> Data Strip</a>                        
                        </div>
                    </div>

                    <a class="item" href="pivot.action">Pivoting Table</a>
                    <a class="item" href="distance.action">Distance Space</a>
                    <a class="item active" href="hirarki.action">Hierarchical</a>
                    <a class="item" href="dendogram.action">Dendogram</a>
                    <a class="item" href="scatter.action">Scatter Plot</a>
                    
                    <div class="ui dropdown item">
                        <i class="dropdown icon"></i>
                        More
                        <div class="menu">
                            <a class="item"><i class="edit icon"></i> Edit Profile</a>
                            <a class="item"><i class="globe icon"></i> Choose Language</a>
                            <a class="item"><i class="settings icon"></i> Account Settings</a>
                        </div>
                    </div>
                </div>
            </div>
                    
            <div class="fourteen wide column zindex-1" style="z-index: 10">
                <div class="ui main">
                    <div class="one column">
                        <div class="column">
                            <h1>Hirarki</h1>
                        </div>
                    </div>
                    <div class="ui divider">
                        
                    </div>
                    <div class="three column stackable ui grid">
                        <div class="column">
                                  <select class="ui fluid dropdown">
                                    <option value="">Metode</option>
                                    <option value="SL">Single Linkage</option>
                                    <option value="CL">Complete Linkage</option>
                                  </select>
                        </div>
                        <div class="column">
                            <button class="ui orange button">Mulai Pivoting</button>
                        </div>
                    </div>

                </div>

                <div class="ui main">
                
                    <div class="one column">
                        <div class="column">
                            <h1>Tabel Pivoting</h1>
                        </div>
                    </div>
                    <div class="ui divider">

                    </div>
                    <table class="ui definition table">
                        <thead>
                        <tr><th></th>
                          <th>Umur</th>
                          <th>AA</th>
                          <th>AB</th>
                          <th>BB</th>                          
                          <th>CC</th>
                        </tr></thead>
                        <tbody>
                          <tr>
                            <td>012100201</td>
                            <td>20</td>
                            <td>5</td>
                            <td>5</td>                            
                            <td>5</td>
                            <td>5</td>
                          </tr>
                          <tr>
                            <td>012020202</td>
                            <td>21</td>
                            <td>3</td>
                            <td>5</td>
                            <td>5</td>                            
                            <td>5</td>                       
                          </tr>
                      </tbody>
                    </table>


                    <div class="ui small modal">
                        <div class="header">Tambah Periode Baru</div>
                        <div class="content">
                            <div class="ui grid">
                                <div class="column">
                                    <div class="ui fluid input">
                                        <input type="text" name="name" placeholder="Period Name">
                                    </div>
                                </div>
                            </div>
                            <div class="two column ui grid">
                                <div class="column">
                                    <div class="ui calendar" id="start-date">
                                        <div class="ui fluid input left icon">
                                            <i class="calendar icon"></i>
                                            <input type="text" placeholder="Date Start">
                                        </div>
                                    </div>
                                </div>
                                <div class="column">
                                    <div class="ui calendar" id="end-date">
                                        <div class="ui fluid input left icon">
                                            <i class="calendar icon"></i>
                                            <input type="text" placeholder="Date End">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui grid">
                                <div class="two wide column right">
                                    <button class="ui button primary">Save</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <%@ include file="footer.jsp" %>
    </body>
</html>
