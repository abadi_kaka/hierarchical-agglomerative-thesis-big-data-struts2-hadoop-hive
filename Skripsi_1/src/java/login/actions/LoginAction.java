/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;

/**
 *
 * @author hadoop
 */
import com.opensymphony.xwork2.ActionContext;
import java.util.Map;
 
import org.apache.struts2.interceptor.SessionAware;
 
import models.User;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LoginAction extends ActionSupport implements SessionAware, ModelDriven<User>{
 
    private static final long serialVersionUID = -3369875299120377549L;
 
    @Override
    public String execute(){
        
        System.out.println("inside execute");
        if("admin".equals(user.getUser()) && "admin".equals(user.getPassword())){
            user.setUserName("Michael Abadi");
            sessionAttributes.put("USER", user);
            return SUCCESS;
        }
        return INPUT;
    }
    
    private User user = new User();
    private Map<String, Object> sessionAttributes = null;
 
    @Override
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
    public String logIn() throws SQLException{
        if (user.getUser().length()== 0 | user.getPassword().length()==0){
            return ERROR;
        }else{
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            String sql = "SELECT COUNT(*) as result FROM `users`";
            sql += "where username= ? and password= ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, user.getUser());
            ps.setString(2, user.getPassword());
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                if (rs.getString(1).equals("1")) {
                    Map<String, Object> session = ActionContext.getContext().getSession();
                    user.setUserName(user.getUser());
                    sessionAttributes.put("USER", user);
                    return SUCCESS;
                }
            }
        }
//        if("admin".equals(user.getUser()) && "admin".equals(user.getPassword())){
//            user.setUserName("Michael Abadi");
//            sessionAttributes.put("USER", user);
//            return SUCCESS;
//        }
//        return ERROR;
        return ERROR;
    }
    public String logOut() {
//                sessionAttributes.remove("USER");
//                sessionAttributes.remove("FACT");
//                sessionAttributes.remove("START");
//                sessionAttributes.remove("END");
//                sessionAttributes.remove("JK");
                sessionAttributes.clear();
                addActionMessage("You have been Successfully Logged Out");
                return SUCCESS;
    }
    
    public String checkSession() {
        Map<String, Object> session = ActionContext.getContext().getSession();
        if (session.get("USER")!= null) {
            return ERROR;
        }else{
            return SUCCESS;
        }
    }
    
    

    @Override
    public User getModel() {
        return user;
    }
    
    public Map<String, Object> getSession() {
                return sessionAttributes;
    }

}
