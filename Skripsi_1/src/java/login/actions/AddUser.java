/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;

import static com.opensymphony.xwork2.Action.SUCCESS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author hadoop
 */
public class AddUser {
    private String username;
    private String password;
    private String jenis_role_tambah;
    

    public String addUser() throws SQLException{
        
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            String sql = "INSERT INTO `users` (`username`, `password`, `role`) VALUES (?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, this.getUsername());
            ps.setString(2, this.getPassword());
            if (this.getJenis_role_tambah().equals("A")) {
            ps.setInt(3, 0);
            }else{
            ps.setInt(3, 1);
            }
            
            ResultSet rs = ps.executeQuery();
            return SUCCESS;
        
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the jenis_role_tambah
     */
    public String getJenis_role_tambah() {
        return jenis_role_tambah;
    }

    /**
     * @param jenis_role_tambah the jenis_role_tambah to set
     */
    public void setJenis_role_tambah(String jenis_role_tambah) {
        this.jenis_role_tambah = jenis_role_tambah;
    }

}
