/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;

/**
 *
 * @author hadoop
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Scanner;
 
/**
 *
 * @author faisalkwok
 */
public class DataMining {
     private static int jumkol;
     private static int counterdata;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         
         
        float data[][] = new float[2000][2000];
        
        String s = bacaFile("newthyroid.txt");
         
        String[] has =s.split("\n");
        for(int i=0;i<has.length;i++){
            String nilai[]=has[i].split("\t");
            jumkol=nilai.length;
            for(int j=0;j<nilai.length;j++){
                try{
                        data[i][j]=Float.valueOf(nilai[j]);
                //
                counterdata++;
                    //System.out.println(data[i][j]);
                }catch(NumberFormatException e){
                    System.out.println(e.getMessage());
                }
            }
            //System.out.println(counterdata/jumkol);
        }
        Scanner input = new Scanner( System.in );
        System.out.print( "1. Metode Normalisasi MinMax  " );
        System.out.print( "\n2. Metode Normalisasi Zscore  " ); 
        System.out.print( "\n3. Metode Normalisasi Decimal Scaling " ); 
        System.out.print( "\n4. Metode Normalisasi Sigmoidal " ); 
        System.out.print( "\n5. Metode Normalisasi Softmax " ); 
        System.out.print( "\nMasukan pilihan metode normalisasi : " );    
        int pil = input.nextInt();
        if(pil==1){
        //normalisasi menggunakan metode min-max
        minMax(data);
        }else if(pil==2){
        //normalisasi menggunakan metode Z-score
        zScore(data);
        } else if(pil==3){
        //normalisasi menggunakan metode Decimal Scaling        
          decimalScaling(data);
           }else if(pil==4){
        //normalisasi menggunakan metode sigmoidal
        sigmoidal(data);
         }else if(pil==5){
        //normalisasi menggunakan metode softmax
        softmax(data);
         }
         else{
             System.out.println("pilihan yang anda masukan salah");
         }
         
    }
     public static void tulisFile(String teks, String namaFile) {
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(
                    new FileWriter(namaFile, true)));
            out.println(teks.toString());
            out.close();
        } catch (IOException e) {
            System.out.println("Gagal menulis ke file " + namaFile);
        }
    }
    private static float max(float[][] data,int j) {
        float max = 0;
        int jumdat=counterdata/jumkol;
        for(int i=0;i<jumdat;i++){
             
            if(data[i][j]>max){
                max=data[i][j];
                 
            }
        }
        //System.out.println(max);
        return max;
    }
     
    private static float min(float[][] data,int j){
     float min=1000;
             
            int jumdat=counterdata/jumkol;
            for(int i=0;i<jumdat;i++){
                if(min>data[i][j]){
                min=data[i][j];
                }
                 
        }
         //System.out.println(min);   
        return min;
    }
     
    private static float mean(float[][] data,int j){
     float mean=0;
     int jumdat=counterdata/jumkol;
            for(int i=0;i<jumdat;i++){
                mean=mean+data[i][j];
                }
             mean=mean/jumdat;
             //System.out.println(mean);
    return mean;
   }
    //mencari standard deviasi
    private static float std(float[][] data,int j){
     float std;
     float s2,x=0,x2=0;
     int jumdat=counterdata/jumkol;
            for(int i=0;i<jumdat;i++){
                x=x+data[i][j];
                x2=(float) (x2+Math.pow(data[i][j], 2));
                //System.out.println(x+"\t"+x2);
                }
        //System.out.println(x);
        x=(float) Math.pow(x, 2);
        s2=((jumdat*x2)-x)/(jumdat*(jumdat-1));
        std=(float) Math.sqrt(s2);
    return std;
    }
     
    public static void minMax(float[][] data){
        float[][] newdata=new float[2000][2000];
        String s;
        int j,i,jumdat=counterdata/jumkol;
        int newmax=1;
        int newmin=0;
        for(j=0;j<jumkol;j++){
        float max=max(data,j);
        float min=min(data,j);
        for(i=0;i<jumdat;i++){
             
            newdata[i][j] = ((data[i][j]-min)*(newmax-newmin))/((max-min)+newmin);
            //newdata[i][j] = (int)(newdata[i][j]*100);
        }
      }
        DecimalFormat df = new DecimalFormat("#.##");
        for(j=0;j<jumdat;j++){
            s=""+df.format(newdata[j][0])+"\t\t"+df.format(newdata[j][1])+"\t\t"+df.format(newdata[j][2])+"\t\t"+df.format(newdata[j][3])+"\t\t"+df.format(newdata[j][4])+"\t"+data[j][jumkol-1];
             tulisFile(s,"minMax.txt");
               System.out.println(s);
        }
      }
         
       
     
     
    public static void zScore(float[][] data){
        float[][] newdata=new float[1500][1500];
        String s ;
        int j,i,jumdat=counterdata/jumkol;
        for(j=0;j<jumkol-1;j++){
            float mean=mean(data,j);
            float std=std(data,j);
            for(i=0;i<jumdat;i++){
             newdata[i][j] = (data[i][j]-mean)/std;
            //newdata[i][j] = (int)(newdata[i][j]*100);
        }
      }
    DecimalFormat df = new DecimalFormat("#.##");
        for(j=0;j<jumdat;j++){
            s=""+df.format(newdata[j][0])+"\t\t"+df.format(newdata[j][1])+"\t\t"+df.format(newdata[j][2])+"\t\t"+df.format(newdata[j][3])+"\t\t"+df.format(newdata[j][4])+"\t"+data[j][jumkol-1];
             tulisFile(s,"zscore.txt");
               System.out.println(s);
        }
    }
     
    public static void decimalScaling(float[][] data){
         float[][] newdata=new float[1500][1500];
        String s ;
        int j,i,jumdat=counterdata/jumkol;
         for(j=0;j<jumkol-1;j++){
            for(i=0;i<jumdat;i++){
         newdata[i][j] = (float) (data[i][j] /Math.pow(10, 2));
            }
         }
          
         DecimalFormat df = new DecimalFormat("#.###");
        for(j=0;j<jumdat;j++){
            s=""+df.format(newdata[j][0])+"\t\t"+df.format(newdata[j][1])+"\t\t"+df.format(newdata[j][2])+"\t\t"+df.format(newdata[j][3])+"\t\t"+df.format(newdata[j][4])+"\t"+data[j][jumkol-1];
             tulisFile(s,"Dscaling.txt");
               System.out.println(s);
        }
    }
    public static void sigmoidal(float[][] data){
        float[][] newdata=new float[1500][1500];
        String s ;
        double e;
        int j,i,jumdat=counterdata/jumkol;
        for(j=0;j<jumkol-1;j++){
            float mean=mean(data,j);
            float std=std(data,j);
            for(i=0;i<jumdat;i++){
             newdata[i][j] = (data[i][j]-mean)/std;
             e=Math.exp(2.718281828);
             newdata[i][j] = (float) ((1-Math.exp((-newdata[i][j])))/(1+ Math.exp(-newdata[i][j])));
              
        }
      }
    DecimalFormat df = new DecimalFormat("#.##");
        for(j=0;j<jumdat;j++){
            s=""+df.format(newdata[j][0])+"\t\t"+df.format(newdata[j][1])+"\t\t"+df.format(newdata[j][2])+"\t\t"+df.format(newdata[j][3])+"\t\t"+df.format(newdata[j][4])+"\t"+data[j][jumkol-1];
            tulisFile(s,"sigmoidal.txt");
               System.out.println(s);
        }
         
         
    }
     
    public static void softmax(float[][] data){
        float[][] newdata=new float[1500][1500];
        String s ;
        double transfdata;
        float x;
        int j,i,jumdat=counterdata/jumkol;
        for(j=0;j<jumkol-1;j++){
            float mean=mean(data,j);
            float std=std(data,j);
            for(i=0;i<jumdat;i++){
             //newdata[i][j] = (data[i][j]-mean)/std;
             //respon linier di deviasi standar
             x =10; 
             transfdata = (data[i][j]-mean)/(x*(std/(2*3.14)));
             newdata[i][j] = (float) (1/(1+Math.exp(-transfdata)));
        }
      }
    DecimalFormat df = new DecimalFormat("#.##");
        for(j=0;j<jumdat;j++){
            s=""+df.format(newdata[j][0])+"\t\t"+df.format(newdata[j][1])+"\t\t"+df.format(newdata[j][2])+"\t\t"+df.format(newdata[j][3])+"\t\t"+df.format(newdata[j][4])+"\t"+data[j][jumkol-1];
            tulisFile(s,"softmax.txt");
               System.out.println(s);
        }
         
         
    }
     
    
   
    // Method baca file
    public static String bacaFile(String namaFile) {
        BufferedReader br = null;
        String stringHasil = "";
  
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader(namaFile));
            while ((sCurrentLine = br.readLine()) != null) {
                stringHasil = stringHasil + sCurrentLine + "\n";
            }
  
        } catch (IOException e) {
            System.out.println("Gagal membaca file " + namaFile);
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return stringHasil;
    }    
}