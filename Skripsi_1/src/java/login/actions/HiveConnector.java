/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author michael abadi
 */
public class HiveConnector {

    private Connection conn;
    private String driverName = "org.apache.hive.jdbc.HiveDriver";
    
    public static void main(String [] args){
        try{
            String URL = "jdbc:hive2://localhost:10000/default";
            //String URL = "jdbc:hive2://192.168.1.100:10000/default";
            
            String driverNames = "org.apache.hive.jdbc.HiveDriver";
            Class.forName(driverNames);
            Connection conn = DriverManager.getConnection(URL,"hive","");
            //String sql = "SELECT * from amigo.pelanggan limit 10";
            String sql = "SELECT * from abadi.pelanggan limit 10";
            System.out.println(sql);
            Statement stmt = conn.createStatement();
            ResultSet res = stmt.executeQuery(sql);
            while (res.next()) {
              System.out.println(res.getString(1));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public HiveConnector(String host, String username, String password, String database) {
        try {
            Class.forName(this.driverName);
            this.conn = DriverManager.getConnection("jdbc:hive2://"+host+":10000/"+database, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Connection getConn() {
        return conn;
    }
    
    

}
