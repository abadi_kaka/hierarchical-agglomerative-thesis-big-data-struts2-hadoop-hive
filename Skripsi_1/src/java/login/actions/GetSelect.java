/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;

import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import models.DataHirarki;
import models.DataNormalisasi;
import models.DataPivoting;
import models.DataFakta;
import models.Period;

/**
 *
 * @author hadoop
 */
public class GetSelect extends ActionSupport {
    private ArrayList<Period> selectPeriod = null;
    private ArrayList<DataPivoting> selectDataPivoting = null;
    private ArrayList<DataNormalisasi> selectDataNormalisasi = null;
    private ArrayList<DataFakta> selectDataFakta = null;
    private ArrayList<DataHirarki> selectDataHirarki = null;
    
    public ArrayList<DataHirarki> getSelectDataHirarki() {
		return selectDataHirarki;
    }

    public void setSelectDataHirarki(ArrayList<DataHirarki> selectDataHirarki) {
            this.selectDataHirarki = selectDataHirarki;
    }
    
    public ArrayList<DataNormalisasi> getSelectDataNormalisasi() {
		return selectDataNormalisasi;
    }

    public void setSelectDataNormalisasi(ArrayList<DataNormalisasi> selectDataNormalisasi) {
            this.selectDataNormalisasi = selectDataNormalisasi;
    }
    
    public ArrayList<DataPivoting> getSelectDataPivoting() {
		return selectDataPivoting;
    }

    public void setSelectDataPivoting(ArrayList<DataPivoting> selectDataPivoting) {
            this.selectDataPivoting = selectDataPivoting;
    }
    
    public ArrayList<DataFakta> getSelectDataFakta() {
		return selectDataFakta;
    }

    public void setSelectDataFakta(ArrayList<DataFakta> selectDataFakta) {
            this.selectDataFakta = selectDataFakta;
    }
    
    public ArrayList<Period> getSelectPeriod() {
		return selectPeriod;
    }

    public void setSelectPeriod(ArrayList<Period> selectPeriod) {
            this.selectPeriod = selectPeriod;
    }
    
    
    public String displayPeriod() throws SQLException{
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from periods";
        System.out.println("Masuk ");
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectPeriod = new ArrayList();
        while(rs0.next()){  
           Period per = new Period();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setStart(rs0.getString(3));
           per.setEnd(rs0.getString(4));
           per.setJenis_kelamin(rs0.getString(5));
           selectPeriod.add(per); 
        }  
    
        }catch(Exception e){
             e.printStackTrace();
        }  
        return SUCCESS;
    }
    
    public String displaySelectPivot() throws SQLException{
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from datapivot";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataPivoting = new ArrayList();
        while(rs0.next()){  
           DataPivoting per = new DataPivoting();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setObject(rs0.getString(3));
           selectDataPivoting.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }  
        return SUCCESS;
    }
    
    public String displaySelectNormalisasi() throws SQLException{
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from datanormal";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataNormalisasi = new ArrayList();
        while(rs0.next()){  
           DataNormalisasi per = new DataNormalisasi();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setObject(rs0.getString(3));
           selectDataNormalisasi.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }  
        return SUCCESS;
    }
    
    public String displaySelectFakta() throws SQLException{
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from datafakta";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataFakta = new ArrayList();
        while(rs0.next()){  
           DataFakta per = new DataFakta();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setObject(rs0.getString(3));
           selectDataFakta.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }  
        return SUCCESS;
    }
    
    public String displaySelectHirarki() throws SQLException{
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from datahirarki";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataHirarki = new ArrayList();
        while(rs0.next()){  
           DataHirarki per = new DataHirarki();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setObject(rs0.getString(3));
           selectDataHirarki.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }  
        return SUCCESS;
    }

}
