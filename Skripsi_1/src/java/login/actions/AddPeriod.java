/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hadoop
 */
public class AddPeriod extends ActionSupport{
    
    private String nama;
    private String start;
    private String end;
    //private List<String> jenis_kelamin;
    private String jenis_kelamin;
    private String jenis_nota;
    
    public String getNama(){
        return nama;
    }
    public void setNama(String nama){
        this.nama = nama;
    }
    public String getStart(){
        return start;
    }
    public void setStart(String start){
        this.start = start;
    }
    public String getEnd(){
        return end;
    }
    public void setEnd(String end){
        this.end = end;
    }
//    public List<String> getJenis_kelamin(){
//        return jenis_kelamin;
//    }
//    public void setJenis_kelamin(List<String> jenis_kelamin){
//        this.jenis_kelamin = jenis_kelamin;
//    }
    public String getJenis_kelamin(){
        return jenis_kelamin;
    }
    public void setJenis_kelamin(String jenis_kelamin){
        this.jenis_kelamin = jenis_kelamin;
    }
    public String getJenis_nota(){
        return jenis_nota;
    }
    public void setJenis_nota(String jenis_nota){
        this.jenis_nota = jenis_nota;
    }

    public String addPeriod() throws SQLException{
        if (this.getNama().length()== 0 | this.getStart().length()==0 | this.getEnd().length()==0 | this.getJenis_kelamin().indexOf(0) == 0 | this.getJenis_nota().indexOf(0) == 0 ){
            return ERROR;
        }else{
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            String sql = "INSERT INTO `periods`(`nama`, `start`, `end`, `jenis_kelamin`, `jenis_nota`) VALUES (?,?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, this.getNama());
            ps.setString(2, this.getStart());
            ps.setString(3, this.getEnd());
            ps.setString(4, this.getJenis_kelamin());
            ps.setString(5, this.getJenis_nota());
            ResultSet rs = ps.executeQuery();
            return SUCCESS;
        }
    }
}
