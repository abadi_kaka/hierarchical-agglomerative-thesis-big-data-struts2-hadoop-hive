/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;

/**
 *
 * @author hadoop
 */

import java.io.*;
import java.lang.*;
import java.sql.*;
import java.util.*;
import oracle.jdbc.driver.*;
import oracle.sql.*;

public class SerializedObject {

   static String genID = 
        "select id from datapivot";

   static String writeSQL = "begin insert into datapivot "+
           " (nama,object) values (?,empty_blob()) "+
           " return object into ?; end;";

   static String writeSQL1 = "insert into datapivot (id,nama,object) values (?,?,?) ";
   static String writeSQL2 = "insert into datafakta (id,nama,object) values (?,?,?) ";
   static String writeSQL3 = "insert into datanormal (id,nama,object) values (?,?,?) ";
   static String writeSQL4 = "insert into datahirarki (id,nama,object) values (?,?,?) ";

   static String readSQL = 
       "select object from datapivot where id = ?";
   static String readSQL1 = 
       "select object from datafakta where id = ?";
   static String readSQL2 = 
       "select object from datanormal where id = ?";
   static String readSQL3 = 
       "select object from datahirarki where id = ?";

   
   static String readmetodeSQL = 
       "select modelfakta from datapivot where id = ?";
   static String readmetodeSQL1 = 
       "select modelfakta from datafakta where id = ?";
   static String readmetodeSQL2 = 
       "select modelfakta from datanormal where id = ?";
   static String readmetodeSQL3 = 
       "select modelfakta from datahirarki where id = ?";

   // We attempt to serialize the object to the database table. 
   // We use a
   // sequence number to identify the object and return that 
   // value to the
   // caller. We also save the class name in case someone wants 
   // to search
   // for objects based on that name. If we get any exception, 
   // we rethrow
   // it to the caller but we always try to clean up after 
   // ourself.

   public void write(Connection conn, String nama, Object o, int idk) 
   throws Exception 
   {
      ObjectOutputStream oop = null;
      //CallableStatement stmt = null;
      PreparedStatement stmt = null;
      OutputStream os = null;
      //long id = nextval(conn);
      String name = nama;
      String className = o.getClass().getName();
      int id = idk;
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(o);
      byte[] objekAsBytes = baos.toByteArray();
//      int modelfakta = model;
      PreparedStatement s = conn.prepareStatement("SELECT * FROM datapivot WHERE id=?");
      s.setInt(1, idk);
      ResultSet result = s.executeQuery ();
      if(result.next()) {
        // dupilcate records present. don't add
      } else {
        try {
           //stmt = conn.prepareCall(writeSQL1);
           ByteArrayInputStream bais = new ByteArrayInputStream(objekAsBytes);
           stmt = conn.prepareStatement(writeSQL1);
           stmt.setInt(1, idk);
           stmt.setString(2, name);
           stmt.setBinaryStream(3, bais, objekAsBytes.length);
  //         stmt.setInt(4, modelfakta);

          //stmt.registerOutParameter(2, java.sql.Types.BLOB);
           stmt.executeUpdate();
           //BLOB blob = (BLOB) stmt.getBlob(2);
           //os = blob.getBinaryOutputStream();
           // get the generated key for the id
  //        ResultSet rs = stmt.getGeneratedKeys();
  //        
  //        if (rs.next()) {
  //          id = rs.getInt(1);
  //        }
  //        
  //        rs.close();
  //         oop = new ObjectOutputStream(os);
  //         oop.writeObject(o);
  //         oop.flush();
        } catch (Exception e) {
           throw e;
        } finally {
           if (oop  != null) { oop.close(); }
           if (os   != null) { os.close(); }
           if (stmt != null) { stmt.close(); }
           System.out.println("Done serializing " + className);
        }
      }

//      return id;
   }
   
   public void writeFakta(Connection conn, String nama, Object o, int idk) 
   throws Exception 
   {
      ObjectOutputStream oop = null;
      PreparedStatement stmt = null;
      OutputStream os = null;
      String name = nama;
      String className = o.getClass().getName();
      int id = -1;
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(o);
      byte[] objekAsBytes = baos.toByteArray();
//      int modelfakta = model;
      PreparedStatement s = conn.prepareStatement("SELECT * FROM datafakta WHERE id=?");
      s.setInt(1, idk);
      ResultSet result = s.executeQuery ();
      if(result.next()) {
        // dupilcate records present. don't add
      } else {

        try {
           ByteArrayInputStream bais = new ByteArrayInputStream(objekAsBytes);
           stmt = conn.prepareStatement(writeSQL2);
           stmt.setInt(1, idk);
           stmt.setString(2, name);
           stmt.setBinaryStream(3, bais, objekAsBytes.length);
  //         stmt.setInt(4, modelfakta);
           stmt.executeUpdate();

        } catch (Exception e) {
           throw e;
        } finally {
           if (oop  != null) { oop.close(); }
           if (os   != null) { os.close(); }
           if (stmt != null) { stmt.close(); }
           System.out.println("Done serializing " + className);
        }
      }
   }
   
   public void writeNormal(Connection conn, String nama, Object o, int idk) 
   throws Exception 
   {
      ObjectOutputStream oop = null;
      PreparedStatement stmt = null;
      OutputStream os = null;
      String name = nama;
      String className = o.getClass().getName();
      int id = -1;
//      int modelfakta = model;
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(o);
      byte[] objekAsBytes = baos.toByteArray();
      PreparedStatement s = conn.prepareStatement("SELECT * FROM datanormal WHERE id=?");
      s.setInt(1, idk);
      ResultSet result = s.executeQuery ();
      if(result.next()) {
        // dupilcate records present. don't add
      } else {
      
        try {
           ByteArrayInputStream bais = new ByteArrayInputStream(objekAsBytes);
           stmt = conn.prepareStatement(writeSQL3);
           stmt.setInt(1, idk);
           stmt.setString(2, name);
           stmt.setBinaryStream(3, bais, objekAsBytes.length);
  //         stmt.setInt(4, modelfakta);
           stmt.executeUpdate();

        } catch (Exception e) {
           throw e;
        } finally {
           if (oop  != null) { oop.close(); }
           if (os   != null) { os.close(); }
           if (stmt != null) { stmt.close(); }
           System.out.println("Done serializing " + className);
        }
      }
   }
   public void writeHirarki(Connection conn, String nama, Object o, int idk) 
   throws Exception 
   {
      ObjectOutputStream oop = null;
      PreparedStatement stmt = null;
      OutputStream os = null;
      String name = nama;
      String className = o.getClass().getName();
      int id = -1;
//      int modelfakta = model;
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(o);
      byte[] objekAsBytes = baos.toByteArray();
      PreparedStatement s = conn.prepareStatement("SELECT * FROM datahirarki WHERE id=?");
      s.setInt(1, idk);
      ResultSet result = s.executeQuery ();
      if(result.next()) {
        // dupilcate records present. don't add
      } else {
      
        try {
           ByteArrayInputStream bais = new ByteArrayInputStream(objekAsBytes);
           stmt = conn.prepareStatement(writeSQL4);
           stmt.setInt(1, idk);
           stmt.setString(2, name);
           stmt.setBinaryStream(3, bais, objekAsBytes.length);
  //         stmt.setInt(4, modelfakta);

           stmt.executeUpdate();

        } catch (Exception e) {
           throw e;
        } finally {
           if (oop  != null) { oop.close(); }
           if (os   != null) { os.close(); }
           if (stmt != null) { stmt.close(); }
           System.out.println("Done serializing " + className);
        }
      }
   }
   

   // We attempt to de-serialize the object from the database 
   // table using
   // the given identifier. If we get any exception, we rethrow 
   // it to the
   // caller but we always try to clean up after ourself.

   public Object read(Connection conn, int id) 
   throws Exception 
   {
      ObjectInputStream oip = null;
      PreparedStatement stmt = null;
      InputStream is = null;
      Object o = null;
      String className = null;

      try {
         stmt = conn.prepareStatement(readSQL);
         stmt.setLong(1, id);
         ResultSet rs = stmt.executeQuery();
         while (rs.next()) {
          byte[] st = (byte[]) rs.getObject(1);
          ByteArrayInputStream baip = new ByteArrayInputStream(st);
          ObjectInputStream ois = new ObjectInputStream(baip);
          o = ois.readObject();
        }
        stmt.close();
        rs.close();
         // Even though we expect only one row back, the caller 
         // could have
         // passed an invalid identifier so we wrap this in a 
         // loop to
         // make sure we don't get null pointer exceptions. In 
         // the case
         // where there are 0 rows, we would return a null 
         // value. Where
         // there are > 1 rows, we would return only the first 
         // one.

//         while (rs.next()) {
//            is = rs.getBlob(1).getBinaryStream();
//            oip = new ObjectInputStream(is);
//            o = oip.readObject();
//            className = o.getClass().getName();
//            break;
//         }
      } catch (Exception e) {
         throw e;
      } finally {
         if (oip  != null) { oip.close(); }
         if (is   != null) { is.close(); }
         if (stmt != null) { stmt.close(); }
         System.out.println("Done de-serializing " + className);
      }

      return o;
   }
   
   public Object readFakta(Connection conn, int id) 
   throws Exception 
   {
      ObjectInputStream oip = null;
      PreparedStatement stmt = null;
      InputStream is = null;
      Object o = null;
      String className = null;

      try {
         stmt = conn.prepareStatement(readSQL1);
         stmt.setLong(1, id);
         ResultSet rs = stmt.executeQuery();
         while (rs.next()) {
          byte[] st = (byte[]) rs.getObject(1);
          ByteArrayInputStream baip = new ByteArrayInputStream(st);
          ObjectInputStream ois = new ObjectInputStream(baip);
          o = ois.readObject();
        }
        stmt.close();
        rs.close();
      } catch (Exception e) {
         throw e;
      } finally {
         if (oip  != null) { oip.close(); }
         if (is   != null) { is.close(); }
         if (stmt != null) { stmt.close(); }
         System.out.println("Done de-serializing " + className);
      }

      return o;
   }
   
   public Object readNormal(Connection conn, int id) 
   throws Exception 
   {
      ObjectInputStream oip = null;
      PreparedStatement stmt = null;
      InputStream is = null;
      Object o = null;
      String className = null;

      try {
         stmt = conn.prepareStatement(readSQL2);
         stmt.setLong(1, id);
         ResultSet rs = stmt.executeQuery();
         while (rs.next()) {
          byte[] st = (byte[]) rs.getObject(1);
          ByteArrayInputStream baip = new ByteArrayInputStream(st);
          ObjectInputStream ois = new ObjectInputStream(baip);
          o = ois.readObject();
        }
        stmt.close();
        rs.close();
      } catch (Exception e) {
         throw e;
      } finally {
         if (oip  != null) { oip.close(); }
         if (is   != null) { is.close(); }
         if (stmt != null) { stmt.close(); }
         System.out.println("Done de-serializing " + className);
      }

      return o;
   }

   public Object readHirarki(Connection conn, int id) 
   throws Exception 
   {
      ObjectInputStream oip = null;
      PreparedStatement stmt = null;
      InputStream is = null;
      Object o = null;
      String className = null;

      try {
         stmt = conn.prepareStatement(readSQL3);
         stmt.setLong(1, id);
         ResultSet rs = stmt.executeQuery();
         while (rs.next()) {
          byte[] st = (byte[]) rs.getObject(1);
          ByteArrayInputStream baip = new ByteArrayInputStream(st);
          ObjectInputStream ois = new ObjectInputStream(baip);
          o = ois.readObject();
        }
        stmt.close();
        rs.close();
      } catch (Exception e) {
         throw e;
      } finally {
         if (oip  != null) { oip.close(); }
         if (is   != null) { is.close(); }
         if (stmt != null) { stmt.close(); }
         System.out.println("Done de-serializing " + className);
      }

      return o;
   }
   
   // Get the next sequence value. Ideally, this would be better 
   // if
   // we just created a prepared statement and repeatedly used 
   // that.
   // Obviously, we must assume the same connection is always 
   // used.

   private static long nextval(Connection conn) 
   throws SQLException {
      long id = -1;
      Statement stmt = null;
      ResultSet rs = null;

      try {
         stmt = conn.createStatement();
         rs = stmt.executeQuery(genID);

         while (rs.next()) {
            id = rs.getInt(1);
         }
      } catch (SQLException e) {
         throw e;
      } finally {
         if (rs != null) { rs.close(); }
         if (stmt != null) { stmt.close(); }
      }

      return id;
   }

   // Here we just simply connect to the database and either
   // serialize or de-serialize an object. If we get an
   // exception then we will rollback whatever we have done.
   // Commit changes as necessary and close the connection.
   private Connection conn;
   private String driverName = "org.apache.hive.jdbc.HiveDriver";
   
   /*public static void main(String[] argv) throws Exception {
      long id;
      Connection conn = null;
      String cs = "jdbc:oracle:oci8:@ora816dev";
      String user = "scott";
      String pass = "tiger";

      try {
          
         DriverManager.registerDriver(new OracleDriver());
         conn = DriverManager.getConnection(cs, user, pass);
         conn.setAutoCommit(false);

         if (argv.length == 0) {
            id = write(conn, new java.util.Date());
            conn.commit();
            System.out.println("ID= " + id);

            LinkedList l = new LinkedList();
            l.add("This");
            l.add("is");
            l.add("a");
            l.add("test");
            id = write(conn, l);
            conn.commit();
            System.out.println("ID= " + id);
         } else {
            id = Long.parseLong(argv[0]);
            Object o = read(conn, id);
            System.out.println("Object= " + o);
         }
      } catch (Exception e) {
         e.printStackTrace();
         if (conn != null) { conn.rollback(); }
      } finally {
         if (conn != null) { conn.close(); }
      }
   }*/
   public String readMetode(Connection conn, int id) 
   throws Exception 
   {
      ObjectInputStream oip = null;
      PreparedStatement stmt = null;
      InputStream is = null;
      Object o = null;
      String className = null;
      String metode = null;
      try {
         stmt = conn.prepareStatement(readmetodeSQL);
         stmt.setLong(1, id);
         ResultSet rs = stmt.executeQuery();
         while (rs.next()) {
             metode = rs.getString(1);
        }
        stmt.close();
        rs.close();

      } catch (Exception e) {
         throw e;
      } finally {
         if (oip  != null) { oip.close(); }
         if (is   != null) { is.close(); }
         if (stmt != null) { stmt.close(); }
         System.out.println("Done de-serializing " + className);
      }

      return metode;
   }
   
   public String readMetodeFakta(Connection conn, int id) 
   throws Exception 
   {
      ObjectInputStream oip = null;
      PreparedStatement stmt = null;
      InputStream is = null;
      Object o = null;
      String className = null;
      String metode = null;
      try {
         stmt = conn.prepareStatement(readmetodeSQL1);
         stmt.setLong(1, id);
         ResultSet rs = stmt.executeQuery();
         while (rs.next()) {
             metode = rs.getString(1);
        }
        stmt.close();
        rs.close();

      } catch (Exception e) {
         throw e;
      } finally {
         if (oip  != null) { oip.close(); }
         if (is   != null) { is.close(); }
         if (stmt != null) { stmt.close(); }
         System.out.println("Done de-serializing " + className);
      }

      return metode;
   }
   
   public String readMetodeNormal(Connection conn, int id) 
   throws Exception 
   {
      ObjectInputStream oip = null;
      PreparedStatement stmt = null;
      InputStream is = null;
      Object o = null;
      String className = null;
      String metode = null;
      try {
         stmt = conn.prepareStatement(readmetodeSQL2);
         stmt.setLong(1, id);
         ResultSet rs = stmt.executeQuery();
         while (rs.next()) {
             metode = rs.getString(1);
        }
        stmt.close();
        rs.close();

      } catch (Exception e) {
         throw e;
      } finally {
         if (oip  != null) { oip.close(); }
         if (is   != null) { is.close(); }
         if (stmt != null) { stmt.close(); }
         System.out.println("Done de-serializing " + className);
      }

      return metode;
   }
   
   public String readMetodeHirarki(Connection conn, int id) 
   throws Exception 
   {
      ObjectInputStream oip = null;
      PreparedStatement stmt = null;
      InputStream is = null;
      Object o = null;
      String className = null;
      String metode = null;
      try {
         stmt = conn.prepareStatement(readmetodeSQL3);
         stmt.setLong(1, id);
         ResultSet rs = stmt.executeQuery();
         while (rs.next()) {
             metode = rs.getString(1);
        }
        stmt.close();
        rs.close();

      } catch (Exception e) {
         throw e;
      } finally {
         if (oip  != null) { oip.close(); }
         if (is   != null) { is.close(); }
         if (stmt != null) { stmt.close(); }
         System.out.println("Done de-serializing " + className);
      }

      return metode;
   }
}
