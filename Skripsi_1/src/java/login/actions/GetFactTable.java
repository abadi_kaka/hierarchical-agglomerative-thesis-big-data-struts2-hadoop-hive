/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;
import com.mabadi.algorithm.AggloromerativeClustering;
import com.mabadi.algorithm.Cluster;
/*import com.apporiented.algorithm.clustering.AverageLinkageStrategy;
import com.apporiented.algorithm.clustering.Cluster;
import com.apporiented.algorithm.clustering.ClusteringAlgorithm;
import com.apporiented.algorithm.clustering.DefaultClusteringAlgorithm;
import com.apporiented.algorithm.clustering.DendrogramPanel;
import com.apporiented.algorithm.clustering.SingleLinkageStrategy;*/
import static com.mabadi.algorithm.AggloromerativeClustering.ClusteringType.SINGLE_LINKAGE;
import static com.mabadi.algorithm.AggloromerativeClustering.ClusteringType.COMPLETE_LINKAGE;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.*;  
import java.util.ArrayList;  
import java.util.List;
import java.util.Map;
import java.lang.Number;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import models.Bon_Distance;
import models.Bon_Transaction;
import models.Distance_Table;
import models.Period;
import models.Pivot_Table;
import models.PivotJenis_Table;
import models.Jenis;
import models.Strip;
import models.User;
import org.apache.struts2.interceptor.SessionAware;
import weka.core.EuclideanDistance;
import weka.core.Instance;
//import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import models.BonJenis_Transaction;
import models.ClusterManual;
import models.DataHirarki;
import models.DataNormalisasi;
import models.DataPivoting;
import models.DataFakta;
import models.DataUser;
import models.InfoNotaBon;
import models.InfoNotaTunai;
import models.InfoPelanggan;
import models.InfoStrip;
import models.InfoToko;
import models.Informasi;
import models.Scatter;
import models.ScatterJenis;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;
/**
 *
 * @author hadoop
 */
public class GetFactTable extends ActionSupport implements SessionAware {
    
    private ArrayList<Bon_Transaction> list = null;
    private ArrayList<Bon_Transaction> listnormal = null;
    private ArrayList<BonJenis_Transaction> listjenis = null;
    private ArrayList<BonJenis_Transaction> listjenisnormal = null;
    private ArrayList<Pivot_Table> pivot = null;
    private ArrayList<Pivot_Table> pivotbaru = null;
    private ArrayList<PivotJenis_Table> pivotjenis = null;
    private ArrayList<PivotJenis_Table> pivotjenisbaru = null;
    private ArrayList<PivotJenis_Table> pivotjenisbaru2 = null;
    private ArrayList<PivotJenis_Table> pivotjenismanual = null;
    private ArrayList<ClusterManual> clustermanual = null;
    private ArrayList<Period> selectPeriod = null;
    

    //ArrayList utk data sesudah di euclid
    private ArrayList<DataHirarki> selectDataHirarki = null;
    private ArrayList<DataUser> selectDataUser = null;
    
    //ArrayList utk data sesudah di pivot
    private ArrayList<DataPivoting> selectDataPivoting = null;
    
    //ArrayList utk data sesudah di normal
    private ArrayList<DataNormalisasi> selectDataNormalisasi = null;
    
    //ArrayList utk data sesudah di fakta
    private ArrayList<DataFakta> selectDataFakta = null;
    
    private ArrayList<Strip> strip = null;
    private ArrayList<Strip> stripcopy = null;
    private ArrayList<Jenis> jenis = null;
    private ArrayList<Jenis> jeniscopy = null;
    private ArrayList<Distance_Table> distancespace = null;
    private ArrayList<Bon_Distance> bondistance = null;
    private ArrayList<Scatter> scatterplot = null;
    private ArrayList<ScatterJenis> scatterplotjenis = null;
    private ArrayList scatterplothasil = null;
    private ArrayList<Informasi> info = null;
    private Cluster dendrogram = null;
    private ArrayList<InfoNotaTunai> infonotatunai = null;
    private ArrayList<InfoNotaBon> infonotabon = null;
    private ArrayList<InfoPelanggan> infopelanggan = null;
    private ArrayList<InfoStrip> infostrip = null;
    private ArrayList<InfoToko> infotoko = null;
    private String idUsr;

    //getter setter
    public Cluster getDendrogram() {
            return dendrogram;
    }
   
    public void setDendrogram(Cluster dendrogram) {
            this.dendrogram = dendrogram;
    }
    
    public ArrayList getScatterplothasil() {
		return scatterplothasil;
    }
   

    public void setScatterplothasil(ArrayList scatterplothasil) {
            this.scatterplothasil = scatterplothasil;
    }
    
    public ArrayList<Scatter> getScatterplot() {
		return scatterplot;
    }
   

    public void setScatterplot(ArrayList<Scatter> scatterplot) {
            this.scatterplot = scatterplot;
    }
    
    public void setInfo(ArrayList<Informasi> info) {
            this.info = info;
    }
    
    public ArrayList<Informasi> getInfo() {
		return info;
    }
    
    public ArrayList<ScatterJenis> getScatterplotjenis() {
		return scatterplotjenis;
    }
   

    public void setScatterplotjenis(ArrayList<ScatterJenis> scatterplotjenis) {
            this.scatterplotjenis = scatterplotjenis;
    }
    
    public ArrayList<Bon_Distance> getBondistance() {
		return bondistance;
    }
   

    public void setBondistance(ArrayList<Bon_Distance> bondistance) {
            this.bondistance = bondistance;
    }
    
    public ArrayList<Distance_Table> getDistancespace() {
		return distancespace;
    }

    public void setDistancespace(ArrayList<Distance_Table> distancespace) {
            this.distancespace = distancespace;
    }
    
    public ArrayList<Period> getSelectPeriod() {
		return selectPeriod;
    }

    public void setSelectPeriod(ArrayList<Period> selectPeriod) {
            this.selectPeriod = selectPeriod;
    }
    
    public ArrayList<DataHirarki> getSelectDataHirarki() {
		return selectDataHirarki;
    }

    public void setSelectDataHirarki(ArrayList<DataHirarki> selectDataHirarki) {
            this.selectDataHirarki = selectDataHirarki;
    }
    
    
    public ArrayList<DataUser> getSelectDataUser() {
		return selectDataUser;
    }

    public void setSelectDataUser(ArrayList<DataUser> selectDataUser) {
            this.selectDataUser = selectDataUser;
    }
    
    public ArrayList<DataNormalisasi> getSelectDataNormalisasi() {
		return selectDataNormalisasi;
    }

    public void setSelectDataNormalisasi(ArrayList<DataNormalisasi> selectDataNormalisasi) {
            this.selectDataNormalisasi = selectDataNormalisasi;
    }
    
    public ArrayList<DataPivoting> getSelectDataPivoting() {
		return selectDataPivoting;
    }

    public void setSelectDataPivoting(ArrayList<DataPivoting> selectDataPivoting) {
            this.selectDataPivoting = selectDataPivoting;
    }
    
    public ArrayList<DataFakta> getSelectDataFakta() {
		return selectDataFakta;
    }

    public void setSelectDataFakta(ArrayList<DataFakta> selectDataFakta) {
            this.selectDataFakta = selectDataFakta;
    }
    
    public ArrayList<Strip> getStrip() {
		return strip;
    }

    public void setStrip(ArrayList<Strip> strip) {
            this.strip = strip;
    }
    public ArrayList<Strip> getStripcopy() {
		return stripcopy;
    }

    public void setStripcopy(ArrayList<Strip> stripcopy) {
            this.stripcopy = stripcopy;
    }
    
    public ArrayList<Bon_Transaction> getList() {  
        return list;  
    }  
    
    public void setList(ArrayList<Bon_Transaction> list) {  
        this.list = list;  
    }
    
    public void setPivot(ArrayList<Pivot_Table> pivot) {  
        this.pivot = pivot;  
    }  
    
    
    public ArrayList<Pivot_Table> getPivot() {  
        return pivot;  
    }
    
    public void setPivotjenis(ArrayList<PivotJenis_Table> pivotjenis) {  
        this.pivotjenis = pivotjenis;  
    }  
    
    
    public ArrayList<PivotJenis_Table> getPivotjenis() {  
        return pivotjenis;  
    }
    
    public void setPivotjenismanual(ArrayList<PivotJenis_Table> pivotjenismanual) {  
        this.pivotjenismanual = pivotjenismanual;  
    }  
    
    public ArrayList<PivotJenis_Table> getPivotjenismanual() {  
        return pivotjenismanual;  
    }
    
    public void setClustermanual(ArrayList<ClusterManual> clustermanual) {  
        this.clustermanual = clustermanual;  
    }  
    
    public ArrayList<ClusterManual> getClustermanual() {  
        return clustermanual;  
    }
    
    public void setPivotjenisbaru(ArrayList<PivotJenis_Table> pivotjenisbaru) {  
        this.pivotjenisbaru = pivotjenisbaru;  
    }  
    
    public ArrayList<PivotJenis_Table> getPivotjenisbaru() {  
        return pivotjenisbaru;  
    }
    
    public void setPivotbaru(ArrayList<Pivot_Table> pivotbaru) {  
        this.pivotbaru = pivotbaru;  
    }  
    
    public ArrayList<Pivot_Table> getPivotbaru() {  
        return pivotbaru;  
    }  
    
    public ArrayList<Bon_Transaction> getListnormal() {  
        return listnormal;  
    }  
    
    public void setListnormal(ArrayList<Bon_Transaction> listnormal) {  
        this.listnormal = listnormal;  
    }  
    
    public ArrayList<BonJenis_Transaction> getListjenisnormal() {  
        return listjenisnormal;  
    }  
    
    public void setListjenisnormal(ArrayList<BonJenis_Transaction> listjenisnormal) {  
        this.listjenisnormal = listjenisnormal;  
    }
    
    public ArrayList<BonJenis_Transaction> getListjenis() {  
        return listjenis;  
    }  
    
    public void setListjenis(ArrayList<BonJenis_Transaction> listjenis) {  
        this.listjenis = listjenis;  
    }
    
    public void displaySelectPivot(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from datapivot";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataPivoting = new ArrayList();
        while(rs0.next()){  
           DataPivoting per = new DataPivoting();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setObject(rs0.getString(3));
           selectDataPivoting.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    public String displaySelectUser(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from users";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataUser = new ArrayList();
        while(rs0.next()){  
           DataUser per = new DataUser();
           per.setUserId(rs0.getString(1));
           per.setUsername(rs0.getString(2));
           per.setPassword(rs0.getString(3));
           if (rs0.getString(4).equals("0")) {
               per.setRole("Admin");    
           }else{
               per.setRole("User");    
               
           }
           selectDataUser.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String deleteUser(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "DELETE from users where id ="+idUsr+"";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();          
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public void displaySelectNormalisasi(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from datanormal";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataNormalisasi = new ArrayList();
        while(rs0.next()){  
           DataNormalisasi per = new DataNormalisasi();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setObject(rs0.getString(3));
           selectDataNormalisasi.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    public void displaySelectFakta(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from datafakta";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataFakta = new ArrayList();
        while(rs0.next()){  
           DataFakta per = new DataFakta();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setObject(rs0.getString(3));
           selectDataFakta.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    public void displaySelectSingle(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from datahirarki";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectDataHirarki = new ArrayList();
        while(rs0.next()){  
           DataHirarki per = new DataHirarki();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setObject(rs0.getString(3));
           selectDataHirarki.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    public void displaySelect(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        String sql = "SELECT * from periods";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs0 = ps.executeQuery();
        selectPeriod = new ArrayList();
        while(rs0.next()){  
           Period per = new Period();
           per.setId(rs0.getString(1));
           per.setNama(rs0.getString(2));
           per.setStart(rs0.getString(3));
           per.setEnd(rs0.getString(4));
           per.setJenis_kelamin(rs0.getString(5));
           selectPeriod.add(per); 
        }  
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    private String start;
    private String end;
    private String jenis_kelamin;
    private String namaperiod;
    private String periodselect;
    private String potongselect;
    private String modelfakta;
    private String metodeselect;
    private String jenis_nota;
    
    public void setPeriodselect(String periodselect){
        this.periodselect = periodselect;
    }
    public String getPeriodselect(){
        return periodselect;
    }
    
    public void setModelfakta(String modelfakta){
        this.modelfakta = modelfakta;
    }
    public String getModelfakta(){
        return modelfakta;
    }
    
    public void setMetodeselect(String metodeselect){
        this.metodeselect = metodeselect;
    }
    public String getMetodeselect(){
        return metodeselect;
    }
    
    public void getDate(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        System.out.println("PERIOD : "+periodselect);
            
        String sql = "SELECT start, end, jenis_kelamin, nama, jenis_nota from periods where id = " + periodselect;
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs1 = ps.executeQuery();
        
        while(rs1.next()){  
           this.start = rs1.getString(1);
           this.end = rs1.getString(2);
           this.jenis_kelamin = rs1.getString(3);
           this.namaperiod = rs1.getString(4);
           this.jenis_nota  = rs1.getString(5);
        }  
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        session.put("NAMAPER", namaperiod);
        session.put("JK", jenis_kelamin);
        session.put("JN", jenis_nota);
        
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    public void getKelamin(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        Map<String, Object> session = ActionContext.getContext().getSession();
        periodselect = (String)session.get("SELPERIOD");
        String sql = "SELECT jenis_kelamin from periods where id = " + periodselect;
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs1 = ps.executeQuery();
        
        while(rs1.next()){  
           this.jenis_kelamin = rs1.getString(1);
        }  
        
        session.put("JK", jenis_kelamin);
        
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    
    public void getDataPivot(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
            
        String sql = "SELECT nama, object from datapivot where id = " + periodselect;
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs1 = ps.executeQuery();
        
        while(rs1.next()){  
           this.namaperiod = rs1.getString(1);
        }  
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        session.put("NAMAPER", namaperiod);
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    public void getDataFakta(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        
        String sql = "SELECT nama, object from datafakta where id = " + periodselect;
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs1 = ps.executeQuery();
        
        while(rs1.next()){  
           this.namaperiod = rs1.getString(1);
        }  
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        session.put("NAMAPER", namaperiod);
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    public void getDataNormal(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
            
        String sql = "SELECT nama, object from datanormal where id = " + periodselect;
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs1 = ps.executeQuery();
        
        while(rs1.next()){  
           this.namaperiod = rs1.getString(1);
        }  
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        session.put("NAMAPER", namaperiod);
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    public void getDataHirarki(){
        try{  
        
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
            
        String sql = "SELECT nama, object from datahirarki where id = " + periodselect;
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs1 = ps.executeQuery();
        
        while(rs1.next()){  
           this.namaperiod = rs1.getString(1);
        }  
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        session.put("NAMAPER", namaperiod);
        conn.close();
        }catch(Exception e){
             e.printStackTrace();
        }
    }
    
    private Double minNormUmur;
    private Double maxNormUmur;
    private Double minNormJumlah;
    private Double maxNormJumlah;
    private Double minNormJumlahAnak;
    private Double maxNormJumlahAnak;
    private Double minNormJumlahWanita;
    private Double maxNormJumlahWanita;
    private Double minNormJumlahPria;
    private Double maxNormJumlahPria;
    private Double minNormJumlahSepatu;
    private Double maxNormJumlahSepatu;
    
    private Map<String, Object> sessionAttributes = null;
    
    public void setSession(Map<String, Object> sessionAttributes) {
        this.sessionAttributes = sessionAttributes;
    }
    
    public String checkFact() {
        Map<String, Object> session = ActionContext.getContext().getSession();
        if (session.get("FACT")!= null) {
            return ERROR;
        }else{
            return SUCCESS;
        }
    }
    
    public Map<String, Object> getSession() {
                return sessionAttributes;
    }
    
    public String normalisasi(){        
        try{  
            this.displaySelectFakta();
            this.getDataFakta();
            Map<String, Object> session = ActionContext.getContext().getSession();
            namaperiod = (String) session.get("NAMAPER");

            SerializedObject se = new SerializedObject();
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            conn.setAutoCommit(false);
            Object o = se.readFakta(conn, Integer.parseInt(periodselect));
            conn.commit();
            listjenisnormal = new ArrayList<>((ArrayList<BonJenis_Transaction>)o);
        
            if (metodeselect.equals("0")) {
                Double maxUmur = Double.MIN_VALUE;
                Double minUmur = Double.MAX_VALUE;
                Double minJumlah = Double.MAX_VALUE;
                Double maxJumlah = Double.MIN_VALUE;
                Double minJumlahAnak = Double.MAX_VALUE;
                Double maxJumlahAnak = Double.MIN_VALUE;
                Double minJumlahWanita = Double.MAX_VALUE;
                Double maxJumlahWanita = Double.MIN_VALUE;
                Double minJumlahPria = Double.MAX_VALUE;
                Double maxJumlahPria = Double.MIN_VALUE;
                Double minJumlahSepatu = Double.MAX_VALUE;
                Double maxJumlahSepatu = Double.MIN_VALUE;

                for (int i = 0; i < listjenisnormal.size(); i++) {

                    if (listjenisnormal.get(i).getKel_jns().equals("A")) {
                        if (listjenisnormal.get(i).getJumlah() < minJumlahAnak) {
                            minJumlahAnak = listjenisnormal.get(i).getJumlah();
                        }else if(listjenisnormal.get(i).getJumlah() > maxJumlahAnak){
                            maxJumlahAnak = listjenisnormal.get(i).getJumlah();
                        }                    
                    }else if(listjenisnormal.get(i).getKel_jns().equals("W")){
                        if (listjenisnormal.get(i).getJumlah() < minJumlahWanita) {
                            minJumlahWanita = listjenisnormal.get(i).getJumlah();
                        }else if(listjenisnormal.get(i).getJumlah() > maxJumlahWanita){
                            maxJumlahWanita = listjenisnormal.get(i).getJumlah();
                        }                    
                    }else if(listjenisnormal.get(i).getKel_jns().equals("P")){
                        if (listjenisnormal.get(i).getJumlah() < minJumlahPria) {
                            minJumlahPria = listjenisnormal.get(i).getJumlah();
                        }else if(listjenisnormal.get(i).getJumlah() > maxJumlahPria){
                            maxJumlahPria = listjenisnormal.get(i).getJumlah();
                        }                                        
                    }else if(listjenisnormal.get(i).getKel_jns().equals("S")){
                        if (listjenisnormal.get(i).getJumlah() < minJumlahSepatu) {
                            minJumlahSepatu = listjenisnormal.get(i).getJumlah();
                        }else if(listjenisnormal.get(i).getJumlah() > maxJumlahSepatu){
                            maxJumlahSepatu = listjenisnormal.get(i).getJumlah();
                        }                                        

                    }

                    if (listjenisnormal.get(i).getUmur()< minUmur) {
                            minUmur = listjenisnormal.get(i).getUmur();
                    }else if(listjenisnormal.get(i).getUmur() > maxUmur){
                            maxUmur = listjenisnormal.get(i).getUmur();
                    }
                }

            this.minNormUmur = minUmur;
            this.maxNormUmur = maxUmur;
            this.minNormJumlahAnak = minJumlahAnak;
            this.maxNormJumlahAnak = maxJumlahAnak;
            this.minNormJumlahWanita = minJumlahWanita;
            this.maxNormJumlahWanita = maxJumlahWanita;
            this.minNormJumlahPria = minJumlahPria;
            this.maxNormJumlahPria = maxJumlahPria;
            this.minNormJumlahSepatu = minJumlahSepatu;
            this.maxNormJumlahSepatu = maxJumlahSepatu;

            Double difjumlahanak = this.maxNormJumlahAnak - this.minNormJumlahAnak;
            Double difjumlahwanita = this.maxNormJumlahWanita - this.minNormJumlahWanita;
            Double difjumlahpria = this.maxNormJumlahPria - this.minNormJumlahPria;
            Double difjumlahsepatu = this.maxNormJumlahSepatu - this.minNormJumlahSepatu;
            Double difumur = this.maxNormUmur - this.minNormUmur;
            Double hasilUmur = 0.0; 
            Double hasilJumlahAnak = 0.0;
            Double hasilJumlahWanita = 0.0;
            Double hasilJumlahPria = 0.0;
            Double hasilJumlahSepatu = 0.0; 

                for (int i = 0; i < listjenisnormal.size(); i++) {
                    hasilUmur = ((listjenisnormal.get(i).getUmur() - minNormUmur)/difumur) * 1;
                    if (listjenisnormal.get(i).getKel_jns().equals("A")) {
                        hasilJumlahAnak = ((listjenisnormal.get(i).getJumlah()- minNormJumlahAnak)/difjumlahanak) * 1;
                        if (Double.isNaN(hasilJumlahAnak)) {

                            listjenisnormal.get(i).setJumlahnormal(0.0);                    
                        }else{
                            hasilJumlahAnak =  hasilJumlahAnak * 10000 / 10000;
                            listjenisnormal.get(i).setJumlahnormal(hasilJumlahAnak);                    
                        }

                    }else if(listjenisnormal.get(i).getKel_jns().equals("W")){
                        hasilJumlahWanita = ((listjenisnormal.get(i).getJumlah()- minNormJumlahWanita)/difjumlahwanita) * 1;             
                        if (Double.isNaN(hasilJumlahWanita)) {
                            listjenisnormal.get(i).setJumlahnormal(0.0);

                        }else{
                            hasilJumlahWanita =  hasilJumlahWanita * 10000 / 10000;
                            listjenisnormal.get(i).setJumlahnormal(hasilJumlahWanita);
                        }
                    }else if(listjenisnormal.get(i).getKel_jns().equals("P")){
                        hasilJumlahPria = ((listjenisnormal.get(i).getJumlah()- minNormJumlahPria)/difjumlahpria) * 1;            
                        if (Double.isNaN(hasilJumlahPria)) {
                            listjenisnormal.get(i).setJumlahnormal(0.0);

                        }else{
                            hasilJumlahPria =  hasilJumlahPria * 10000 / 10000;
                            listjenisnormal.get(i).setJumlahnormal(hasilJumlahPria);                    
                        }                    
                    }else if(listjenisnormal.get(i).getKel_jns().equals("S")){
                        hasilJumlahSepatu = ((listjenisnormal.get(i).getJumlah()- minNormJumlahSepatu)/difjumlahsepatu) * 1;             
                        listjenisnormal.get(i).setJumlahnormal(hasilJumlahSepatu);                    
                        if (Double.isNaN(hasilJumlahSepatu)) {
                            listjenisnormal.get(i).setJumlahnormal(0.0);

                        }else{
                            hasilJumlahSepatu =  hasilJumlahSepatu * 10000 / 10000;
                            listjenisnormal.get(i).setJumlahnormal(hasilJumlahSepatu);                    
                        }                    

                    }
                    hasilUmur =  hasilUmur * 10000 / 10000;
                    listjenisnormal.get(i).setUmurnormal(hasilUmur);

                }
            
            }else if (metodeselect.equals("1")) {
    
                    Double totalUmur = new Double(0.0);
                    Double totalJumlah = new Double(0.0);
                    Double meanUmur = new Double(0.0);
                    Double meanAnak = new Double(0.0);
                    Double meanWanita = new Double(0.0);
                    Double meanPria = new Double(0.0);
                    Double meanSepatu = new Double(0.0);
                    Double stdUmur = new Double(0.0);
                    Double stdJumlah = new Double(0.0);
                    Double stdAnak = new Double(0.0);
                    Double stdPria = new Double(0.0);
                    Double stdWanita = new Double(0.0);
                    Double stdSepatu = new Double(0.0);
                    Double stdXUmur = new Double(0.0);
                    Double stdXUmurKuadrat = new Double(0.0);
                    Double stdXJumlah = new Double(0.0);
                    Double stdXJumlahKuadrat = new Double(0.0);
                    Double stdXJumlahAnak = new Double(0.0);
                    Double stdXJumlahAnakKuadrat = new Double(0.0);
                    Double stdXJumlahWanita = new Double(0.0);
                    Double stdXJumlahWanitaKuadrat = new Double(0.0);
                    Double stdXJumlahPria = new Double(0.0);
                    Double stdXJumlahPriaKuadrat = new Double(0.0);
                    Double stdXJumlahSepatu = new Double(0.0);
                    Double stdXJumlahSepatuKuadrat = new Double(0.0);

                    
                    Double totalJumlahAnak = new Double(0.0);
                    Double totalJumlahWanita = new Double(0.0);
                    Double totalJumlahPria = new Double(0.0);
                    Double totalJumlahSepatu = new Double(0.0);
                    
        //          cari mean
                    int countNormal = 0;
                    int countNormalJenisAnak = 0;
                    int countNormalJenisWanita = 0;
                    int countNormalJenisPria = 0;
                    int countNormalJenisSepatu = 0;
                    for (int i = 0; i < listjenisnormal.size(); i++) {
                        //jenis
                        if (listjenisnormal.get(i).getKel_jns().equals("A")) {
                            totalJumlahAnak = totalJumlahAnak + listjenisnormal.get(i).getJumlah();
                            stdXJumlahAnak = stdXJumlahAnak + listjenisnormal.get(i).getJumlah();
                            stdXJumlahAnakKuadrat = stdXJumlahAnakKuadrat + Math.pow(listjenisnormal.get(i).getJumlah(),2);                    
                            countNormalJenisAnak++;
                        }else if (listjenisnormal.get(i).getKel_jns().equals("W")) {
                            totalJumlahWanita = totalJumlahWanita + listjenisnormal.get(i).getJumlah();                            
                            stdXJumlahWanita = stdXJumlahWanita + listjenisnormal.get(i).getJumlah();
                            stdXJumlahWanitaKuadrat = stdXJumlahWanitaKuadrat + Math.pow(listjenisnormal.get(i).getJumlah(),2);
                            countNormalJenisWanita++;

                        }else if (listjenisnormal.get(i).getKel_jns().equals("P")){
                            totalJumlahPria = totalJumlahPria + listjenisnormal.get(i).getJumlah();
                            stdXJumlahPria = stdXJumlahPria + listjenisnormal.get(i).getJumlah();
                            stdXJumlahPriaKuadrat = stdXJumlahPriaKuadrat + Math.pow(listjenisnormal.get(i).getJumlah(),2);
                            countNormalJenisPria++;
                        }else{
                            totalJumlahSepatu = totalJumlahSepatu + listjenisnormal.get(i).getJumlah();
                            stdXJumlahSepatu = stdXJumlahSepatu + listjenisnormal.get(i).getJumlah();
                            stdXJumlahSepatuKuadrat = stdXJumlahSepatuKuadrat + Math.pow(listjenisnormal.get(i).getJumlah(),2);
                            countNormalJenisSepatu++;

                        }
                        
                        if (i==0) {
                            totalUmur = totalUmur +listjenisnormal.get(i).getUmur();
                            stdXUmurKuadrat = stdXUmurKuadrat + Math.pow(listjenisnormal.get(i).getUmur(),2);
                            stdXUmur = stdXUmur + listjenisnormal.get(i).getUmur();
                            countNormal++;
                            continue;
                        }
                        if (listjenisnormal.get(i).getNo_bon().equals(listjenisnormal.get(i-1).getNo_bon())) {
                            continue;
                        }else{
                            totalUmur = totalUmur + listjenisnormal.get(i).getUmur();                            
                            countNormal++;
                            stdXUmurKuadrat = stdXUmurKuadrat + Math.pow(listjenisnormal.get(i).getUmur(),2);
                            stdXUmur = stdXUmur + listjenisnormal.get(i).getUmur();

                        }
                        
                    }

                    meanAnak = totalJumlahAnak / countNormalJenisAnak;
                    meanWanita = totalJumlahWanita / countNormalJenisWanita;
                    meanPria = totalJumlahPria / countNormalJenisPria;
                    meanSepatu = totalJumlahSepatu / countNormalJenisSepatu;
                    meanUmur = totalUmur / countNormal;
                    stdAnak = Math.sqrt(((countNormalJenisAnak * stdXJumlahAnakKuadrat)-Math.pow(stdXJumlahAnak,2))/(countNormalJenisAnak*(countNormalJenisAnak-1)));
                    stdWanita = Math.sqrt(((countNormalJenisWanita * stdXJumlahWanitaKuadrat)-Math.pow(stdXJumlahWanita,2))/(countNormalJenisWanita*(countNormalJenisWanita-1)));
                    stdPria = Math.sqrt(((countNormalJenisPria * stdXJumlahPriaKuadrat)-Math.pow(stdXJumlahPria,2))/(countNormalJenisPria*(countNormalJenisPria-1)));
                    stdSepatu = Math.sqrt(((countNormalJenisSepatu * stdXJumlahSepatuKuadrat)-Math.pow(stdXJumlahSepatu,2))/(countNormalJenisSepatu*(countNormalJenisSepatu-1)));
                    stdUmur = Math.sqrt(((countNormal * stdXUmurKuadrat)-Math.pow(stdXUmur,2))/(countNormal*(countNormal-1)));
                    
                    for(int j=0;j<listjenisnormal.size();j++){
                        if (listjenisnormal.get(j).getKel_jns().equals("A")) {
                            Double hslNormalJumlah = new Double((listjenisnormal.get(j).getJumlah() - meanAnak) / stdAnak);
                            hslNormalJumlah =  hslNormalJumlah * 10000 / 10000;
                            listjenisnormal.get(j).setJumlahnormal(hslNormalJumlah);                        
                        }else if (listjenisnormal.get(j).getKel_jns().equals("W")) {
                            Double hslNormalJumlah = new Double((listjenisnormal.get(j).getJumlah() - meanWanita) / stdWanita);
                            hslNormalJumlah =  hslNormalJumlah * 10000 / 10000;
                            listjenisnormal.get(j).setJumlahnormal(hslNormalJumlah);                        
                        }else if (listjenisnormal.get(j).getKel_jns().equals("P")){
                            Double hslNormalJumlah = new Double((listjenisnormal.get(j).getJumlah() - meanPria) / stdPria);
                            hslNormalJumlah = hslNormalJumlah * 10000 / 10000;
                            listjenisnormal.get(j).setJumlahnormal(hslNormalJumlah);
                        }else{
                            Double hslNormalJumlah = new Double((listjenisnormal.get(j).getJumlah() - meanSepatu) / stdSepatu);
                            hslNormalJumlah =  hslNormalJumlah * 10000 / 10000;
                            listjenisnormal.get(j).setJumlahnormal(hslNormalJumlah);
                        }
                        
                        Double hslNormalUmur = new Double((listjenisnormal.get(j).getUmur() - meanUmur) / stdUmur);                        
                        hslNormalUmur =  hslNormalUmur * 10000 / 10000;
                        listjenisnormal.get(j).setUmurnormal(hslNormalUmur);

                    }
            }


            SerializedObject se1 = new SerializedObject();
            DBConnector connector1 = new DBConnector();
            Connection conn1 = connector1.getConn();
            conn1.setAutoCommit(false);
            se1.writeNormal(conn1, namaperiod, listjenisnormal, Integer.parseInt(periodselect));
            conn1.commit();
  
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    public String normalisasi1(){
        
        try{  
        //listnormal = null;
        
        this.displaySelectFakta();
        this.getDataFakta();
        
        //Map<String, Object> session = ActionContext.getContext().getSession();
        //list = new ArrayList<>((ArrayList<Bon_Transaction>)session.get("FACT"));
        //listnormal = new ArrayList<>((ArrayList<Bon_Transaction>)session.get("FACT"));
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        namaperiod = (String) session.get("NAMAPER");
        
        SerializedObject se = new SerializedObject();
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        conn.setAutoCommit(false);
        modelfakta = se.readMetodeFakta(conn, Integer.parseInt(periodselect));
        Object o = se.readFakta(conn, Integer.parseInt(periodselect));
        conn.commit();

        if (modelfakta.equals("0")) {
            listnormal = new ArrayList<>((ArrayList<Bon_Transaction>)o);
        
            if (metodeselect.equals("0")) {
                    Double maxUmur = listnormal.get(listnormal.size()-1).getUmur();
                    Double minUmur = listnormal.get(0).getUmur();
                    Double minJumlah = listnormal.get(0).getJumlah();
                    Double maxJumlah = listnormal.get(listnormal.size()-1).getJumlah();

                /*
                int maxUmur = Integer.parseInt(listnormal.get(listnormal.size()-1).getUmur());
                int minUmur = Integer.parseInt(listnormal.get(0).getUmur());
                int minJumlah = Integer.parseInt(listnormal.get(0).getJumlah());
                int maxJumlah = Integer.parseInt(listnormal.get(listnormal.size()-1).getJumlah());
                */
                /*
                for (int i = 0; i < listnormal.size(); i++) {
                    if (Integer.parseInt(listnormal.get(i).getJumlah()) < minJumlah) {
                        minJumlah = Integer.parseInt(listnormal.get(i).getJumlah());
                    }else if(Integer.parseInt(listnormal.get(i).getUmur()) < minUmur){
                        minUmur = Integer.parseInt(listnormal.get(i).getUmur());
                    }else if(Integer.parseInt(listnormal.get(i).getJumlah()) > maxJumlah){
                        maxJumlah = Integer.parseInt(listnormal.get(i).getJumlah());
                    }else if(Integer.parseInt(listnormal.get(i).getUmur()) > maxUmur){
                        maxUmur = Integer.parseInt(listnormal.get(i).getUmur());
                    }
                }*/
                for (int i = 0; i < listnormal.size(); i++) {
                    if (listnormal.get(i).getJumlah() < minJumlah) {
                        minJumlah = listnormal.get(i).getJumlah();
                    }else if(listnormal.get(i).getUmur() < minUmur){
                        minUmur = listnormal.get(i).getUmur();
                    }else if(listnormal.get(i).getJumlah() > maxJumlah){
                        maxJumlah = listnormal.get(i).getJumlah();
                    }else if(listnormal.get(i).getUmur() > maxUmur){
                        maxUmur = listnormal.get(i).getUmur();
                    }
                }

                this.minNormUmur = minUmur;
                this.maxNormUmur = maxUmur;
                this.minNormJumlah = minJumlah;
                this.maxNormJumlah = maxJumlah;

                Double difjumlah = this.maxNormJumlah - this.minNormJumlah;
                Double difumur = this.maxNormUmur - this.minNormUmur;
                Double hasilUmur; 
                Double hasilJumlah; 

                /*
                for (int i = 0; i < listnormal.size(); i++) {
                    hasilUmur = (((float)Integer.parseInt(listnormal.get(i).getUmur()) - (float)minNormUmur) / (float)difumur) * 10;
                    hasilJumlah = (((float)Integer.parseInt(listnormal.get(i).getJumlah()) - (float)minNormJumlah) / (float)difjumlah) * 10;

                    listnormal.get(i).setUmurnormal(Float.toString(hasilUmur));
                    listnormal.get(i).setJumlahnormal(Float.toString(hasilJumlah));            
                }*/
                for (int i = 0; i < listnormal.size(); i++) {
                    hasilUmur = ((listnormal.get(i).getUmur() - minNormUmur)/difumur) * 1;
                    hasilJumlah = ((listnormal.get(i).getJumlah()- minNormJumlah)/difjumlah) * 1;             
                    listnormal.get(i).setUmurnormal(hasilUmur);
        //            if (listnormal.get(i).getKel_jns().equals("A")) {                
        //                listnormal.get(i).setJumlahnormal(hasilJumlah);
        //            }else if (listnormal.get(i).getKel_jns().equals("W")) {                
        //                listnormal.get(i).setJumlahnormal(hasilJumlah * 2);
        //            }else if (listnormal.get(i).getKel_jns().equals("P")) {                
        //                listnormal.get(i).setJumlahnormal(hasilJumlah * 3);
        //            }else if (listnormal.get(i).getKel_jns().equals("S")) {                
        //                listnormal.get(i).setJumlahnormal(hasilJumlah * 4);
        //            }            
                    listnormal.get(i).setJumlahnormal(hasilJumlah);
                }
                }else if (metodeselect.equals("1")) {
        //          float[][] newdata=new float[1500][1500];
        //          int j,i,jumdat=counterdata/jumkol;
                    Double totalUmur = new Double(0.0);
                    Double totalJumlah = new Double(0.0);
                    Double meanUmur = new Double(0.0);
                    Double meanAnak = new Double(0.0);
                    Double meanWanita = new Double(0.0);
                    Double meanPria = new Double(0.0);
                    Double meanSepatu = new Double(0.0);
                    Double stdUmur = new Double(0.0);
                    Double stdJumlah = new Double(0.0);
                    Double stdAnak = new Double(0.0);
                    Double stdPria = new Double(0.0);
                    Double stdWanita = new Double(0.0);
                    Double stdSepatu = new Double(0.0);
                    Double stdXUmur = new Double(0.0);
                    Double stdXUmurKuadrat = new Double(0.0);
                    Double stdXJumlah = new Double(0.0);
                    Double stdXJumlahKuadrat = new Double(0.0);
                    Double stdXJumlahAnak = new Double(0.0);
                    Double stdXJumlahAnakKuadrat = new Double(0.0);
                    Double stdXJumlahWanita = new Double(0.0);
                    Double stdXJumlahWanitaKuadrat = new Double(0.0);
                    Double stdXJumlahPria = new Double(0.0);
                    Double stdXJumlahPriaKuadrat = new Double(0.0);
                    Double stdXJumlahSepatu = new Double(0.0);
                    Double stdXJumlahSepatuKuadrat = new Double(0.0);

                    
                    Double totalJumlahAnak = new Double(0.0);
                    Double totalJumlahWanita = new Double(0.0);
                    Double totalJumlahPria = new Double(0.0);
                    Double totalJumlahSepatu = new Double(0.0);
                    
        //          cari mean
                    int countNormal = 0;
                    int countNormalJenisAnak = 0;
                    int countNormalJenisWanita = 0;
                    int countNormalJenisPria = 0;
                    int countNormalJenisSepatu = 0;
                    for (int i = 0; i < listnormal.size(); i++) {
                        //jenis
                        if (listnormal.get(i).getKel_jns().equals("A")) {
                            totalJumlahAnak = totalJumlahAnak + listnormal.get(i).getJumlah();
                            stdXJumlahAnak = stdXJumlahAnak + listnormal.get(i).getJumlah();
                            stdXJumlahAnakKuadrat = stdXJumlahAnakKuadrat + Math.pow(listnormal.get(i).getJumlah(),2);                    
                            countNormalJenisAnak++;
                        }else if (listnormal.get(i).getKel_jns().equals("W")) {
                            totalJumlahWanita = totalJumlahWanita + listnormal.get(i).getJumlah();                            
                            stdXJumlahWanita = stdXJumlahWanita + listnormal.get(i).getJumlah();
                            stdXJumlahWanitaKuadrat = stdXJumlahWanitaKuadrat + Math.pow(listnormal.get(i).getJumlah(),2);
                            countNormalJenisWanita++;

                        }else if (listnormal.get(i).getKel_jns().equals("P")){
                            totalJumlahPria = totalJumlahPria + listnormal.get(i).getJumlah();
                            stdXJumlahPria = stdXJumlahPria + listnormal.get(i).getJumlah();
                            stdXJumlahPriaKuadrat = stdXJumlahPriaKuadrat + Math.pow(listnormal.get(i).getJumlah(),2);
                            countNormalJenisPria++;
                        }else{
                            totalJumlahSepatu = totalJumlahSepatu + listnormal.get(i).getJumlah();
                            stdXJumlahSepatu = stdXJumlahSepatu + listnormal.get(i).getJumlah();
                            stdXJumlahSepatuKuadrat = stdXJumlahSepatuKuadrat + Math.pow(listnormal.get(i).getJumlah(),2);
                            countNormalJenisSepatu++;

                        }
                        
                        if (i==0) {
                            totalUmur = totalUmur +listnormal.get(i).getUmur();
                            stdXUmurKuadrat = stdXUmurKuadrat + Math.pow(listnormal.get(i).getUmur(),2);
                            stdXUmur = stdXUmur + listnormal.get(i).getUmur();
                            countNormal++;
                            continue;
                        }
                        if (listnormal.get(i).getNo_bon().equals(listnormal.get(i-1).getNo_bon())) {
                            continue;
                        }else{
                            totalUmur = totalUmur + listnormal.get(i).getUmur();                            
                            countNormal++;
                            stdXUmurKuadrat = stdXUmurKuadrat + Math.pow(listnormal.get(i).getUmur(),2);
                            stdXUmur = stdXUmur + listnormal.get(i).getUmur();

                        }
                        
                    }

                    meanAnak = totalJumlahAnak / countNormalJenisAnak;
                    meanWanita = totalJumlahWanita / countNormalJenisWanita;
                    meanPria = totalJumlahPria / countNormalJenisPria;
                    meanSepatu = totalJumlahSepatu / countNormalJenisSepatu;
                    meanUmur = totalUmur / countNormal;

        //            System.out.println("meanJumlah" + meanJumlah);
        //            System.out.println("meanUmur" + meanUmur);

        //          cari std
//                    for (int i = 0; i < listnormal.size(); i++) {
//                        stdXUmur = stdXUmur + listnormal.get(i).getUmur();
//                        stdXJumlah = stdXJumlah + listnormal.get(i).getJumlah();
//                        stdXUmurKuadrat = stdXUmurKuadrat + Math.pow(listnormal.get(i).getUmur(),2);
//                        stdXJumlahKuadrat = stdXJumlahKuadrat + Math.pow(listnormal.get(i).getJumlah(),2);
//                    }

        //            System.out.println("stdXUmur" + stdXUmur );
        //            System.out.println("stdXUmurKuadrat" + stdXUmurKuadrat);
        //            System.out.println("stdXJumlah" + stdXJumlah );
        //            System.out.println("stdXJumlahKuadrat" + stdXJumlahKuadrat);
        //            System.out.println("size" + listnormal.size());
        //            System.out.println("coba1 " + (listnormal.size() * stdXJumlahKuadrat));
        //            System.out.println("coba2 " + Math.pow(stdXJumlah,2));
        //            System.out.println("coba3 " + listnormal.size()*(listnormal.size()-1));
        //            System.out.println("Tes " + ((listnormal.size() * stdXJumlahKuadrat)-Math.pow(stdXJumlah,2))/listnormal.size()*(listnormal.size()-1));

                    stdAnak = Math.sqrt(((totalJumlahAnak * stdXJumlahAnakKuadrat)-Math.pow(stdXJumlahAnak,2))/(totalJumlahAnak*(totalJumlahAnak-1)));
                    stdWanita = Math.sqrt(((totalJumlahWanita * stdXJumlahWanitaKuadrat)-Math.pow(stdXJumlahWanita,2))/(totalJumlahWanita*(totalJumlahWanita-1)));
                    stdPria = Math.sqrt(((totalJumlahPria * stdXJumlahPriaKuadrat)-Math.pow(stdXJumlahPria,2))/(totalJumlahPria*(totalJumlahPria-1)));
                    stdSepatu = Math.sqrt(((totalJumlahSepatu * stdXJumlahSepatuKuadrat)-Math.pow(stdXJumlahSepatu,2))/(totalJumlahSepatu*(totalJumlahSepatu-1)));
                    stdUmur = Math.sqrt(((totalUmur * stdXUmurKuadrat)-Math.pow(stdXUmur,2))/(totalUmur*(totalUmur-1)));


        //            System.out.println("stdJumlah" + stdJumlah );
        //            System.out.println("stdUmur" + stdUmur);

                    for(int j=0;j<listnormal.size();j++){
                        if (listnormal.get(j).getKel_jns().equals("A")) {
                            Double hslNormalJumlah = new Double((listnormal.get(j).getJumlah() - meanAnak) / stdAnak);
                            listnormal.get(j).setJumlahnormal(hslNormalJumlah);                        
                        }else if (listnormal.get(j).getKel_jns().equals("W")) {
                            Double hslNormalJumlah = new Double((listnormal.get(j).getJumlah() - meanWanita) / stdWanita);
                            listnormal.get(j).setJumlahnormal(hslNormalJumlah);                        
                        }else if (listnormal.get(j).getKel_jns().equals("P")){
                            Double hslNormalJumlah = new Double((listnormal.get(j).getJumlah() - meanPria) / stdPria);
                            listnormal.get(j).setJumlahnormal(hslNormalJumlah);
                        }else{
                            Double hslNormalJumlah = new Double((listnormal.get(j).getJumlah() - meanSepatu) / stdSepatu);
                            listnormal.get(j).setJumlahnormal(hslNormalJumlah);
                        }
                        
                        Double hslNormalUmur = new Double((listnormal.get(j).getUmur() - meanUmur) / stdUmur);                        
                        listnormal.get(j).setUmurnormal(hslNormalUmur);

                    }
                }


                SerializedObject se1 = new SerializedObject();
                DBConnector connector1 = new DBConnector();
                Connection conn1 = connector1.getConn();
                conn1.setAutoCommit(false);
//                se1.writeNormal(conn1, namaperiod, listnormal, Integer.parseInt(periodselect), Integer.parseInt(modelfakta));
                conn1.commit();

        }else{
            listjenisnormal = new ArrayList<>((ArrayList<BonJenis_Transaction>)o);
        
            if (metodeselect.equals("0")) {
                Double maxUmur = Double.MIN_VALUE;
                Double minUmur = Double.MAX_VALUE;
                Double minJumlah = Double.MAX_VALUE;
                Double maxJumlah = Double.MIN_VALUE;
                Double minJumlahAnak = Double.MAX_VALUE;
                Double maxJumlahAnak = Double.MIN_VALUE;
                Double minJumlahWanita = Double.MAX_VALUE;
                Double maxJumlahWanita = Double.MIN_VALUE;
                Double minJumlahPria = Double.MAX_VALUE;
                Double maxJumlahPria = Double.MIN_VALUE;
                Double minJumlahSepatu = Double.MAX_VALUE;
                Double maxJumlahSepatu = Double.MIN_VALUE;

            for (int i = 0; i < listjenisnormal.size(); i++) {
                
                if (listjenisnormal.get(i).getKel_jns().equals("A")) {
                    if (listjenisnormal.get(i).getJumlah() < minJumlahAnak) {
                        minJumlahAnak = listjenisnormal.get(i).getJumlah();
                    }else if(listjenisnormal.get(i).getJumlah() > maxJumlahAnak){
                        maxJumlahAnak = listjenisnormal.get(i).getJumlah();
                    }                    
                }else if(listjenisnormal.get(i).getKel_jns().equals("W")){
                    if (listjenisnormal.get(i).getJumlah() < minJumlahWanita) {
                        minJumlahWanita = listjenisnormal.get(i).getJumlah();
                    }else if(listjenisnormal.get(i).getJumlah() > maxJumlahWanita){
                        maxJumlahWanita = listjenisnormal.get(i).getJumlah();
                    }                    
                }else if(listjenisnormal.get(i).getKel_jns().equals("P")){
                    if (listjenisnormal.get(i).getJumlah() < minJumlahPria) {
                        minJumlahPria = listjenisnormal.get(i).getJumlah();
                    }else if(listjenisnormal.get(i).getJumlah() > maxJumlahPria){
                        maxJumlahPria = listjenisnormal.get(i).getJumlah();
                    }                                        
                }else if(listjenisnormal.get(i).getKel_jns().equals("S")){
                    if (listjenisnormal.get(i).getJumlah() < minJumlahSepatu) {
                        minJumlahSepatu = listjenisnormal.get(i).getJumlah();
                    }else if(listjenisnormal.get(i).getJumlah() > maxJumlahSepatu){
                        maxJumlahSepatu = listjenisnormal.get(i).getJumlah();
                    }                                        
                    
                }

                if (listjenisnormal.get(i).getUmur()< minUmur) {
                        minUmur = listjenisnormal.get(i).getUmur();
                }else if(listjenisnormal.get(i).getUmur() > maxUmur){
                        maxUmur = listjenisnormal.get(i).getUmur();
                }
            }

            this.minNormUmur = minUmur;
            this.maxNormUmur = maxUmur;
            this.minNormJumlahAnak = minJumlahAnak;
            this.maxNormJumlahAnak = maxJumlahAnak;
            this.minNormJumlahWanita = minJumlahWanita;
            this.maxNormJumlahWanita = maxJumlahWanita;
            this.minNormJumlahPria = minJumlahPria;
            this.maxNormJumlahPria = maxJumlahPria;
            this.minNormJumlahSepatu = minJumlahSepatu;
            this.maxNormJumlahSepatu = maxJumlahSepatu;

            Double difjumlahanak = this.maxNormJumlahAnak - this.minNormJumlahAnak;
            Double difjumlahwanita = this.maxNormJumlahWanita - this.minNormJumlahWanita;
            Double difjumlahpria = this.maxNormJumlahPria - this.minNormJumlahPria;
            Double difjumlahsepatu = this.maxNormJumlahSepatu - this.minNormJumlahSepatu;
            Double difumur = this.maxNormUmur - this.minNormUmur;
            Double hasilUmur = 0.0; 
            Double hasilJumlahAnak = 0.0;
            Double hasilJumlahWanita = 0.0;
            Double hasilJumlahPria = 0.0;
            Double hasilJumlahSepatu = 0.0; 

            for (int i = 0; i < listjenisnormal.size(); i++) {
                hasilUmur = ((listjenisnormal.get(i).getUmur() - minNormUmur)/difumur) * 1;
                if (listjenisnormal.get(i).getKel_jns().equals("A")) {
                    hasilJumlahAnak = ((listjenisnormal.get(i).getJumlah()- minNormJumlahAnak)/difjumlahanak) * 1;
                    if (Double.isNaN(hasilJumlahAnak)) {
                        
                        listjenisnormal.get(i).setJumlahnormal(0.0);                    
                    }else{
                        hasilJumlahAnak =  hasilJumlahAnak * 10000 / 10000;
                        listjenisnormal.get(i).setJumlahnormal(hasilJumlahAnak);                    
                    }
                    
                }else if(listjenisnormal.get(i).getKel_jns().equals("W")){
                    hasilJumlahWanita = ((listjenisnormal.get(i).getJumlah()- minNormJumlahWanita)/difjumlahwanita) * 1;             
                    if (Double.isNaN(hasilJumlahWanita)) {
                        listjenisnormal.get(i).setJumlahnormal(0.0);

                    }else{
                        hasilJumlahWanita =  hasilJumlahWanita * 10000 / 10000;
                        listjenisnormal.get(i).setJumlahnormal(hasilJumlahWanita);
                    }
                }else if(listjenisnormal.get(i).getKel_jns().equals("P")){
                    hasilJumlahPria = ((listjenisnormal.get(i).getJumlah()- minNormJumlahPria)/difjumlahpria) * 1;            
                    if (Double.isNaN(hasilJumlahPria)) {
                        listjenisnormal.get(i).setJumlahnormal(0.0);

                    }else{
                        hasilJumlahPria =  hasilJumlahPria * 10000 / 10000;
                        listjenisnormal.get(i).setJumlahnormal(hasilJumlahPria);                    
                    }                    
                }else if(listjenisnormal.get(i).getKel_jns().equals("S")){
                    hasilJumlahSepatu = ((listjenisnormal.get(i).getJumlah()- minNormJumlahSepatu)/difjumlahsepatu) * 1;             
                    listjenisnormal.get(i).setJumlahnormal(hasilJumlahSepatu);                    
                    if (Double.isNaN(hasilJumlahSepatu)) {
                        listjenisnormal.get(i).setJumlahnormal(0.0);

                    }else{
                        hasilJumlahSepatu =  hasilJumlahSepatu * 10000 / 10000;
                        listjenisnormal.get(i).setJumlahnormal(hasilJumlahSepatu);                    
                    }                    

                }
                hasilUmur =  hasilUmur * 10000 / 10000;
                listjenisnormal.get(i).setUmurnormal(hasilUmur);

            }
            
            }else if (metodeselect.equals("1")) {
    
                    Double totalUmur = new Double(0.0);
                    Double totalJumlah = new Double(0.0);
                    Double meanUmur = new Double(0.0);
                    Double meanAnak = new Double(0.0);
                    Double meanWanita = new Double(0.0);
                    Double meanPria = new Double(0.0);
                    Double meanSepatu = new Double(0.0);
                    Double stdUmur = new Double(0.0);
                    Double stdJumlah = new Double(0.0);
                    Double stdAnak = new Double(0.0);
                    Double stdPria = new Double(0.0);
                    Double stdWanita = new Double(0.0);
                    Double stdSepatu = new Double(0.0);
                    Double stdXUmur = new Double(0.0);
                    Double stdXUmurKuadrat = new Double(0.0);
                    Double stdXJumlah = new Double(0.0);
                    Double stdXJumlahKuadrat = new Double(0.0);
                    Double stdXJumlahAnak = new Double(0.0);
                    Double stdXJumlahAnakKuadrat = new Double(0.0);
                    Double stdXJumlahWanita = new Double(0.0);
                    Double stdXJumlahWanitaKuadrat = new Double(0.0);
                    Double stdXJumlahPria = new Double(0.0);
                    Double stdXJumlahPriaKuadrat = new Double(0.0);
                    Double stdXJumlahSepatu = new Double(0.0);
                    Double stdXJumlahSepatuKuadrat = new Double(0.0);

                    
                    Double totalJumlahAnak = new Double(0.0);
                    Double totalJumlahWanita = new Double(0.0);
                    Double totalJumlahPria = new Double(0.0);
                    Double totalJumlahSepatu = new Double(0.0);
                    
        //          cari mean
                    int countNormal = 0;
                    int countNormalJenisAnak = 0;
                    int countNormalJenisWanita = 0;
                    int countNormalJenisPria = 0;
                    int countNormalJenisSepatu = 0;
                    for (int i = 0; i < listjenisnormal.size(); i++) {
                        //jenis
                        if (listjenisnormal.get(i).getKel_jns().equals("A")) {
                            totalJumlahAnak = totalJumlahAnak + listjenisnormal.get(i).getJumlah();
                            stdXJumlahAnak = stdXJumlahAnak + listjenisnormal.get(i).getJumlah();
                            stdXJumlahAnakKuadrat = stdXJumlahAnakKuadrat + Math.pow(listjenisnormal.get(i).getJumlah(),2);                    
                            countNormalJenisAnak++;
                        }else if (listjenisnormal.get(i).getKel_jns().equals("W")) {
                            totalJumlahWanita = totalJumlahWanita + listjenisnormal.get(i).getJumlah();                            
                            stdXJumlahWanita = stdXJumlahWanita + listjenisnormal.get(i).getJumlah();
                            stdXJumlahWanitaKuadrat = stdXJumlahWanitaKuadrat + Math.pow(listjenisnormal.get(i).getJumlah(),2);
                            countNormalJenisWanita++;

                        }else if (listjenisnormal.get(i).getKel_jns().equals("P")){
                            totalJumlahPria = totalJumlahPria + listjenisnormal.get(i).getJumlah();
                            stdXJumlahPria = stdXJumlahPria + listjenisnormal.get(i).getJumlah();
                            stdXJumlahPriaKuadrat = stdXJumlahPriaKuadrat + Math.pow(listjenisnormal.get(i).getJumlah(),2);
                            countNormalJenisPria++;
                        }else{
                            totalJumlahSepatu = totalJumlahSepatu + listjenisnormal.get(i).getJumlah();
                            stdXJumlahSepatu = stdXJumlahSepatu + listjenisnormal.get(i).getJumlah();
                            stdXJumlahSepatuKuadrat = stdXJumlahSepatuKuadrat + Math.pow(listjenisnormal.get(i).getJumlah(),2);
                            countNormalJenisSepatu++;

                        }
                        
                        if (i==0) {
                            totalUmur = totalUmur +listjenisnormal.get(i).getUmur();
                            stdXUmurKuadrat = stdXUmurKuadrat + Math.pow(listjenisnormal.get(i).getUmur(),2);
                            stdXUmur = stdXUmur + listjenisnormal.get(i).getUmur();
                            countNormal++;
                            continue;
                        }
                        if (listjenisnormal.get(i).getNo_bon().equals(listjenisnormal.get(i-1).getNo_bon())) {
                            continue;
                        }else{
                            totalUmur = totalUmur + listjenisnormal.get(i).getUmur();                            
                            countNormal++;
                            stdXUmurKuadrat = stdXUmurKuadrat + Math.pow(listjenisnormal.get(i).getUmur(),2);
                            stdXUmur = stdXUmur + listjenisnormal.get(i).getUmur();

                        }
                        
                    }

                    meanAnak = totalJumlahAnak / countNormalJenisAnak;
                    meanWanita = totalJumlahWanita / countNormalJenisWanita;
                    meanPria = totalJumlahPria / countNormalJenisPria;
                    meanSepatu = totalJumlahSepatu / countNormalJenisSepatu;
                    meanUmur = totalUmur / countNormal;
                    
                    /*
                    System.out.println("Mean Umur" + meanUmur);
                    System.out.println("Mean Wanita" + meanWanita);
                    System.out.println("Mean Pria" + meanPria);
                    System.out.println("Mean Sepatu" + meanSepatu);
                    System.out.println("Mean Anak" + meanAnak);
                    
                    
                    System.out.println("Total Jumlah Anak" + totalJumlahAnak);
                    System.out.println("Total STDX Anak" + stdXJumlahAnak);
                    System.out.println("Total STDX Anak Kuadrat" + stdXJumlahAnakKuadrat);

                    System.out.println("Total Umur " + totalUmur);
                    System.out.println("Total STDX " + stdXUmur);
                    System.out.println("Total STDX  Kuadrat" + stdXUmurKuadrat);

                    
                    System.out.println("Total Count Anak" + countNormalJenisAnak);
                    System.out.println("Total Count" + countNormal);
                    System.out.println("Total Count Pra" + countNormalJenisPria);
                    System.out.println("Total Count Sepatu" + countNormalJenisSepatu);
                    System.out.println("Total Count Wanita" + countNormalJenisWanita);
                    */
                    
                    
                    stdAnak = Math.sqrt(((countNormalJenisAnak * stdXJumlahAnakKuadrat)-Math.pow(stdXJumlahAnak,2))/(countNormalJenisAnak*(countNormalJenisAnak-1)));
                    stdWanita = Math.sqrt(((countNormalJenisWanita * stdXJumlahWanitaKuadrat)-Math.pow(stdXJumlahWanita,2))/(countNormalJenisWanita*(countNormalJenisWanita-1)));
                    stdPria = Math.sqrt(((countNormalJenisPria * stdXJumlahPriaKuadrat)-Math.pow(stdXJumlahPria,2))/(countNormalJenisPria*(countNormalJenisPria-1)));
                    stdSepatu = Math.sqrt(((countNormalJenisSepatu * stdXJumlahSepatuKuadrat)-Math.pow(stdXJumlahSepatu,2))/(countNormalJenisSepatu*(countNormalJenisSepatu-1)));
                    stdUmur = Math.sqrt(((countNormal * stdXUmurKuadrat)-Math.pow(stdXUmur,2))/(countNormal*(countNormal-1)));
                    
                    for(int j=0;j<listjenisnormal.size();j++){
                        if (listjenisnormal.get(j).getKel_jns().equals("A")) {
                            Double hslNormalJumlah = new Double((listjenisnormal.get(j).getJumlah() - meanAnak) / stdAnak);
                            hslNormalJumlah =  hslNormalJumlah * 10000 / 10000;
                            listjenisnormal.get(j).setJumlahnormal(hslNormalJumlah);                        
                        }else if (listjenisnormal.get(j).getKel_jns().equals("W")) {
                            Double hslNormalJumlah = new Double((listjenisnormal.get(j).getJumlah() - meanWanita) / stdWanita);
                            hslNormalJumlah =  hslNormalJumlah * 10000 / 10000;
                            listjenisnormal.get(j).setJumlahnormal(hslNormalJumlah);                        
                        }else if (listjenisnormal.get(j).getKel_jns().equals("P")){
                            Double hslNormalJumlah = new Double((listjenisnormal.get(j).getJumlah() - meanPria) / stdPria);
                            hslNormalJumlah = hslNormalJumlah * 10000 / 10000;
                            listjenisnormal.get(j).setJumlahnormal(hslNormalJumlah);
                        }else{
                            Double hslNormalJumlah = new Double((listjenisnormal.get(j).getJumlah() - meanSepatu) / stdSepatu);
                            hslNormalJumlah =  hslNormalJumlah * 10000 / 10000;
                            listjenisnormal.get(j).setJumlahnormal(hslNormalJumlah);
                        }
                        
                        Double hslNormalUmur = new Double((listjenisnormal.get(j).getUmur() - meanUmur) / stdUmur);                        
                        hslNormalUmur =  hslNormalUmur * 10000 / 10000;
                        listjenisnormal.get(j).setUmurnormal(hslNormalUmur);

                    }
                }


            SerializedObject se1 = new SerializedObject();
            DBConnector connector1 = new DBConnector();
            Connection conn1 = connector1.getConn();
            conn1.setAutoCommit(false);
//            se1.writeNormal(conn1, namaperiod, listjenisnormal, Integer.parseInt(periodselect), Integer.parseInt(modelfakta));
            conn1.commit();

        }
        //session.put("NORMAL", listnormal);
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
            
    private Statement stat;
    private ResultSet rs;
    
    public String getInfoToko(){
        try {
            //stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
            stat = new HiveConnector("192.168.1.100", "hive", "", "abadi").getConn().createStatement();
            rs= stat.executeQuery("select * from lokasi");
            infotoko = new ArrayList();
            while(rs.next()){  
             InfoToko toko = new InfoToko();
             toko.setNama_toko(rs.getString(1));
             toko.setKd_lokasi(rs.getString(2));
             infotoko.add(toko); 
            }  
        
        stat.close();
        } catch (Exception e) {
             e.printStackTrace();
        }
        return SUCCESS;
    }
    public String getInfoStrip(){
        try {
            //stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
            stat = new HiveConnector("192.168.1.100", "hive", "", "abadi").getConn().createStatement();
            rs= stat.executeQuery("select * from strip");
            infostrip = new ArrayList();
            while(rs.next()){  
             InfoStrip strip = new InfoStrip();
             strip.setKd_strip(rs.getString(1));
             strip.setNama_strip(rs.getString(2));
             strip.setKel_jenis(rs.getString(3));
             infostrip.add(strip); 
            }  
        
        stat.close();
        } catch (Exception e) {
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String getInfoPelanggan(){
        try {
            //stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
            stat = new HiveConnector("192.168.1.100", "hive", "", "abadi").getConn().createStatement();
            rs= stat.executeQuery("select * from pelanggan");
            infopelanggan = new ArrayList();
            while(rs.next()){  
                InfoPelanggan pelanggan = new InfoPelanggan();
                pelanggan.setKd_customer(rs.getString(1));
                pelanggan.setNama(rs.getString(2));
                pelanggan.setTgl_lahir(rs.getString(3));
                pelanggan.setJns_kelamin(rs.getString(4));
                infopelanggan.add(pelanggan); 
            }  
        
        stat.close();
        } catch (Exception e) {
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String getInfoNotaBon(){
        try {
            stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
            //stat = new HiveConnector("192.168.1.100", "hive", "", "abadi").getConn().createStatement();
            rs= stat.executeQuery("select * from bon_fact_jenis3");
            infonotabon = new ArrayList();
            while(rs.next()){  
                InfoNotaBon infobon = new InfoNotaBon();
                infobon.setTanggal(rs.getString(1));
                infobon.setNo_bon(rs.getString(2));
                infobon.setKd_customer(rs.getString(3));
                infobon.setUmur(rs.getString(4));
                infobon.setKd_lokasi(rs.getString(5));
                infobon.setKel_jenis(rs.getString(6));
                infobon.setJumlah(rs.getString(7));
                infonotabon.add(infobon); 
            }
        
        stat.close();
        } catch (Exception e) {
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String getInfoNotaTunai(){
        try {
            stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
            //stat = new HiveConnector("192.168.1.100", "hive", "", "abadi").getConn().createStatement();
            rs= stat.executeQuery("select * from nota_fact_jenis");
            infonotatunai = new ArrayList();
            while(rs.next()){  
                InfoNotaTunai infotunai = new InfoNotaTunai();
                infotunai.setTanggal(rs.getString(1));
                infotunai.setNo_tunai(rs.getString(2));
                infotunai.setKd_customer(rs.getString(3));
                infotunai.setUmur(rs.getString(4));
                infotunai.setKd_lokasi(rs.getString(5));
                infotunai.setKel_jenis(rs.getString(6));
                infotunai.setJumlah(rs.getString(7));
                infonotatunai.add(infotunai); 
            }
        
        stat.close();
        } catch (Exception e) {
             e.printStackTrace();
        }
        return SUCCESS;
    }

    
    public String getInfoBon(){
        try {
            stat = new HiveConnector("192.168.1.100", "hive", "", "abadi").getConn().createStatement();
            rs= stat.executeQuery("select * from bon_fact_jenis3");
            infonotabon = new ArrayList();
            while(rs.next()){  
                InfoNotaBon infobon = new InfoNotaBon();
                infobon.setTanggal(rs.getString(1));
                infobon.setNo_bon(rs.getString(2));
                infobon.setKd_customer(rs.getString(3));
                infobon.setUmur(rs.getString(4));
                infobon.setKd_lokasi(rs.getString(5));
                infobon.setKel_jenis(rs.getString(6));
                infobon.setJumlah(rs.getString(7));
                infonotabon.add(infobon); 
            }
        
        stat.close();
        } catch (Exception e) {
             e.printStackTrace();
        }
        return SUCCESS;

    }
    
    public String getInfoTunai(){
        try {
            stat = new HiveConnector("192.168.1.100", "hive", "", "abadi").getConn().createStatement();
            rs= stat.executeQuery("select * from nota_fact_jenis");
            infonotatunai = new ArrayList();
            while(rs.next()){  
                InfoNotaTunai infotunai = new InfoNotaTunai();
                infotunai.setTanggal(rs.getString(1));
                infotunai.setNo_tunai(rs.getString(2));
                infotunai.setKd_customer(rs.getString(3));
                infotunai.setUmur(rs.getString(4));
                infotunai.setKd_lokasi(rs.getString(5));
                infotunai.setKel_jenis(rs.getString(6));
                infotunai.setJumlah(rs.getString(7));
                infonotatunai.add(infotunai); 
            }
        
        stat.close();
        } catch (Exception e) {
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String getFactTable(){
        try{
        list = null;
        start = null;
        end = null;
        jenis_kelamin = null;    
        
        this.displaySelect();
        this.getDate();
//        if (start.equals("2010-01-01") && end.equals("2012-12-31")) {
//            stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
//        }else{
//            stat = new HiveConnector("192.168.1.100", "abadi", "", "abadi").getConn().createStatement();
//        }
        
        stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
        //stat = new HiveConnector("192.168.1.100", "abadi", "", "abadi").getConn().createStatement();
        if (jenis_nota.equals("B")) {
            if(!jenis_kelamin.equals("A")){
                String substring = start.substring(0,4);
                if (substring.equals("2010")) {                        
                    rs= stat.executeQuery("select * from bon_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 4 and 64");
                }else if (substring.equals("2011")) {
                    rs= stat.executeQuery("select * from bon_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 6 and 62");                
                }else{
                    rs= stat.executeQuery("select * from bon_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 8 and 59");                    
                }
            }else{
                String substring = start.substring(0,4);
                if (substring.equals("2010")) {
                    rs= stat.executeQuery("select * from bon_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 4 and 64");
                }else if (substring.equals("2011")) {
                    rs= stat.executeQuery("select * from bon_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 6 and 62");
                }else{
                    rs= stat.executeQuery("select * from bon_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 8 and 59");
                }

            }        
        }else{
            if(!jenis_kelamin.equals("A")){
                String substring = start.substring(start.lastIndexOf("-") + 1);
                if (substring.equals("2010")) {
                    rs= stat.executeQuery("select * from nota_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 7 and 71");
                }else if (substring.equals("2011")) {
                    rs= stat.executeQuery("select * from nota_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 13 and 64");
                }else{
                    rs= stat.executeQuery("select * from nota_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 7 and 71");
                }
            }else{
                String substring = start.substring(start.lastIndexOf("-") + 1);
                if (substring.equals("2010")) {
                    rs= stat.executeQuery("select * from nota_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 7 and 71");
                }else if (substring.equals("2011")) {
                    rs= stat.executeQuery("select * from nota_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 13 and 64");
                }else{
                    rs= stat.executeQuery("select * from nota_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 7 and 71");
                }
            }
        
        }
        
        //ResultSet rs= ps.executeQuery();
         listjenis = new ArrayList();
         while(rs.next()){  
          BonJenis_Transaction bon = new BonJenis_Transaction();
          bon.setTanggal(rs.getString(1));
          bon.setNo_bon(rs.getString(2));
          bon.setKd_customer(rs.getString(3));
          bon.setUmur(rs.getDouble(4));
          bon.setId_toko(rs.getString(5));
          bon.setKel_jns(rs.getString(6));
          bon.setJumlah(rs.getDouble(7));
          listjenis.add(bon); 
         }  
         SerializedObject se = new SerializedObject();
         DBConnector connector = new DBConnector();
         Connection conn = connector.getConn();
         conn.setAutoCommit(false);
         se.writeFakta(conn, namaperiod, listjenis,Integer.parseInt(periodselect));
         conn.commit();

         Map<String, Object> session = ActionContext.getContext().getSession();
         session.put("START", start);        
         session.put("END", end);
        
        stat.close();
                   
        }catch(Exception e){
         e.printStackTrace();
        }  
        return SUCCESS;
    }
    
    public String getFactTabel(){
        try{
            
        list = null;
        start = null;
        end = null;
        jenis_kelamin = null;    
        
        this.displaySelect();
        this.getDate();
        if (start.equals("2010-01-01") && end.equals("2012-12-31")) {
            stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
        }else{
            stat = new HiveConnector("192.168.1.100", "abadi", "", "abadi").getConn().createStatement();
        }
        
        
        //stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
        
        /*Connection conn = new HiveConnector("localhost", "hive", "", "default").getConn();
        PreparedStatement ps = conn.prepareStatement("select * from amigo.bon_fact limit ?,20");
        ps.setInt(1, 0);*/
        //ResultSet rs= stat.executeQuery("select * from bon_fact");
        //System.out.println("JK : " + jenis_kelamin);
//         bon.setTanggal(rs.getString(1));
//         bon.setNo_bon(rs.getString(2));
//         bon.setKd_customer(rs.getString(3));
//         bon.setUmur(rs.getDouble(4));
//         bon.setId_toko(rs.getString(5));
//         bon.setKd_strip(rs.getString(6));
//         bon.setJumlah(rs.getDouble(7));
        modelfakta = "1";
        if (modelfakta.equals("0")) {
            if (jenis_nota.equals("B")) {
                if(!jenis_kelamin.equals("A")){
                    String substring = start.substring(0,4);
                    if (substring.equals("2010")) {
                        rs= stat.executeQuery("select * from bon_fact b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 4 and 64");
                    }else if (substring.equals("2011")) {
                        rs= stat.executeQuery("select * from bon_fact b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 6 and 62");                
                    }else{
                        rs= stat.executeQuery("select * from bon_fact b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 8 and 59");                    
                    }
                }else{
                    String substring = start.substring(0,4);
                    if (substring.equals("2010")) {
                        rs= stat.executeQuery("select * from bon_fact where tanggal between '"+ start +"' and '" + end +"' and umur between 4 and 64");
                    }else if (substring.equals("2011")) {
                        rs= stat.executeQuery("select * from bon_fact where tanggal between '"+ start +"' and '" + end +"' and umur between 6 and 62");
                    }else{
                        rs= stat.executeQuery("select * from bon_fact where tanggal between '"+ start +"' and '" + end +"' and umur between 8 and 59");
                    }

                }        
            }else{
                if(!jenis_kelamin.equals("A")){
                    String substring = start.substring(start.lastIndexOf("-") + 1);
                    if (substring.equals("2010")) {
                        rs= stat.executeQuery("select * from nota_fact b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and '" + end +"' and b.umur between 7 and 71");
                    }else if (substring.equals("2011")) {
                        rs= stat.executeQuery("select * from nota_fact b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"'and '" + end +"' and b.umur between 13 and 64");
                    }else{
                        rs= stat.executeQuery("select * from nota_fact b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"'and '" + end +"' and b.umur between 7 and 71");
                    }
                }else{
                    String substring = start.substring(start.lastIndexOf("-") + 1);
                    if (substring.equals("2010")) {
                        rs= stat.executeQuery("select * from nota_fact where tanggal between '"+ start +"' and '" + end +"'and '" + end +"' and b.umur between 7 and 71");
                    }else if (substring.equals("2011")) {
                        rs= stat.executeQuery("select * from nota_fact where tanggal between '"+ start +"' and '" + end +"'and '" + end +"' and b.umur between 13 and 64");
                    }else{
                        rs= stat.executeQuery("select * from nota_fact where tanggal between '"+ start +"' and '" + end +"'and '" + end +"' and b.umur between 7 and 71");
                    }
                }
            }

            //ResultSet rs= ps.executeQuery();
            list = new ArrayList();
            while(rs.next()){  
             Bon_Transaction bon = new Bon_Transaction();
             bon.setTanggal(rs.getString(1));
             bon.setNo_bon(rs.getString(2));
             bon.setKd_customer(rs.getString(3));
             bon.setUmur(rs.getDouble(4));
             bon.setId_toko(rs.getString(5));
             bon.setKd_strip(rs.getString(6));
             bon.setJumlah(rs.getDouble(7));
             bon.setKel_jns(rs.getString(8));
             list.add(bon); 
            }  

            SerializedObject se = new SerializedObject();
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            conn.setAutoCommit(false);
//            se.writeFakta(conn, namaperiod, list,Integer.parseInt(periodselect),Integer.parseInt(modelfakta));
            conn.commit();

            Map<String, Object> session = ActionContext.getContext().getSession();
            //session.put("FACT", list);
            //session.put("NAMAPER", namaperiod);        
            session.put("START", start);        
            session.put("END", end);        
            //session.put("JK", jenis_kelamin);

            //this.normalisasi();
        }else{
            if (jenis_nota.equals("B")) {
                if(!jenis_kelamin.equals("A")){
                    String substring = start.substring(0,4);
                    if (substring.equals("2010")) {                        
                        rs= stat.executeQuery("select * from bon_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 4 and 64");
                    }else if (substring.equals("2011")) {
                        rs= stat.executeQuery("select * from bon_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 6 and 62");                
                    }else{
                        rs= stat.executeQuery("select * from bon_fact_jenis3 b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 8 and 59");                    
                    }
                }else{
                    String substring = start.substring(0,4);
                    if (substring.equals("2010")) {
                        rs= stat.executeQuery("select * from bon_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 4 and 64");
                    }else if (substring.equals("2011")) {
                        rs= stat.executeQuery("select * from bon_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 6 and 62");
                    }else{
                        rs= stat.executeQuery("select * from bon_fact_jenis3 where tanggal between '"+ start +"' and '" + end +"' and umur between 8 and 59");
                    }

                }        
            }else{
                if(!jenis_kelamin.equals("A")){
                    String substring = start.substring(start.lastIndexOf("-") + 1);
                    if (substring.equals("2010")) {
                        rs= stat.executeQuery("select * from nota_fact_jenis b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 7 and 71");
                    }else if (substring.equals("2011")) {
                        rs= stat.executeQuery("select * from nota_fact_jenis b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 13 and 64");
                    }else{
                        rs= stat.executeQuery("select * from nota_fact_jenis b, pelanggan p where b.kd_customer = p.kd_customer and p.jns_kelamin = '"+ jenis_kelamin +"' and b.tanggal between '"+ start +"' and '" + end +"' and b.umur between 7 and 71");
                    }
                }else{
                    String substring = start.substring(start.lastIndexOf("-") + 1);
                    if (substring.equals("2010")) {
                        rs= stat.executeQuery("select * from nota_fact_jenis where tanggal between '"+ start +"' and '" + end +"' and umur between 7 and 71");
                    }else if (substring.equals("2011")) {
                        rs= stat.executeQuery("select * from nota_fact_jenis where tanggal between '"+ start +"' and '" + end +"' and umur between 13 and 64");
                    }else{
                        rs= stat.executeQuery("select * from nota_fact_jenis where tanggal between '"+ start +"' and '" + end +"' and umur between 7 and 71");
                    }
                }
            }

            //ResultSet rs= ps.executeQuery();
            listjenis = new ArrayList();
            while(rs.next()){  
             BonJenis_Transaction bon = new BonJenis_Transaction();
             bon.setTanggal(rs.getString(1));
             bon.setNo_bon(rs.getString(2));
             bon.setKd_customer(rs.getString(3));
             bon.setUmur(rs.getDouble(4));
             bon.setId_toko(rs.getString(5));
             bon.setKel_jns(rs.getString(6));
             bon.setJumlah(rs.getDouble(7));
             listjenis.add(bon); 
            }  
            SerializedObject se = new SerializedObject();
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            conn.setAutoCommit(false);
//            se.writeFakta(conn, namaperiod, listjenis,Integer.parseInt(periodselect),Integer.parseInt(modelfakta));
            conn.commit();

            Map<String, Object> session = ActionContext.getContext().getSession();
            //session.put("FACT", list);
            //session.put("NAMAPER", namaperiod);        
            session.put("START", start);        
            session.put("END", end);        
            //session.put("JK", jenis_kelamin);

            //this.normalisasi();
        }
        
        stat.close();
        
     }catch(Exception e){
         e.printStackTrace();
     }  
        return SUCCESS;
    }
    
    public String getScatter(){
        try{
        this.getKelamin();        
        Map<String, Object> session = ActionContext.getContext().getSession();
        pivotjenisbaru = new ArrayList<>((ArrayList<PivotJenis_Table>)session.get("PIVOT"));            
        scatterplotjenis = new ArrayList();
        jenis_kelamin =  (String)session.get("JK");
        System.out.println("JK" + jenis_kelamin);
        for (int i = 0; i < pivotjenisbaru.size(); i++) {
            ScatterJenis sct = new ScatterJenis();
            sct.setNo_bon(pivotjenisbaru.get(i).getNo_bon());
            sct.setUmur(pivotjenisbaru.get(i).getUmurnormal());
            sct.setUmurasli(pivotjenisbaru.get(i).getUmurasli());
            sct.setJenis_kelamin(jenis_kelamin);
            sct.setStrip(pivotjenisbaru.get(i).getJenispivot());
            scatterplotjenis.add(sct);
            sct = null;
        }
        scatterplothasil =  new ArrayList<>();
        scatterplothasil = scatterplotjenis;
        
        //this.normalisasi();
  
     }catch(Exception e){
         e.printStackTrace();
     }  
        return SUCCESS;
    }
    
    public String execute(){  
     try{    
        Statement stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();
        //Statement stat = new HiveConnector("192.168.1.100", "hive", "", "abadi").getConn().createStatement();
        /*Connection conn = new HiveConnector("localhost", "hive", "", "default").getConn();
        PreparedStatement ps = conn.prepareStatement("select * from amigo.bon_fact limit ?,20");
        ps.setInt(1, 0);*/
        ResultSet rs= stat.executeQuery("select * from bon_fact");
        //ResultSet rs= ps.executeQuery();
        list = new ArrayList();
        while(rs.next()){  
         Bon_Transaction bon = new Bon_Transaction();
         bon.setTanggal(rs.getString(1));
         bon.setNo_bon(rs.getString(2));
         bon.setKd_customer(rs.getString(3));
         bon.setUmur(rs.getDouble(4));
         bon.setId_toko(rs.getString(5));
         bon.setKd_strip(rs.getString(6));
         bon.setJumlah(rs.getDouble(7));
         list.add(bon); 
        }  
  
     }catch(Exception e){
         e.printStackTrace();
     }  

        return SUCCESS;  
    }  
    
    public String similarity(){
        try{  
            this.displaySelectPivot();
            this.getDataPivot();

            SerializedObject se = new SerializedObject();
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            conn.setAutoCommit(false);
            //modelfakta = se.readMetode(conn, Integer.parseInt(periodselect));
            Object o = se.read(conn, Integer.parseInt(periodselect));
            conn.commit();

            pivotjenisbaru = new ArrayList<>((ArrayList<PivotJenis_Table>)o);
            distancespace = new ArrayList();        
        
            Double[][] sim = new Double[pivotjenisbaru.size()][pivotjenisbaru.size()];       

            if (metodeselect.equals("0")) {
                
                for (int i = 0; i < pivotjenisbaru.size(); i++) {
                    Double sumP = new Double(0);
                    Double sumA = new Double(0);
                    Double sumW = new Double(0);
                    Double sumS = new Double(0);                
                    bondistance = new ArrayList();
                    for (int j = 0; j < pivotjenisbaru.size(); j++) {
                        if (i<j) {
                            Double sumumur = new Double(0);
                            Double sumtotal = new Double(0);
                            Double d = new Double(0);
                            Double e = new Double(0);
                            Double anak = new Double(0);
                            Double wanita = new Double(0);
                            Double pria = new Double(0);
                            Double sepatu = new Double(0);
                            sumumur = Math.abs(pivotjenisbaru.get(i).getUmurnormal() - pivotjenisbaru.get(j).getUmurnormal());
                            for (int k = 0; k < pivotjenisbaru.get(i).getJenispivot().size(); k++) {
                                if (k==0) {
                                    anak = Math.abs(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() - pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah());
                                }else if(k==1){
                                    wanita = Math.abs(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() - pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah());                                
                                }else if(k==2){
                                    pria = Math.abs(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() - pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah());
                                }else if(k==3){
                                    sepatu = Math.abs(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() - pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah());                                
                                }
                            }                    
                            sumumur = Math.pow(sumumur,2);
                            sumA = Math.pow(anak,2);
                            sumW = Math.pow(wanita,2);
                            sumP = Math.pow(pria,2);
                            sumS = Math.pow(sepatu,2);

                            sumtotal =  Math.sqrt(sumumur + sumA + sumP + sumS + sumW);
                            sim[i][j] = sumtotal;
                            sim[j][i] = sumtotal;

                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            sumumur = null;
                            sumtotal = null;
                            d = null;
                            e = null;

                        }else if(i==j){
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(0.0);
                            bondistance.add(bd);
                            bd = null;
                        }else{
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            bd = null;
                        }
                    }
                    Distance_Table dt = new Distance_Table();
                    dt.setNo_bon(pivotjenisbaru.get(i).getNo_bon());
                    dt.setDistancebon(bondistance);
                    distancespace.add(dt);
                    dt = null;
                }

            SerializedObject se5 = new SerializedObject();
            DBConnector connector5 = new DBConnector();
            Connection conn5 = connector5.getConn();
            conn5.setAutoCommit(false);
            se5.writeHirarki(conn5, namaperiod, distancespace, Integer.parseInt(periodselect));
            conn5.commit();

            }else{

                for (int i = 0; i < pivotjenisbaru.size(); i++) {

                    Double dotProductUmur = 0.0;
                    Double dotProductAnak = 0.0;
                    Double dotProductWanita = 0.0;
                    Double dotProductPria = 0.0;
                    Double dotProductSepatu =  0.0;
                    Double magnitude1 = 0.0;
                    Double magnitude2 = 0.0;
                    Double cosineSimilarity = 0.0;

                    bondistance = new ArrayList();
                    for (int j = 0; j < pivotjenisbaru.size(); j++) {
                        if (i<j) {
                            dotProductUmur = pivotjenisbaru.get(i).getUmurnormal() * pivotjenisbaru.get(j).getUmurnormal();  //a.b
                            magnitude1 += Math.pow(pivotjenisbaru.get(i).getUmurnormal(), 2);  //(a^2)
                            magnitude2 += Math.pow(pivotjenisbaru.get(j).getUmurnormal(), 2); //(b^2)
                            
                            for (int k = 0; k < pivotjenisbaru.get(i).getJenispivot().size(); k++) {
                                if (k==0) {
                                    dotProductAnak = pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() * pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah();  //a.b                           
                                }else if (k==1) {
                                    dotProductWanita = pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() * pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah();  //a.b                           

                                }else if (k==2) {
                                    dotProductPria = pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() * pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah();  //a.b                           

                                }else if (k==3) {
                                    dotProductSepatu = pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() * pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah();  //a.b                           
                                }                       
                                magnitude1 += Math.pow(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah(), 2);  //(a^2)
                                magnitude2 += Math.pow(pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah(), 2); //(b^2)
                            }
                            magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
                            magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)
                            /*
                            magnitude1 = Math.ceil(magnitude1 * 10000) / 10000;
                            magnitude2 = Math.ceil(magnitude2 * 10000) / 10000;
                            dotProductAnak = Math.ceil(dotProductAnak * 10000) / 10000;
                            dotProductWanita = Math.ceil(dotProductWanita * 10000) / 10000;
                            dotProductPria = Math.ceil(dotProductPria * 10000) / 10000;
                            dotProductSepatu = Math.ceil(dotProductSepatu * 10000) / 10000;
                            dotProductUmur = Math.ceil(dotProductUmur * 10000) / 10000;*/
                            /*if (j==1) {
                                System.out.println("dotU" + dotProductUmur);
                                System.out.println("dotA" + dotProductAnak);
                                System.out.println("dotW" + dotProductWanita);
                                System.out.println("dotP" + dotProductPria);
                                System.out.println("dotS" + dotProductSepatu);
                                System.out.println("mag1" + magnitude1);
                                System.out.println("mag2" + magnitude2);
                            }*/
                            
                            if (magnitude1 != 0.0 | magnitude2 != 0.0) {
                                cosineSimilarity = (dotProductUmur + dotProductAnak + dotProductPria + dotProductSepatu + dotProductWanita) / (magnitude1 * magnitude2);
                                //cosineSimilarity = Math.ceil(cosineSimilarity * 10000) / 10000;
                                if (Double.isNaN(cosineSimilarity)) {                                    
                                    cosineSimilarity = 0.0;
                                }
                                //|| Double.toString(cosineSimilarity).contains("E")
                                //Math.ceil(cosineSimilarity * 10000) / 10000;
                            } else {
                                cosineSimilarity = 0.0;
                            }
                            sim[i][j] = cosineSimilarity;
                            sim[j][i] = cosineSimilarity;

                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            bd = null;                        
                         }else if(i==j){
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(0.0);
                            bondistance.add(bd);
                            bd = null;
                        }else{
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            bd = null;
                        }
                    }
                    Distance_Table dt = new Distance_Table();
                    dt.setNo_bon(pivotjenisbaru.get(i).getNo_bon());
                    dt.setDistancebon(bondistance);
                    distancespace.add(dt);
                    dt = null;

                }
                SerializedObject se5 = new SerializedObject();
                DBConnector connector5 = new DBConnector();
                Connection conn5 = connector5.getConn();
                conn5.setAutoCommit(false);
                se5.writeHirarki(conn5, namaperiod, distancespace, Integer.parseInt(periodselect));
                conn5.commit();

            }

        
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String similarity1(){
        try{  

        this.displaySelectPivot();
        this.getDataPivot();

        SerializedObject se = new SerializedObject();
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        conn.setAutoCommit(false);
        modelfakta = se.readMetode(conn, Integer.parseInt(periodselect));
        Object o = se.read(conn, Integer.parseInt(periodselect));
        conn.commit();

        //simpen dulu ya
        
        /*pivotjenis = new ArrayList<>((ArrayList<PivotJenis_Table>)o);
        distancespace = new ArrayList();        
        
        Double[][] sim = new Double[pivotjenis.size()][pivotjenis.size()];       
        */   
        if (modelfakta.equals("0")) {
        
            pivotbaru = new ArrayList<>((ArrayList<Pivot_Table>)o);
            distancespace = new ArrayList();        
        
            Double[][] sim = new Double[pivotbaru.size()][pivotbaru.size()];       

            if (metodeselect.equals("0")) {

            for (int i = 0; i < pivotbaru.size(); i++) {
                Double sumstrip = new Double(0);
                //Double sumstripasli = new Double(0);

                bondistance = new ArrayList();
                for (int j = 0; j < pivotbaru.size(); j++) {
                    if (i<j) {
                        Double sumumur = new Double(0);
                        Double sumtotal = new Double(0);
                        Double d = new Double(0);
                        Double e = new Double(0);
                        //Double f = new Double(0);
                        sumumur = Math.abs(pivotbaru.get(i).getUmurnormal() - pivotbaru.get(j).getUmurnormal());
                        //sumumur = Math.abs(Float.parseFloat(pivotbaru.get(i).getUmurnormal()) - Float.parseFloat(pivotbaru.get(j).getUmurnormal()));
                        for (int k = 0; k < pivotbaru.get(i).getStrippivot().size(); k++) {
                            d = pivotbaru.get(i).getStrippivot().get(k).getJumlah() - pivotbaru.get(j).getStrippivot().get(k).getJumlah();
                            //f = pivotbaru.get(i).getStrippivot().get(k).getJumlahasli()- pivotbaru.get(j).getStrippivot().get(k).getJumlahasli();
                            sumstrip = Math.abs(sumstrip + Math.pow(d,2));
                            //sumstripasli = Math.abs(sumstripasli + Math.pow(f,2));

                            //String x = new String(pivotbaru.get(i).getStrippivot().get(k).getJumlah());
                        }                    
                        sumumur = Math.pow(sumumur,2);
                        sumtotal =  Math.sqrt(sumumur + sumstrip);
                        sim[i][j] = sumtotal;
                        sim[j][i] = sumtotal;

                        Bon_Distance bd = new Bon_Distance();      
                        bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                        bd.setJumlahdistance(sim[i][j]);
                        bondistance.add(bd);
                        sumumur = null;
                        sumtotal = null;
                        d = null;
                        e = null;

                    }else if(i==j){
                        Bon_Distance bd = new Bon_Distance();      
                        bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                        bd.setJumlahdistance(0.0);
                        bondistance.add(bd);
                        bd = null;
                    }else{
                        Bon_Distance bd = new Bon_Distance();      
                        bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                        bd.setJumlahdistance(sim[i][j]);
                        bondistance.add(bd);
                        bd = null;
                    }
                }

                Distance_Table dt = new Distance_Table();
                dt.setNo_bon(pivotbaru.get(i).getNo_bon());
                dt.setDistancebon(bondistance);
    //            dt.setUmurasli(pivotbaru.get(i).getUmurasli());
                distancespace.add(dt);
                dt = null;
            }

            SerializedObject se5 = new SerializedObject();
            DBConnector connector5 = new DBConnector();
            Connection conn5 = connector5.getConn();
            conn5.setAutoCommit(false);
//            se5.writeHirarki(conn5, namaperiod, distancespace, Integer.parseInt(periodselect),Integer.parseInt(modelfakta));
            conn5.commit();


            }else{

                for (int i = 0; i < pivotbaru.size(); i++) {

                    /*Double dotProductUmur = 0.0;
                    Double dotProductAnak = 0.0;
                    Double dotProductWanita = 0.0;
                    Double dotProductPria = 0.0;
                    Double dotProductSepatu =  0.0;
                    Double magnitude1 = 0.0;
                    Double magnitude2 = 0.0;
                    Double cosineSimilarity = 0.0;

                    bondistance = new ArrayList();
                    for (int j = 0; j < pivotjenis.size(); j++) {
                        if (i<=j) {

                            dotProductUmur += pivotjenis.get(i).getUmurnormal() * pivotjenis.get(j).getUmurnormal();  //a.b
                            magnitude1 += Math.pow(Math.abs(pivotjenis.get(i).getUmurnormal()), 2);  //(a^2)
                            magnitude2 += Math.pow(Math.abs(pivotjenis.get(j).getUmurnormal()), 2); //(b^2)

                            for (int k = 0; k < pivotjenis.get(i).getJenispivot().size(); k++) {
                                if (k==0) {
                                    dotProductAnak += pivotjenis.get(i).getJenispivot().get(k).getJumlah() * pivotjenis.get(j).getJenispivot().get(k).getJumlah();  //a.b                           
                                }else if (k==1) {
                                    dotProductWanita += pivotjenis.get(i).getJenispivot().get(k).getJumlah() * pivotjenis.get(j).getJenispivot().get(k).getJumlah();  //a.b                           

                                }else if (k==2) {
                                    dotProductPria += pivotjenis.get(i).getJenispivot().get(k).getJumlah() * pivotjenis.get(j).getJenispivot().get(k).getJumlah();  //a.b                           

                                }else if (k==3) {
                                    dotProductSepatu += pivotjenis.get(i).getJenispivot().get(k).getJumlah() * pivotjenis.get(j).getJenispivot().get(k).getJumlah();  //a.b                           
                                }                       
                                magnitude1 += Math.pow(Math.abs(pivotjenis.get(i).getJenispivot().get(k).getJumlah()), 2);  //(a^2)
                                magnitude2 += Math.pow(Math.abs(pivotjenis.get(j).getJenispivot().get(k).getJumlah()), 2); //(b^2)
                            }
                            magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
                            magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)

                            if (magnitude1 != 0.0 | magnitude2 != 0.0) {
                                cosineSimilarity = (dotProductUmur + dotProductAnak + dotProductPria + dotProductSepatu + dotProductWanita) / (magnitude1 * magnitude2);
                            } else {
                                cosineSimilarity = 0.0;
                            }
                            sim[i][j] = cosineSimilarity;
                            sim[j][i] = cosineSimilarity;

                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenis.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            bd = null;                        
                         }else{
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenis.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            bd = null;
                        }
                    }
                    Distance_Table dt = new Distance_Table();
                    dt.setNo_bon(pivotjenis.get(i).getNo_bon());
                    dt.setDistancebon(bondistance);
                    distancespace.add(dt);
                    dt = null;
                    */
                }
                SerializedObject se5 = new SerializedObject();
                DBConnector connector5 = new DBConnector();
                Connection conn5 = connector5.getConn();
                conn5.setAutoCommit(false);
//                se5.writeHirarki(conn5, namaperiod, distancespace, Integer.parseInt(periodselect),Integer.parseInt(modelfakta));
                conn5.commit();
                    
            }

        }else{
            
            pivotjenisbaru = new ArrayList<>((ArrayList<PivotJenis_Table>)o);
            distancespace = new ArrayList();        
        
            Double[][] sim = new Double[pivotjenisbaru.size()][pivotjenisbaru.size()];       

            if (metodeselect.equals("0")) {

                
                for (int i = 0; i < pivotjenisbaru.size(); i++) {
                    Double sumP = new Double(0);
                    Double sumA = new Double(0);
                    Double sumW = new Double(0);
                    Double sumS = new Double(0);                
                    bondistance = new ArrayList();
                    for (int j = 0; j < pivotjenisbaru.size(); j++) {
                        if (i<j) {
                            Double sumumur = new Double(0);
                            Double sumtotal = new Double(0);
                            Double d = new Double(0);
                            Double e = new Double(0);
                            Double anak = new Double(0);
                            Double wanita = new Double(0);
                            Double pria = new Double(0);
                            Double sepatu = new Double(0);
                            sumumur = Math.abs(pivotjenisbaru.get(i).getUmurnormal() - pivotjenisbaru.get(j).getUmurnormal());
                            for (int k = 0; k < pivotjenisbaru.get(i).getJenispivot().size(); k++) {
    //                            d = pivotbaru.get(i).getStrippivot().get(k).getJumlah() - pivotbaru.get(j).getStrippivot().get(k).getJumlah();
                                if (k==0) {
                                    anak = Math.abs(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() - pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah());
    //                               sumA = Math.abs(sumA + Math.pow(anak,2));
                                }else if(k==1){
                                    wanita = Math.abs(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() - pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah());                                
    //                                sumW = Math.abs(sumW + Math.pow(wanita,2));
                                }else if(k==2){
                                    pria = Math.abs(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() - pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah());
    //                                sumP = Math.abs(sumP + Math.pow(pria,2));
                                }else if(k==3){
                                    sepatu = Math.abs(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() - pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah());                                
    //                                sumS = Math.abs(sumS + Math.pow(sepatu,2));
                                }
                            }                    
                            sumumur = Math.pow(sumumur,2);
                            sumA = Math.pow(anak,2);
                            sumW = Math.pow(wanita,2);
                            sumP = Math.pow(pria,2);
                            sumS = Math.pow(sepatu,2);

                            sumtotal =  Math.sqrt(sumumur + sumA + sumP + sumS + sumW);
                            sim[i][j] = sumtotal;
                            sim[j][i] = sumtotal;

                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            sumumur = null;
                            sumtotal = null;
                            d = null;
                            e = null;

                        }else if(i==j){
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(0.0);
                            bondistance.add(bd);
                            bd = null;
                        }else{
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            bd = null;
                        }
                    }

                    Distance_Table dt = new Distance_Table();
                    dt.setNo_bon(pivotjenisbaru.get(i).getNo_bon());
                    dt.setDistancebon(bondistance);
                    distancespace.add(dt);
                    dt = null;
                }

            SerializedObject se5 = new SerializedObject();
            DBConnector connector5 = new DBConnector();
            Connection conn5 = connector5.getConn();
            conn5.setAutoCommit(false);
//            se5.writeHirarki(conn5, namaperiod, distancespace, Integer.parseInt(periodselect),Integer.parseInt(modelfakta));
            conn5.commit();


            }else{

                for (int i = 0; i < pivotjenisbaru.size(); i++) {

                    Double dotProductUmur = 0.0;
                    Double dotProductAnak = 0.0;
                    Double dotProductWanita = 0.0;
                    Double dotProductPria = 0.0;
                    Double dotProductSepatu =  0.0;
                    Double magnitude1 = 0.0;
                    Double magnitude2 = 0.0;
                    Double cosineSimilarity = 0.0;

                    bondistance = new ArrayList();
                    for (int j = 0; j < pivotjenisbaru.size(); j++) {
                        if (i<j) {
                            dotProductUmur = pivotjenisbaru.get(i).getUmurnormal() * pivotjenisbaru.get(j).getUmurnormal();  //a.b
                            magnitude1 += Math.pow(pivotjenisbaru.get(i).getUmurnormal(), 2);  //(a^2)
                            magnitude2 += Math.pow(pivotjenisbaru.get(j).getUmurnormal(), 2); //(b^2)
                            
                            for (int k = 0; k < pivotjenisbaru.get(i).getJenispivot().size(); k++) {
                                if (k==0) {
                                    dotProductAnak = pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() * pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah();  //a.b                           
                                }else if (k==1) {
                                    dotProductWanita = pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() * pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah();  //a.b                           

                                }else if (k==2) {
                                    dotProductPria = pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() * pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah();  //a.b                           

                                }else if (k==3) {
                                    dotProductSepatu = pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah() * pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah();  //a.b                           
                                }                       
                                magnitude1 += Math.pow(pivotjenisbaru.get(i).getJenispivot().get(k).getJumlah(), 2);  //(a^2)
                                magnitude2 += Math.pow(pivotjenisbaru.get(j).getJenispivot().get(k).getJumlah(), 2); //(b^2)
                            }
                            magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
                            magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)
                            /*
                            magnitude1 = Math.ceil(magnitude1 * 10000) / 10000;
                            magnitude2 = Math.ceil(magnitude2 * 10000) / 10000;
                            dotProductAnak = Math.ceil(dotProductAnak * 10000) / 10000;
                            dotProductWanita = Math.ceil(dotProductWanita * 10000) / 10000;
                            dotProductPria = Math.ceil(dotProductPria * 10000) / 10000;
                            dotProductSepatu = Math.ceil(dotProductSepatu * 10000) / 10000;
                            dotProductUmur = Math.ceil(dotProductUmur * 10000) / 10000;*/
                            
                            if (magnitude1 != 0.0 | magnitude2 != 0.0) {
                                cosineSimilarity = (dotProductUmur + dotProductAnak + dotProductPria + dotProductSepatu + dotProductWanita) / magnitude1 * magnitude2;
                                //cosineSimilarity = Math.ceil(cosineSimilarity * 10000) / 10000;
                                if (Double.isNaN(cosineSimilarity)) {                                    
                                    cosineSimilarity = 0.0;
                                }
                                //|| Double.toString(cosineSimilarity).contains("E")
                                //Math.ceil(cosineSimilarity * 10000) / 10000;
                            } else {
                                cosineSimilarity = 0.0;
                            }
                            sim[i][j] = cosineSimilarity;
                            sim[j][i] = cosineSimilarity;

                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            bd = null;                        
                         }else if(i==j){
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(0.0);
                            bondistance.add(bd);
                            bd = null;
                        }else{
                            Bon_Distance bd = new Bon_Distance();      
                            bd.setNo_bon(pivotjenisbaru.get(j).getNo_bon());
                            bd.setJumlahdistance(sim[i][j]);
                            bondistance.add(bd);
                            bd = null;
                        }
                    }
                    Distance_Table dt = new Distance_Table();
                    dt.setNo_bon(pivotjenisbaru.get(i).getNo_bon());
                    dt.setDistancebon(bondistance);
                    distancespace.add(dt);
                    dt = null;

                }
                SerializedObject se5 = new SerializedObject();
                DBConnector connector5 = new DBConnector();
                Connection conn5 = connector5.getConn();
                conn5.setAutoCommit(false);
//                se5.writeHirarki(conn5, namaperiod, distancespace, Integer.parseInt(periodselect),Integer.parseInt(modelfakta));
                conn5.commit();

            }

        }
        
        //Map<String, Object> session = ActionContext.getContext().getSession();
        //session.put("EUCID", distancespace);
        //session.put("SELPERIOD", periodselect);
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String eucid(){
        try{  
        this.displaySelectPivot();
        this.getDataPivot();

        SerializedObject se = new SerializedObject();
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        conn.setAutoCommit(false);
        modelfakta = se.readMetode(conn, Integer.parseInt(periodselect));
        Object o = se.read(conn, Integer.parseInt(periodselect));
        conn.commit();
        pivotbaru = new ArrayList<>((ArrayList<Pivot_Table>)o);
        
        distancespace = new ArrayList();        
        
        Double[][] sim = new Double[pivotbaru.size()][pivotbaru.size()];       
        
        for (int i = 0; i < pivotbaru.size(); i++) {
            Double sumstrip = new Double(0);
            //Double sumstripasli = new Double(0);

            bondistance = new ArrayList();
            for (int j = 0; j < pivotbaru.size(); j++) {
                if (i<j) {
                    Double sumumur = new Double(0);
                    Double sumtotal = new Double(0);
                    Double d = new Double(0);
                    Double e = new Double(0);
                    //Double f = new Double(0);
                    sumumur = Math.abs(pivotbaru.get(i).getUmurnormal() - pivotbaru.get(j).getUmurnormal());
                    //sumumur = Math.abs(Float.parseFloat(pivotbaru.get(i).getUmurnormal()) - Float.parseFloat(pivotbaru.get(j).getUmurnormal()));
                    for (int k = 0; k < pivotbaru.get(i).getStrippivot().size(); k++) {
                        d = pivotbaru.get(i).getStrippivot().get(k).getJumlah() - pivotbaru.get(j).getStrippivot().get(k).getJumlah();
                        //f = pivotbaru.get(i).getStrippivot().get(k).getJumlahasli()- pivotbaru.get(j).getStrippivot().get(k).getJumlahasli();
                        sumstrip = Math.abs(sumstrip + Math.pow(d,2));
                        //sumstripasli = Math.abs(sumstripasli + Math.pow(f,2));

                        //String x = new String(pivotbaru.get(i).getStrippivot().get(k).getJumlah());
                    }                    
                    sumumur = Math.pow(sumumur,2);
                    sumtotal =  Math.sqrt(sumumur + sumstrip);
                    sim[i][j] = sumtotal;
                    sim[j][i] = sumtotal;
                    
                    Bon_Distance bd = new Bon_Distance();      
                    bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                    bd.setJumlahdistance(sim[i][j]);
                    bondistance.add(bd);
                    sumumur = null;
                    sumtotal = null;
                    d = null;
                    e = null;
                    
                }else if(i==j){
                    Bon_Distance bd = new Bon_Distance();      
                    bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                    bd.setJumlahdistance(0.0);
                    bondistance.add(bd);
                    bd = null;
                }else{
                    Bon_Distance bd = new Bon_Distance();      
                    bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                    bd.setJumlahdistance(sim[i][j]);
                    bondistance.add(bd);
                    bd = null;
                }
            }
                    
            Distance_Table dt = new Distance_Table();
            dt.setNo_bon(pivotbaru.get(i).getNo_bon());
            dt.setDistancebon(bondistance);
//            dt.setUmurasli(pivotbaru.get(i).getUmurasli());
            distancespace.add(dt);
            dt = null;
        }
        
        SerializedObject se5 = new SerializedObject();
        DBConnector connector5 = new DBConnector();
        Connection conn5 = connector5.getConn();
        conn5.setAutoCommit(false);
//        se5.writeHirarki(conn5, namaperiod, distancespace, Integer.parseInt(periodselect),Integer.parseInt(modelfakta));
        conn5.commit();
        
        //Map<String, Object> session = ActionContext.getContext().getSession();
        //session.put("EUCID", distancespace);
        //session.put("SELPERIOD", periodselect);
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    
    public String euclideanDistance(){
        try{  
        list = null;
        listnormal = null;
        start = null;
        end = null;
        jenis_kelamin = null;
        pivotbaru = null;
        distancespace = null;
        this.displaySelect();
        this.getDate();

        Map<String, Object> session = ActionContext.getContext().getSession();
        list = new ArrayList<>((ArrayList<Bon_Transaction>)session.get("FACT"));
        listnormal = new ArrayList<>((ArrayList<Bon_Transaction>)session.get("NORMAL"));
        pivotbaru = new ArrayList<>((ArrayList<Pivot_Table>)session.get("PIVOT"));
        //pivotbaruutil = new ObjectArrayList<>((ObjectArrayList<Pivot_Table>)session.get("PIVOT"));
        
        distancespace = new ArrayList();        
        /*HashMap<String, Pivot_Table> pivotMap = new HashMap<String, Pivot_Table>();
        for (Pivot_Table pivot : pivotbaru) {
           pivotMap.put(pivot.getNo_bon(), pivot);
        }
        
        System.out.println("Ada : " + pivotMap.size());
        for (Map.Entry<String, Pivot_Table> entrySet : pivotMap.entrySet()) {
            String key = entrySet.getKey();
            Pivot_Table value = entrySet.getValue();  
            sumstrip = 0;
            bondistance = new ArrayList();
            for (Map.Entry<String, Pivot_Table> entrySetDua : pivotMap.entrySet()) {
                String keydua = entrySetDua.getKey();
                Pivot_Table valuedua = entrySetDua.getValue();    
                sumumur = Math.abs(Float.parseFloat(value.getUmurnormal()) - Float.parseFloat(valuedua.getUmurnormal()));
                for (int k = 0; k < value.getStrippivot().size(); k++) {
                    d = Double.parseDouble(Float.toString(Float.parseFloat(value.getStrippivot().get(k).getJumlah()) - Float.parseFloat(valuedua.getStrippivot().get(k).getJumlah())));
                    sumstrip = (float) Math.abs(sumstrip + Math.pow(d,2));
                }
                e = Double.parseDouble(Float.toString(sumumur));
                sumumur = (float)Math.pow(e,2);
                sumtotal =  (float) Math.sqrt(sumumur + sumstrip);                
                Bon_Distance bd = new Bon_Distance();      
                bd.setNo_bon(keydua);
                bd.setJumlahdistance(Float.toString(sumtotal));
                bondistance.add(bd);                
                sumumur = 0;
                sumstrip = 0;
                sumtotal = 0;
            }
            Distance_Table dt = new Distance_Table();
            dt.setNo_bon(key);
            dt.setDistancebon(bondistance);
            distancespace.add(dt);
        }*/
        /*String namabon[] = new String[pivotbaru.size()];;
        for (int i = 0; i < pivotbaru.size(); i++) {
            namabon[i] = pivotbaru.get(i).getNo_bon();
        }*/
        Double[][] sim = new Double[pivotbaru.size()][pivotbaru.size()];       
        /*for (int i = 0; i < pivotbaru.size(); i++) {
            Double d = new Double(0);
            Double e = new Double(0);
            Float sumumur = new Float(0);
            Float sumstrip = new Float(0);
            Float sumtotal = new Float(0);
            for (int j = i; j < pivotbaru.size(); j++) {
                    sumumur = Math.abs(Float.parseFloat(pivotbaru.get(i).getUmurnormal()) - Float.parseFloat(pivotbaru.get(j).getUmurnormal()));
                    for (int k = 0; k < pivotbaru.get(i).getStrippivot().size(); k++) {
                        String x = new String(pivotbaru.get(i).getStrippivot().get(k).getJumlah());

                        //Float baru1 = new Float(Float.parseFloat(pivotbaru.get(i).getStrippivot().get(k).getJumlah())); 
                        //Float baru2 = new Float(Float.parseFloat(pivotbaru.get(j).getStrippivot().get(k).getJumlah()));
                        //d = Float.parseFloat(pivotbaru.get(i).getStrippivot().get(k).getJumlah()).DoubleValue();
                        //d = Double.parseDouble(Float.toString(Float.parseFloat(pivotbaru.get(i).getStrippivot().get(k).getJumlah()) - Float.parseFloat(pivotbaru.get(j).getStrippivot().get(k).getJumlah())));
                        //sumstrip = (float) Math.abs(sumstrip + Math.pow(d,2));
                    }
                    //e = Double.parseDouble(Float.toString(sumumur));
                    //sumumur = (float)Math.pow(e,2);
                    //sumtotal =  (float) Math.sqrt(sumumur + sumstrip);
                    //sim[i][j] = sumtotal;
                    //sim[j][i] = sumtotal;
                    //sumumur = 0;
                    //sumstrip = 0;
                    //sumtotal = 0;
            }
            System.out.println("Sudah sampai iterasi ke - " + i);
        }
        
        for (int i = 0; i < pivotbaru.size(); i++) {
            float sumstrip = new Float(0);
            //sumstrip = 0;
            bondistance = new ArrayList();
            for (int j = 0; j < pivotbaru.size(); j++) {
                
                    Bon_Distance bd = new Bon_Distance();      
                    bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                    bd.setJumlahdistance(Float.toString(sim[i][j]));
                    bondistance.add(bd);
            }
            Distance_Table dt = new Distance_Table();
            dt.setNo_bon(pivotbaru.get(i).getNo_bon());
            dt.setDistancebon(bondistance);
            distancespace.add(dt);
            System.out.println("Sudah sampai penamaan iterasi ke - " + i);
        }*/
        
        for (int i = 0; i < pivotbaru.size(); i++) {
            Double sumstrip = new Double(0);
            bondistance = new ArrayList();
            for (int j = 0; j < pivotbaru.size(); j++) {
                if (i<j) {
                    Double sumumur = new Double(0);
                    Double sumtotal = new Double(0);
                    Double d = new Double(0);
                    Double e = new Double(0);
                    sumumur = Math.abs(pivotbaru.get(i).getUmurnormal() - pivotbaru.get(j).getUmurnormal());
                    //sumumur = Math.abs(Float.parseFloat(pivotbaru.get(i).getUmurnormal()) - Float.parseFloat(pivotbaru.get(j).getUmurnormal()));
                    for (int k = 0; k < pivotbaru.get(i).getStrippivot().size(); k++) {
                        d = pivotbaru.get(i).getStrippivot().get(k).getJumlah() - pivotbaru.get(j).getStrippivot().get(k).getJumlah();
                        sumstrip = Math.abs(sumstrip + Math.pow(d,2));
                        //String x = new String(pivotbaru.get(i).getStrippivot().get(k).getJumlah());
                    }                    
                    sumumur = Math.pow(sumumur,2);
                    sumtotal =  Math.sqrt(sumumur + sumstrip);
                    sim[i][j] = sumtotal;
                    sim[j][i] = sumtotal;
                    
                    Bon_Distance bd = new Bon_Distance();      
                    bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                    bd.setJumlahdistance(sim[i][j]);
                    bondistance.add(bd);
                    sumumur = null;
                    sumtotal = null;
                    d = null;
                    e = null;
                    
                }else if(i==j){
                    Bon_Distance bd = new Bon_Distance();      
                    bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                    bd.setJumlahdistance(0.0);
                    bondistance.add(bd);
                    bd = null;
                }else{
                    Bon_Distance bd = new Bon_Distance();      
                    bd.setNo_bon(pivotbaru.get(j).getNo_bon());
                    bd.setJumlahdistance(sim[i][j]);
                    bondistance.add(bd);
                    bd = null;
                }
//                    Bon_Distance be = new Bon_Distance();      
//                    be.setNo_bon(pivotbaru.get(i).getNo_bon());
//                    be.setJumlahdistance(Float.toString(sumtotal));
//                    bondistance.add(bd);
            }
                    
            Distance_Table dt = new Distance_Table();
            dt.setNo_bon(pivotbaru.get(i).getNo_bon());
            dt.setDistancebon(bondistance);
            distancespace.add(dt);
//            System.out.println("Sudah sampai iterasi ke - " + i);
            dt = null;
        }
        
        System.out.println("ITS DONE");
        session.put("DISTANCE", distancespace);
        session.put("DISTANCESIM", sim);
        sim = null;
        
        /*
        EuclideanDistance e = new EuclideanDistance();
        e.setDontNormalize(true);
        e.setInstances(pivotbaru.get(0));
        Instance first = test.get(0);
        Instance second = test.get(1);
        Double x = e.distance(pivotbaru.get(0).getStrippivot().get(0).jumlah, pivotbaru.get(0).getStrippivot().get(0).jumlah);
        System.out.println("jarak : " + x);*/
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String getDendrogramTable(){
        try{  
        dendrogram = null;
        //this.displaySelect();
        //this.getDate();
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        Cluster cl = (Cluster)session.get("DENDROGRAM");
        dendrogram = cl;        
        //System.out.println("Test session"+ cl.getClusterMinDist());
        //System.out.println("Test session"+ dendrogram.getItems().get(0).getTotalAnak());
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        ObjectOutputStream out = new ObjectOutputStream(baos);
//        out.writeObject(dendrogram);
//        out.close();
//        System.out.println("SIZE :" + baos.toByteArray().length);
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public String singleLinkage(){
        try{  
            this.displaySelectSingle();
            this.getDataHirarki();
        
            Map<String, Object> session = ActionContext.getContext().getSession();
        
            SerializedObject se = new SerializedObject();
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            conn.setAutoCommit(false);
        
            //modelfakta = se.readMetodeHirarki(conn, Integer.parseInt(periodselect));
            Object o = se.read(conn, Integer.parseInt(periodselect));
            Object d = se.readHirarki(conn, Integer.parseInt(periodselect));
            conn.commit();
            pivotjenis = new ArrayList<>((ArrayList<PivotJenis_Table>)o);
            distancespace = new ArrayList<>((ArrayList<Distance_Table>)d);

            String[] names = new String[pivotjenis.size()];
            double[] umur = new double[pivotjenis.size()];
            double[] total = new double[pivotjenis.size()];
            double[] totalAnak = new double[pivotjenis.size()];
            double[] totalWanita = new double[pivotjenis.size()];
            double[] totalPria = new double[pivotjenis.size()];
            double[] totalSepatu = new double[pivotjenis.size()];

            for (int i = 0; i < pivotjenis.size(); i++) {
                    names[i] = pivotjenis.get(i).getNo_bon();
                    umur[i] = pivotjenis.get(i).getUmurnormal();
                    for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                        if (j == 0) {
                            if (pivotjenis.get(i).getJenispivot().get(j).getJumlah().equals(0.0)) {
                               totalAnak[i] = Double.MIN_EXPONENT;                                
                            }else{
                               totalAnak[i] = pivotjenis.get(i).getJenispivot().get(j).getJumlah();
                            }
                        }else if(j == 1){
                            
                            if (pivotjenis.get(i).getJenispivot().get(j).getJumlah().equals(0.0)) {
                               totalWanita[i] = Double.MIN_EXPONENT;                                
                            }else{
                               totalWanita[i] = pivotjenis.get(i).getJenispivot().get(j).getJumlah();                            
                            }
                        }else if(j == 2){
                            
                            if (pivotjenis.get(i).getJenispivot().get(j).getJumlah().equals(0.0)) {
                               totalPria[i] = Double.MIN_EXPONENT;                                
                            }else{
                               totalPria[i] = pivotjenis.get(i).getJenispivot().get(j).getJumlah();                            
                            }
                        }else if(j == 3){
                            
                            if (pivotjenis.get(i).getJenispivot().get(j).getJumlah().equals(0.0)) {
                               totalSepatu[i] = Double.MIN_EXPONENT;                                
                            }else{
                               totalSepatu[i] = pivotjenis.get(i).getJenispivot().get(j).getJumlah();                            
                            }
                        }
                
                    }
            }

            double[][] distances = new double[pivotjenis.size()][pivotjenis.size()];
            for (int i = 0; i < pivotjenis.size(); i++) {
                for (int j = 0; j < pivotjenis.size(); j++) {
                    distances[i][j] = Double.parseDouble(distancespace.get(i).getDistancebon().get(j).getJumlahdistance().toString());
                }
            }

            AggloromerativeClustering alg = new AggloromerativeClustering(SINGLE_LINKAGE);
            Cluster x = alg.induceClusters(names, distances, umur, totalAnak, totalWanita, totalPria, totalSepatu);
            dendrogram = x;        
            session.put("DENDROGRAM", x);
       
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
     public String completeLinkage(){
        try{  
            this.displaySelectSingle();
            this.getDataHirarki();
        
            Map<String, Object> session = ActionContext.getContext().getSession();
        
            SerializedObject se = new SerializedObject();
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            conn.setAutoCommit(false);
            //modelfakta = se.readMetodeHirarki(conn, Integer.parseInt(periodselect));
            Object o = se.read(conn, Integer.parseInt(periodselect));
            Object d = se.readHirarki(conn, Integer.parseInt(periodselect));
            conn.commit();
        
            pivotjenis = new ArrayList<>((ArrayList<PivotJenis_Table>)o);
            distancespace = new ArrayList<>((ArrayList<Distance_Table>)d);

            double[] umur = new double[pivotjenis.size()];
            double[] total = new double[pivotjenis.size()];
            double[] totalAnak = new double[pivotjenis.size()];
            double[] totalWanita = new double[pivotjenis.size()];
            double[] totalPria = new double[pivotjenis.size()];
            double[] totalSepatu = new double[pivotjenis.size()];


            String[] names = new String[pivotjenis.size()];
            for (int i = 0; i < pivotjenis.size(); i++) {
                    names[i] = pivotjenis.get(i).getNo_bon();
                    umur[i] = pivotjenis.get(i).getUmurnormal();
                    for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                        if (j == 0) {
                            
                            if (pivotjenis.get(i).getJenispivot().get(j).getJumlah().equals(0.0)) {
                               totalAnak[i] = Double.MIN_EXPONENT;                                
                            }else{
                               totalAnak[i] = pivotjenis.get(i).getJenispivot().get(j).getJumlah();                            
                            }
                            
                        }else if(j == 1){
                            
                            if (pivotjenis.get(i).getJenispivot().get(j).getJumlah().equals(0.0)) {
                               totalWanita[i] = Double.MIN_EXPONENT;                                
                            }else{
                               totalWanita[i] = pivotjenis.get(i).getJenispivot().get(j).getJumlah();                            
                            }
                        }else if(j == 2){
                            
                            if (pivotjenis.get(i).getJenispivot().get(j).getJumlah().equals(0.0)) {
                               totalPria[i] = Double.MIN_EXPONENT;                                
                            }else{
                               totalPria[i] = pivotjenis.get(i).getJenispivot().get(j).getJumlah();                            
                            }
                        }else if(j == 3){
                            
                            if (pivotjenis.get(i).getJenispivot().get(j).getJumlah().equals(0.0)) {
                               totalSepatu[i] = Double.MIN_EXPONENT;                                
                            }else{
                               totalSepatu[i] = pivotjenis.get(i).getJenispivot().get(j).getJumlah();                            
                            }
                        }
                
                    }
                    
            }

            double[][] distances = new double[pivotjenis.size()][pivotjenis.size()];
            for (int i = 0; i < pivotjenis.size(); i++) {
                for (int j = 0; j < pivotjenis.size(); j++) {
                    distances[i][j] = Double.parseDouble(distancespace.get(i).getDistancebon().get(j).getJumlahdistance().toString());
                }
            }

            AggloromerativeClustering alg = new AggloromerativeClustering(COMPLETE_LINKAGE);
            Cluster x = alg.induceClusters(names, distances, umur, totalAnak, totalWanita, totalPria, totalSepatu);
            dendrogram =  x;
            session.put("DENDROGRAM", x);
        
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
        public String pivot(){
        try{  
            this.displaySelectNormalisasi();
            this.getDataNormal();
        
            Map<String, Object> session = ActionContext.getContext().getSession();
            namaperiod = (String) session.get("NAMAPER");
            SerializedObject se3 = new SerializedObject();
            DBConnector connector3 = new DBConnector();
            Connection conn3 = connector3.getConn();
            conn3.setAutoCommit(false);
            //modelfakta = se3.readMetodeNormal(conn3, Integer.parseInt(periodselect));
            Object o3 = se3.readNormal(conn3, Integer.parseInt(periodselect));
            conn3.commit();
            listjenisnormal = new ArrayList<>((ArrayList<BonJenis_Transaction>)o3);
        
            //stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();        

            jenis = new ArrayList();            
            jeniscopy = new ArrayList();
            for (int i = 0; i < 4; i++) {
                Jenis stripkita = new Jenis();
                if (i==0) {                    
                    stripkita.setKel_jns("A");
                }else if(i==1){                    
                    stripkita.setKel_jns("W");
                }else if(i==2){                    
                    stripkita.setKel_jns("P");
                }else if(i==3){
                    stripkita.setKel_jns("S");
                }
                jenis.add(stripkita);
                stripkita = null;
            }
            
            pivotjenis = new ArrayList();
            
            for (int i = 0; i < listjenisnormal.size(); i++) {
                PivotJenis_Table pivottabel = new PivotJenis_Table();
                jeniscopy = new ArrayList();
                for (int j = 0; j < jenis.size(); j++) {
                    Jenis stripini = new Jenis();
                    if (listjenisnormal.get(i).getKel_jns().equals(jenis.get(j).getKel_jns())) {
                        stripini.setKel_jns(jenis.get(j).getKel_jns());
                        stripini.setJumlah(listjenisnormal.get(i).getJumlahnormal());
                        stripini.setJumlahasli(listjenisnormal.get(i).getJumlah());
                    }else{
                        stripini.setKel_jns(jenis.get(j).getKel_jns());
                        stripini.setJumlah(0.0);
                        stripini.setJumlahasli(0.0);
                    }
                    jeniscopy.add(stripini);
                }
                
                pivottabel.setNo_bon(listjenisnormal.get(i).getNo_bon());
                pivottabel.setUmurnormal(listjenisnormal.get(i).getUmurnormal());
                pivottabel.setUmurasli(listjenisnormal.get(i).getUmur());
                pivottabel.setJenispivot(jeniscopy);
                pivotjenis.add(pivottabel);
            }

            String namabonlama = pivotjenis.get(0).getNo_bon();
            Double tampungan = 0.0;

            pivotjenisbaru = new ArrayList();
            int count = 0;


            for (int i = 0; i < pivotjenis.size(); i++) {
                //kalau i di posisi row terakir
                if (i == pivotjenis.size()-1) {
                    if (pivotjenis.get(i).getNo_bon().equals(pivotjenis.get(i-1).getNo_bon())){
                        for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                            pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah()+ pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                            pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());
                        }
                    }
                    
                    PivotJenis_Table piv = new PivotJenis_Table();
                    piv.setNo_bon(namabonlama);
                    piv.setUmurnormal(pivotjenis.get(i).getUmurnormal());
                    piv.setJenispivot(pivotjenis.get(i).getJenispivot());
                    piv.setUmurasli(pivotjenis.get(i).getUmurasli());
                    pivotjenisbaru.add(piv); 
                    count = 0;
                    break;
                }
                //kalau setelahnya nama ne gak sama
                if (!pivotjenis.get(i+1).getNo_bon().equals(namabonlama)) {
                        if (i > 0) {
                            //kalau penanda sudah 0 berarti tidak perlu set pivot yg lama supaya tidak terjadi penambahan jumlah sebelumnya
                            if (count != 0) {
                                for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                                    tampungan = pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah();
                                    pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                                    pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());

                                }       
                            }
                        }
                        PivotJenis_Table piv = new PivotJenis_Table();
                        piv.setNo_bon(namabonlama);
                        piv.setUmurnormal(pivotjenis.get(i).getUmurnormal());
                        piv.setJenispivot(pivotjenis.get(i).getJenispivot());
                        piv.setUmurasli(pivotjenis.get(i).getUmurasli());
                        pivotjenisbaru.add(piv); 
                        namabonlama = pivotjenis.get(i+1).getNo_bon();
                        count = 0;
                }else{
    //                System.out.println("Nama sama" + pivot.get(i).getNo_bon());                
                    if (count==0) {
    //                    System.out.println("Nama sama Iterasi Pertama");
                        count++;
                        continue;
                    }else{                    
    //                    System.out.println("Nama sama Iterasi berikut");
                        for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                            tampungan = pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah();
                            pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                            pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());

                        }
                        count++;
                    }
                    continue;
                }

            }

            session.put("PIVOT", pivotjenisbaru);
            session.put("SELPERIOD", periodselect);            
            session.put("MODEL", modelfakta);

            SerializedObject se1 = new SerializedObject();
            DBConnector connector1 = new DBConnector();
            Connection conn1 = connector1.getConn();
            conn1.setAutoCommit(false);
            se1.write(conn1, namaperiod, pivotjenisbaru, Integer.parseInt(periodselect));        
            conn1.commit();
        
        
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    } 
     
    public String pivoting(){
        try{  
            this.displaySelectNormalisasi();
            this.getDataNormal();
        
            Map<String, Object> session = ActionContext.getContext().getSession();
            namaperiod = (String) session.get("NAMAPER");
            SerializedObject se3 = new SerializedObject();
            DBConnector connector3 = new DBConnector();
            Connection conn3 = connector3.getConn();
            conn3.setAutoCommit(false);
            //modelfakta = se3.readMetodeNormal(conn3, Integer.parseInt(periodselect));
            Object o3 = se3.readNormal(conn3, Integer.parseInt(periodselect));
            conn3.commit();
            listjenisnormal = new ArrayList<>((ArrayList<BonJenis_Transaction>)o3);
        
            //stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();        

            jenis = new ArrayList();            
            jeniscopy = new ArrayList();
            for (int i = 0; i < 4; i++) {
                Jenis stripkita = new Jenis();
                if (i==0) {                    
                    stripkita.setKel_jns("A");
                }else if(i==1){                    
                    stripkita.setKel_jns("W");
                }else if(i==2){                    
                    stripkita.setKel_jns("P");
                }else if(i==3){
                    stripkita.setKel_jns("S");
                }
                jenis.add(stripkita);
                stripkita = null;
            }
            
            pivotjenis = new ArrayList();
            
            for (int i = 0; i < listjenisnormal.size(); i++) {
                PivotJenis_Table pivottabel = new PivotJenis_Table();
                jeniscopy = new ArrayList();
                for (int j = 0; j < jenis.size(); j++) {
                    Jenis stripini = new Jenis();
                    if (listjenisnormal.get(i).getKel_jns().equals(jenis.get(j).getKel_jns())) {
                        stripini.setKel_jns(jenis.get(j).getKel_jns());
                        stripini.setJumlah(listjenisnormal.get(i).getJumlahnormal());
                        stripini.setJumlahasli(listjenisnormal.get(i).getJumlah());
                    }else{
                        stripini.setKel_jns(jenis.get(j).getKel_jns());
                        stripini.setJumlah(0.0);
                        stripini.setJumlahasli(0.0);
                    }
                    jeniscopy.add(stripini);
                }
                
                pivottabel.setNo_bon(listjenisnormal.get(i).getNo_bon());
                pivottabel.setUmurnormal(listjenisnormal.get(i).getUmurnormal());
                pivottabel.setUmurasli(listjenisnormal.get(i).getUmur());
                pivottabel.setJenispivot(jeniscopy);
                pivotjenis.add(pivottabel);
            }

            String namabonlama = pivotjenis.get(0).getNo_bon();
            Double tampungan = 0.0;

            pivotjenisbaru = new ArrayList();
            int count = 0;


            for (int i = 0; i < pivotjenis.size(); i++) {
                //kalau i di posisi row terakir
                if (i == pivotjenis.size()-1) {
//                    if (pivotjenis.get(i).getNo_bon().equals(pivotjenis.get(i-1).getNo_bon())){
//                        for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
//                            pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah()+ pivotjenis.get(i).getJenispivot().get(j).getJumlah());
//                            pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());
//                        }
//                    }
                    for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                            pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah()+ pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                            pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());
                    }
                    PivotJenis_Table piv = new PivotJenis_Table();
                    piv.setNo_bon(namabonlama);
                    piv.setUmurnormal(pivotjenis.get(i).getUmurnormal());
                    piv.setJenispivot(pivotjenis.get(i).getJenispivot());
                    piv.setUmurasli(pivotjenis.get(i).getUmurasli());
                    pivotjenisbaru.add(piv); 
                    count = 0;
                    break;
                }
                //kalau setelahnya nama ne gak sama
                if (!pivotjenis.get(i+1).getNo_bon().equals(namabonlama)) {
                        if (i > 0) {
                            //kalau penanda sudah 0 berarti tidak perlu set pivot yg lama supaya tidak terjadi penambahan jumlah sebelumnya
                            if (count != 0) {
                                for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                                    tampungan = pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah();
                                    pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                                    pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());

                                }       
                            }
                        }
                        PivotJenis_Table piv = new PivotJenis_Table();
                        piv.setNo_bon(namabonlama);
                        piv.setUmurnormal(pivotjenis.get(i).getUmurnormal());
                        piv.setJenispivot(pivotjenis.get(i).getJenispivot());
                        piv.setUmurasli(pivotjenis.get(i).getUmurasli());
                        pivotjenisbaru.add(piv); 
                        namabonlama = pivotjenis.get(i+1).getNo_bon();
                        count = 0;
                }else{
    //                System.out.println("Nama sama" + pivot.get(i).getNo_bon());                
                    if (count==0) {
    //                    System.out.println("Nama sama Iterasi Pertama");
                        count++;
                        continue;
                    }else{                    
    //                    System.out.println("Nama sama Iterasi berikut");
                        for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                            tampungan = pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah();
                            pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                            pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());

                        }
                        count++;
                    }
                    continue;
                }

            }

            session.put("PIVOT", pivotjenisbaru);
            session.put("SELPERIOD", periodselect);            
            session.put("MODEL", modelfakta);

            SerializedObject se1 = new SerializedObject();
            DBConnector connector1 = new DBConnector();
            Connection conn1 = connector1.getConn();
            conn1.setAutoCommit(false);
            se1.write(conn1, namaperiod, pivotjenisbaru, Integer.parseInt(periodselect));        
            conn1.commit();
        
        
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }

    
    public String pivoting1(){
        try{  
        
        this.displaySelectNormalisasi();
        this.getDataNormal();
//        this.getDate();
        
        Map<String, Object> session = ActionContext.getContext().getSession();
        namaperiod = (String) session.get("NAMAPER");
/*        SerializedObject se = new SerializedObject();
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        conn.setAutoCommit(false);
        modelfakta = se.readMetodeNormal(conn, Integer.parseInt(periodselect));
        Object o = se.readNormal(conn, Integer.parseInt(periodselect));
        conn.commit();
        pivotbaru = new ArrayList<>((ArrayList<Pivot_Table>)o);        
*/       
        SerializedObject se3 = new SerializedObject();
        DBConnector connector3 = new DBConnector();
        Connection conn3 = connector3.getConn();
        conn3.setAutoCommit(false);
        modelfakta = se3.readMetodeNormal(conn3, Integer.parseInt(periodselect));
        Object o3 = se3.readNormal(conn3, Integer.parseInt(periodselect));
        conn3.commit();
        
        if (modelfakta.equals("0")) {
            listnormal = new ArrayList<>((ArrayList<Bon_Transaction>)o3);
        
            stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();        
            rs= stat.executeQuery("select kd_strip from strip");
            strip = new ArrayList();
            stripcopy = new ArrayList();
            while(rs.next()){  
                Strip stripkita = new Strip();
                stripkita.setNamastrip(rs.getString(1));
                strip.add(stripkita);
                stripkita = null;
            }        
            pivot = new ArrayList();
            for (int i = 0; i < listnormal.size(); i++) {
                Pivot_Table pivottabel = new Pivot_Table();
                stripcopy = new ArrayList();
                for (int j = 0; j < strip.size(); j++) {
                    Strip stripini = new Strip();
                    if (listnormal.get(i).getKd_strip().equals(strip.get(j).getNamastrip())) {
                        stripini.setNamastrip(strip.get(j).getNamastrip());
                        stripini.setJumlah(listnormal.get(i).getJumlahnormal());
                        stripini.setJumlahasli(listnormal.get(i).getJumlah());
                        stripini.setKel_jns(listnormal.get(i).getKel_jns());
                    }else{
                        stripini.setNamastrip(strip.get(j).getNamastrip());
                        stripini.setJumlah(0.0);
                        stripini.setJumlahasli(0.0);
                        stripini.setKel_jns(listnormal.get(i).getKel_jns());
                    }
                    stripcopy.add(stripini);
                }

                pivottabel.setNo_bon(listnormal.get(i).getNo_bon());
                pivottabel.setUmurnormal(listnormal.get(i).getUmurnormal());
                pivottabel.setUmurasli(listnormal.get(i).getUmur());
                pivottabel.setStrippivot(stripcopy);
                pivot.add(pivottabel);
            }


            String namabonlama = pivot.get(0).getNo_bon();
            Double tampungan = 0.0;

            pivotbaru = new ArrayList();
            int count = 0;

            for (int i = 0; i < pivot.size(); i++) {
                //kalau i di posisi row terakir
                if (i == pivot.size()-1) {
                    for (int j = 0; j < pivot.get(i).getStrippivot().size(); j++) {
                            pivot.get(i).getStrippivot().get(j).setJumlah(pivot.get(i-1).getStrippivot().get(j).getJumlah()+ pivot.get(i).getStrippivot().get(j).getJumlah());
                            pivot.get(i).getStrippivot().get(j).setJumlahasli(pivot.get(i-1).getStrippivot().get(j).getJumlahasli()+ pivot.get(i).getStrippivot().get(j).getJumlahasli());

                    }
                    Pivot_Table piv = new Pivot_Table();
                    piv.setNo_bon(namabonlama);
                    piv.setUmurnormal(pivot.get(i).getUmurnormal());
                    piv.setStrippivot(pivot.get(i).getStrippivot());
                    piv.setUmurasli(pivot.get(i).getUmurasli());
                    pivotbaru.add(piv); 
                    count = 0;
                    break;
                }
                //kalau setelahnya nama ne gak sama
                if (!pivot.get(i+1).getNo_bon().equals(namabonlama)) {
                        if (i > 0) {
                            //kalau penanda sudah 0 berarti tidak perlu set pivot yg lama supaya tidak terjadi penambahan jumlah sebelumnya
                            if (count != 0) {
                                for (int j = 0; j < pivot.get(i).getStrippivot().size(); j++) {
                                    tampungan = pivot.get(i-1).getStrippivot().get(j).getJumlah() + pivot.get(i).getStrippivot().get(j).getJumlah();
                                    pivot.get(i).getStrippivot().get(j).setJumlah(pivot.get(i-1).getStrippivot().get(j).getJumlah() + pivot.get(i).getStrippivot().get(j).getJumlah());
                                    pivot.get(i).getStrippivot().get(j).setJumlahasli(pivot.get(i-1).getStrippivot().get(j).getJumlahasli()+ pivot.get(i).getStrippivot().get(j).getJumlahasli());

                                }       
                            }
                        }
                        Pivot_Table piv = new Pivot_Table();
                        piv.setNo_bon(namabonlama);
                        piv.setUmurnormal(pivot.get(i).getUmurnormal());
                        piv.setStrippivot(pivot.get(i).getStrippivot());
                        piv.setUmurasli(pivot.get(i).getUmurasli());
                        pivotbaru.add(piv); 
                        namabonlama = pivot.get(i+1).getNo_bon();
                        count = 0;
                }else{
    //                System.out.println("Nama sama" + pivot.get(i).getNo_bon());                
                    if (count==0) {
    //                    System.out.println("Nama sama Iterasi Pertama");
                        count++;
                        continue;
                    }else{                    
    //                    System.out.println("Nama sama Iterasi berikut");
                        for (int j = 0; j < pivot.get(i).getStrippivot().size(); j++) {
                            tampungan = pivot.get(i-1).getStrippivot().get(j).getJumlah() + pivot.get(i).getStrippivot().get(j).getJumlah();
                            pivot.get(i).getStrippivot().get(j).setJumlah(pivot.get(i-1).getStrippivot().get(j).getJumlah() + pivot.get(i).getStrippivot().get(j).getJumlah());
                            pivot.get(i).getStrippivot().get(j).setJumlahasli(pivot.get(i-1).getStrippivot().get(j).getJumlahasli()+ pivot.get(i).getStrippivot().get(j).getJumlahasli());

                        }
                        count++;
                    }
                    continue;
                }
                /*
                if (namabonlama.equals(pivot.get(i).getNo_bon())) {
    //                piv.setStrippivot(pivot.get(i).getStrippivot());
                    System.out.println("Wow jenenge podo ki le " + namabonlama );
                    for (int j = 0; j < pivot.get(i).getStrippivot().size(); j++) {
                        //tampungan = tampungan + (float)Integer.parseInt(pivot.get(i).getStrippivot().get(j).getJumlah());
                        pivot.get(i).getStrippivot().get(j).setJumlah(Float.toString(Float.parseFloat(pivot.get(i-1).getStrippivot().get(j).getJumlah()) + Float.parseFloat(pivot.get(i).getStrippivot().get(j).getJumlah())));
                    }
                    namabonlama = pivot.get(i).getNo_bon();
                    if (!pivot.get(i+1).getNo_bon().equals(namabonlama)) {
                        Pivot_Table piv = new Pivot_Table();
                        piv.setNo_bon(namabonlama);
                        piv.setUmurnormal(pivot.get(i).getUmurnormal());
                        piv.setStrippivot(pivot.get(i).getStrippivot());
                        pivotbaru.add(piv);
                        continue;
                    }else{       
                        //namabonlama = pivot.get(i).getNo_bon();
                        System.out.println("terusno");
                        continue;
                    }
                }else{
                    Pivot_Table piv = new Pivot_Table();
                    piv.setNo_bon(pivot.get(i).getNo_bon());
                    piv.setUmurnormal(pivot.get(i).getUmurnormal());
                    piv.setStrippivot(pivot.get(i).getStrippivot());                
                    pivotbaru.add(piv);
                    namabonlama = pivot.get(i).getNo_bon();

                }*/
            }

            /*
            for (int i = 0; i < stripcopy.size(); i++) {
                System.out.println("Stripcop ke -"+i+"  "+stripcopy.get(i));
                System.out.println("Stripcop Nama ke -"+i+"  "+stripcopy.get(i).getNamastrip());
                System.out.println("Stripcop Jumlah ke -"+i+"  "+stripcopy.get(i).getJumlah());
            }

            for (int i = 0; i < pivot.size(); i++) {
                System.out.println("Pivot ke -"+i+"  "+pivot.get(i));

                for (int j = 0; j < pivot.get(i).getStrip().size(); j++) {
                    System.out.println("Pivot Nama ke -"+j+"  "+pivot.get(i).getStrip().get(j).getNamastrip());
                    System.out.println("Pivot Jumlah ke -"+j+"  "+pivot.get(i).getStrip().get(j).getJumlah());
                }
            }*/




            //utk jenis awal

            /*
            stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();        
            rs= stat.executeQuery("select kd_strip, kel_jns from strip");
            strip = new ArrayList();
            stripcopy = new ArrayList();
            while(rs.next()){  
                Strip stripkita = new Strip();
                stripkita.setNamastrip(rs.getString(1));
                stripkita.setKel_jns(rs.getString(2));
                strip.add(stripkita);
                stripkita = null;
            }        

            for (int i = 0; i < pivotbaru.size(); i++) {
                for (int j = 0; j < pivotbaru.get(i).getStrippivot().size(); j++) {
                    for (int k = 0; k < strip.size(); k++) {
                        if (pivotbaru.get(i).getStrippivot().get(j).getNamastrip().equals(strip.get(k).getNamastrip())) {
                            pivotbaru.get(i).getStrippivot().get(j).setKel_jns(strip.get(k).getKel_jns());
                        }
                    }
                }
            }

            for (int i = 0; i < pivotbaru.size(); i++) {
                Double d = new Double(0);
                for (int j = 0; j < pivotbaru.get(i).getStrippivot().size(); j++) {
                  d = d + pivotbaru.get(i).getStrippivot().get(j).getJumlahasli();
                }
                pivotbaru.get(i).setJumlahtotal(d);
            }


            session.put("PIVOT", pivotbaru);
            session.put("SELPERIOD", periodselect);

            Double jumlahA = new Double(0.0);
            Double jumlahW = new Double(0.0);
            Double jumlahP = new Double(0.0);
            Double jumlahS = new Double(0.0);        
            pivotjenis = new ArrayList();

            for (int i = 0; i < pivotbaru.size(); i++) {
                PivotJenis_Table pivku = new PivotJenis_Table();
                jumlahA = 0.0;
                jumlahW = 0.0;
                jumlahP = 0.0;
                jumlahS = 0.0;

                for (int j = 0; j < pivotbaru.get(i).getStrippivot().size(); j++) {
                    if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("A")) {
                        jumlahA = jumlahA + pivotbaru.get(i).getStrippivot().get(j).getJumlah();
                    }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("W")) {
                        jumlahW = jumlahW + pivotbaru.get(i).getStrippivot().get(j).getJumlah();
                    }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("P")) {
                        jumlahP = jumlahP + pivotbaru.get(i).getStrippivot().get(j).getJumlah();
                    }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("S")) {
                        jumlahS = jumlahS + pivotbaru.get(i).getStrippivot().get(j).getJumlah();
                    }

    //                if (j==pivotbaru.get(i).getStrippivot().size()-1) {
    //                    if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals(pivotbaru.get(i).getStrippivot().get(j-1).getKel_jns())) {
    //
    //                    }else{
    //                        if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("A")) {
    //                            jumlahA = jumlahA + pivotbaru.get(i).getStrippivot().get(j).getJumlah();
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("W")) {
    //                            jumlahW = jumlahW + pivotbaru.get(i).getStrippivot().get(j).getJumlah();
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("P")) {
    //                            jumlahP = jumlahP + pivotbaru.get(i).getStrippivot().get(j).getJumlah();
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("S")) {
    //                            jumlahS = jumlahS + pivotbaru.get(i).getStrippivot().get(j).getJumlah();
    //                        }
    //                    }
    //                    
    //                }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals(pivotbaru.get(i).getStrippivot().get(j+1).getKel_jns())) {
    //                    if (j>0 && !pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals(pivotbaru.get(i).getStrippivot().get(j-1).getKel_jns())) {
    //                        if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("A")) {
    //                           jumlahA = jumlahA + pivotbaru.get(i).getStrippivot().get(j).getJumlah();                                         
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("W")) {
    //                           jumlahW = jumlahW + pivotbaru.get(i).getStrippivot().get(j).getJumlah();                                             
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("P")) {
    //                           jumlahP = jumlahP + pivotbaru.get(i).getStrippivot().get(j).getJumlah();                     
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("S")) {
    //                           jumlahS = jumlahS + pivotbaru.get(i).getStrippivot().get(j).getJumlah();                                             
    //                        }
    //                    }else{
    //                        if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("A")) {
    //                           jumlahA = jumlahA + pivotbaru.get(i).getStrippivot().get(j+1).getJumlah();                                         
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("W")) {
    //                           jumlahW = jumlahW + pivotbaru.get(i).getStrippivot().get(j+1).getJumlah();                                             
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("P")) {
    //                           jumlahP = jumlahP + pivotbaru.get(i).getStrippivot().get(j+1).getJumlah();                     
    //                        }else if (pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals("S")) {
    //                           jumlahS = jumlahS + pivotbaru.get(i).getStrippivot().get(j+1).getJumlah();                                             
    //                        }
    //                    }
    //                }else if(!pivotbaru.get(i).getStrippivot().get(j).getKel_jns().equals(pivotbaru.get(i).getStrippivot().get(j+1).getKel_jns())){
    //                    if (pivotbaru.get(i).getStrippivot().get(j+1).getKel_jns().equals("A")) {
    //                       jumlahA = jumlahA + pivotbaru.get(i).getStrippivot().get(j+1).getJumlah();                                         
    //                    }else if (pivotbaru.get(i).getStrippivot().get(j+1).getKel_jns().equals("W")) {
    //                       jumlahW = jumlahW + pivotbaru.get(i).getStrippivot().get(j+1).getJumlah();                                             
    //                    }else if (pivotbaru.get(i).getStrippivot().get(j+1).getKel_jns().equals("P")) {
    //                       jumlahP = jumlahP + pivotbaru.get(i).getStrippivot().get(j+1).getJumlah();                     
    //                    }else if (pivotbaru.get(i).getStrippivot().get(j+1).getKel_jns().equals("S")) {
    //                       jumlahS = jumlahS + pivotbaru.get(i).getStrippivot().get(j+1).getJumlah();                                             
    //                    }
    //                }

                }
                jenis =  new ArrayList();
                for (int k = 0; k < 4; k++) {
                    Jenis jeniskita = new Jenis();
                    if (k==0) {
                        jeniskita.setKel_jns("A");
                        jeniskita.setJumlah(jumlahA);                            
                    }else if (k==1) {                    
                        jeniskita.setKel_jns("W");
                        jeniskita.setJumlah(jumlahW);
                    }else if (k==2) {                    
                        jeniskita.setKel_jns("P");
                        jeniskita.setJumlah(jumlahP);
                    }else{
                        jeniskita.setKel_jns("S");
                        jeniskita.setJumlah(jumlahS);                        
                    }
                    jenis.add(jeniskita);
                    jeniskita = null;
                }

                pivku.setNo_bon(pivotbaru.get(i).getNo_bon());
                pivku.setUmurnormal(pivotbaru.get(i).getUmurnormal());
                pivku.setUmurasli(pivotbaru.get(i).getUmurasli());
                pivku.setJenispivot(jenis);

                pivotjenis.add(pivku);
            }
            //Tabel ke Jenis Pivot
            /*
            stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();        
            rs= stat.executeQuery("select kel_jns from strip");
            jenis = new ArrayList();
            jeniscopy = new ArrayList();

            while(rs.next()){  
                Jenis jeniskita = new Jenis();
                jeniskita.setKel_jns(rs.getString(1));
                jenis.add(jeniskita);
                jeniskita = null;
            }        

            //sampe sini
            pivotjenis = new ArrayList();

            for (int i = 0; i < listnormal.size(); i++) {
                PivotJenis_Table pivottabel = new PivotJenis_Table();
                jeniscopy = new ArrayList();
                for (int j = 0; j < jenis.size(); j++) {
                    Jenis jenisini = new Jenis();
                    if (listnormal.get(i).getKel_jns().equals(jenis.get(j).getKel_jns())) {
                        if (listnormal.get(i).getKel_jns().equals(listnormal.get(i-1).getKel_jns())) {                        
                            jenisini.setKel_jns(jenis.get(j).getKel_jns());
                            jenisini.setJumlah(listnormal.get(i).getJumlahnormal()+listnormal.get(i-1).getJumlahnormal());
                            jenisini.setJumlahasli(listnormal.get(i).getJumlah()+listnormal.get(i-1).getJumlah());
                        }else{

                        }                    
                    }else{
                        jenisini.setKel_jns(jenis.get(j).getKel_jns());
                        jenisini.setJumlah(0.0);
                        jenisini.setJumlahasli(0.0);
                    }
                    jeniscopy.add(jenisini);
                }

                pivottabel.setNo_bon(listnormal.get(i).getNo_bon());
                pivottabel.setUmurnormal(listnormal.get(i).getUmurnormal());
                pivottabel.setUmurasli(listnormal.get(i).getUmur());
                pivottabel.setJenispivot(jeniscopy);
                pivotjenis.add(pivottabel);
            }


            String namabonjenislama = pivotjenis.get(0).getNo_bon();
            Double tampunganjenis = 0.0;

            pivotjenisbaru = new ArrayList();
            int countjenis = 0;
            //ada kesalahan disini

            for (int i = 0; i < pivotjenis.size(); i++) {
                //kalau i di posisi row terakir
                if (i == pivotjenis.size()-1) {
                    for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                            pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah()+ pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                            pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());
                    }
                    PivotJenis_Table piv = new PivotJenis_Table();
                    piv.setNo_bon(namabonlama);
                    piv.setUmurnormal(pivotjenis.get(i).getUmurnormal());
                    piv.setJenispivot(pivotjenis.get(i).getJenispivot());
                    piv.setUmurasli(pivotjenis.get(i).getUmurasli());
                    pivotjenisbaru.add(piv); 
                    count = 0;
                    break;
                }
                //kalau setelahnya nama ne gak sama
                if (!pivotjenis.get(i+1).getNo_bon().equals(namabonjenislama)) {
                        if (i > 0) {
                            //kalau penanda sudah 0 berarti tidak perlu set pivot yg lama supaya tidak terjadi penambahan jumlah sebelumnya
                            if (count != 0) {
                                for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                                    tampungan = pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah();
                                    pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                                    pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivot.get(i).getStrippivot().get(j).getJumlahasli());

                                }       
                            }
                        }
                        PivotJenis_Table piv = new PivotJenis_Table();
                        piv.setNo_bon(namabonjenislama);
                        piv.setUmurnormal(pivotjenis.get(i).getUmurnormal());
                        piv.setJenispivot(pivotjenis.get(i).getJenispivot());
                        piv.setUmurasli(pivotjenis.get(i).getUmurasli());
                        pivotjenisbaru.add(piv); 
                        namabonjenislama = pivotjenis.get(i+1).getNo_bon();
                        count = 0;
                }else{
    //                System.out.println("Nama sama" + pivot.get(i).getNo_bon());                
                    if (count==0) {
    //                    System.out.println("Nama sama Iterasi Pertama");
                        count++;
                        continue;
                    }else{                    
    //                    System.out.println("Nama sama Iterasi berikut");
                        for (int j = 0; j < pivot.get(i).getStrippivot().size(); j++) {
                            tampungan = pivot.get(i-1).getStrippivot().get(j).getJumlah() + pivot.get(i).getStrippivot().get(j).getJumlah();
                            pivot.get(i).getStrippivot().get(j).setJumlah(pivot.get(i-1).getStrippivot().get(j).getJumlah() + pivot.get(i).getStrippivot().get(j).getJumlah());
                            pivot.get(i).getStrippivot().get(j).setJumlahasli(pivot.get(i-1).getStrippivot().get(j).getJumlahasli()+ pivot.get(i).getStrippivot().get(j).getJumlahasli());

                        }
                        count++;
                    }
                    continue;
                }
            }*/
            session.put("PIVOT", pivotbaru);
            session.put("SELPERIOD", periodselect);
            session.put("MODEL", modelfakta);
        
            SerializedObject se1 = new SerializedObject();
            DBConnector connector1 = new DBConnector();
            Connection conn1 = connector1.getConn();
            conn1.setAutoCommit(false);
    //        se.write(conn1, namaperiod, pivotjenis, Integer.parseInt(periodselect));
//            se1.write(conn1, namaperiod, pivotbaru, Integer.parseInt(periodselect),Integer.parseInt(modelfakta));        
            conn1.commit();
        
        }else{
            listjenisnormal = new ArrayList<>((ArrayList<BonJenis_Transaction>)o3);
        
            stat = new HiveConnector("localhost", "hive", "", "amigo").getConn().createStatement();        
            /*rs= stat.executeQuery("select kel_jns from strip");
            jenis = new ArrayList();            
            jeniscopy = new ArrayList();
            while(rs.next()){  
                Jenis stripkita = new Jenis();
                stripkita.setKel_jns(rs.getString(1));
                jenis.add(stripkita);
                stripkita = null;
            } */       
            jenis = new ArrayList();            
            jeniscopy = new ArrayList();
            for (int i = 0; i < 4; i++) {
                Jenis stripkita = new Jenis();
                if (i==0) {                    
                    stripkita.setKel_jns("A");
                }else if(i==1){                    
                    stripkita.setKel_jns("W");
                }else if(i==2){                    
                    stripkita.setKel_jns("P");
                }else if(i==3){
                    stripkita.setKel_jns("S");
                }
                jenis.add(stripkita);
                stripkita = null;
            }
            
            pivotjenis = new ArrayList();
            
            for (int i = 0; i < listjenisnormal.size(); i++) {
                PivotJenis_Table pivottabel = new PivotJenis_Table();
                jeniscopy = new ArrayList();
                for (int j = 0; j < jenis.size(); j++) {
                    Jenis stripini = new Jenis();
                    if (listjenisnormal.get(i).getKel_jns().equals(jenis.get(j).getKel_jns())) {
                        stripini.setKel_jns(jenis.get(j).getKel_jns());
                        stripini.setJumlah(listjenisnormal.get(i).getJumlahnormal());
                        stripini.setJumlahasli(listjenisnormal.get(i).getJumlah());
                    }else{
                        stripini.setKel_jns(jenis.get(j).getKel_jns());
                        stripini.setJumlah(0.0);
                        stripini.setJumlahasli(0.0);
                    }
                    jeniscopy.add(stripini);
                }
                
                pivottabel.setNo_bon(listjenisnormal.get(i).getNo_bon());
                pivottabel.setUmurnormal(listjenisnormal.get(i).getUmurnormal());
                pivottabel.setUmurasli(listjenisnormal.get(i).getUmur());
                pivottabel.setJenispivot(jeniscopy);
                pivotjenis.add(pivottabel);
            }

            System.out.println("Total jenis" + jenis.size());
            String namabonlama = pivotjenis.get(0).getNo_bon();
            Double tampungan = 0.0;

            pivotjenisbaru = new ArrayList();
            int count = 0;


            for (int i = 0; i < pivotjenis.size(); i++) {
                //kalau i di posisi row terakir
                if (i == pivotjenis.size()-1) {
                    for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                            pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah()+ pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                            pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());
                    }
                    PivotJenis_Table piv = new PivotJenis_Table();
                    piv.setNo_bon(namabonlama);
                    piv.setUmurnormal(pivotjenis.get(i).getUmurnormal());
                    piv.setJenispivot(pivotjenis.get(i).getJenispivot());
                    piv.setUmurasli(pivotjenis.get(i).getUmurasli());
                    pivotjenisbaru.add(piv); 
                    count = 0;
                    break;
                }
                //kalau setelahnya nama ne gak sama
                if (!pivotjenis.get(i+1).getNo_bon().equals(namabonlama)) {
                        if (i > 0) {
                            //kalau penanda sudah 0 berarti tidak perlu set pivot yg lama supaya tidak terjadi penambahan jumlah sebelumnya
                            if (count != 0) {
                                for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                                    tampungan = pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah();
                                    pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                                    pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());

                                }       
                            }
                        }
                        PivotJenis_Table piv = new PivotJenis_Table();
                        piv.setNo_bon(namabonlama);
                        piv.setUmurnormal(pivotjenis.get(i).getUmurnormal());
                        piv.setJenispivot(pivotjenis.get(i).getJenispivot());
                        piv.setUmurasli(pivotjenis.get(i).getUmurasli());
                        pivotjenisbaru.add(piv); 
                        namabonlama = pivotjenis.get(i+1).getNo_bon();
                        count = 0;
                }else{
    //                System.out.println("Nama sama" + pivot.get(i).getNo_bon());                
                    if (count==0) {
    //                    System.out.println("Nama sama Iterasi Pertama");
                        count++;
                        continue;
                    }else{                    
    //                    System.out.println("Nama sama Iterasi berikut");
                        for (int j = 0; j < pivotjenis.get(i).getJenispivot().size(); j++) {
                            tampungan = pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah();
                            pivotjenis.get(i).getJenispivot().get(j).setJumlah(pivotjenis.get(i-1).getJenispivot().get(j).getJumlah() + pivotjenis.get(i).getJenispivot().get(j).getJumlah());
                            pivotjenis.get(i).getJenispivot().get(j).setJumlahasli(pivotjenis.get(i-1).getJenispivot().get(j).getJumlahasli()+ pivotjenis.get(i).getJenispivot().get(j).getJumlahasli());

                        }
                        count++;
                    }
                    continue;
                }

            }

            session.put("PIVOT", pivotjenisbaru);
            session.put("SELPERIOD", periodselect);            
            session.put("MODEL", modelfakta);

            SerializedObject se1 = new SerializedObject();
            DBConnector connector1 = new DBConnector();
            Connection conn1 = connector1.getConn();
            conn1.setAutoCommit(false);
    //        se.write(conn1, namaperiod, pivotjenis, Integer.parseInt(periodselect));
//            se1.write(conn1, namaperiod, pivotjenisbaru, Integer.parseInt(periodselect),Integer.parseInt(modelfakta));        
            conn1.commit();

        }
        
        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }
    
    public Double cosineSimilarity(Double[] docVector1, Double[] docVector2) {
        Double dotProduct = 0.0;
        Double magnitude1 = 0.0;
        Double magnitude2 = 0.0;
        Double cosineSimilarity = 0.0;

        for (int i = 0; i < docVector1.length; i++) //docVector1 and docVector2 must be of same length
        {
            dotProduct += docVector1[i] * docVector2[i];  //a.b
            magnitude1 += Math.pow(docVector1[i], 2);  //(a^2)
            magnitude2 += Math.pow(docVector2[i], 2); //(b^2)
        }

        magnitude1 = Math.sqrt(magnitude1);//sqrt(a^2)
        magnitude2 = Math.sqrt(magnitude2);//sqrt(b^2)

        if (magnitude1 != 0.0 | magnitude2 != 0.0) {
            cosineSimilarity = dotProduct / (magnitude1 * magnitude2);
        } else {
            return 0.0;
        }
        return cosineSimilarity;
    }
    
    public String manual(){
        try{  

        this.displaySelectPivot();
        this.getDataPivot();

        SerializedObject se = new SerializedObject();
        DBConnector connector = new DBConnector();
        Connection conn = connector.getConn();
        conn.setAutoCommit(false);
        Object o = se.read(conn, Integer.parseInt(periodselect));
        conn.commit();

        pivotjenisbaru = new ArrayList<>((ArrayList<PivotJenis_Table>)o);
        pivotjenismanual = new ArrayList();
        clustermanual = new ArrayList<>();
        //cari min-max normal utk kelompok umur
        Double minim = Double.MAX_VALUE;
        Double maksim = Double.NEGATIVE_INFINITY;
        
        for (int i = 0; i < pivotjenisbaru.size(); i++) {
            if (minim > pivotjenisbaru.get(i).getUmurnormal()) {
                minim = pivotjenisbaru.get(i).getUmurnormal();
            }
            if (maksim < pivotjenisbaru.get(i).getUmurnormal()) {
                maksim = pivotjenisbaru.get(i).getUmurnormal();
            }            
            for (int j = 0; j < pivotjenisbaru.get(i).getJenispivot().size(); j++) {
                if (pivotjenisbaru.get(i).getJenispivot().get(j).getJumlah().equals(0.0)){
                    pivotjenisbaru.get(i).getJenispivot().get(j).setJumlah(Double.NEGATIVE_INFINITY);
                }
            }
        }
        
        Double diff = maksim - minim;
        
        ArrayList<PivotJenis_Table> golsatuanak = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golsatuwanita = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golsatupria = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golsatusepatu = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golduaanak = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golduawanita = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golduapria = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golduasepatu = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> goltigaanak = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> goltigawanita = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> goltigapria = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> goltigasepatu = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golempatanak = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golempatwanita = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golempatpria = new ArrayList<PivotJenis_Table>();
        ArrayList<PivotJenis_Table> golempatsepatu = new ArrayList<PivotJenis_Table>();
        
        if (potongselect.equals("1")) {
            //kalautigagolumur
            Double bagitiga = diff / 3;
            Double akirgolsatu = minim + bagitiga;
            Double akirgoldua = akirgolsatu + bagitiga;
            Double akirgoltiga = akirgoldua + bagitiga;
            info = new ArrayList<>();
            Informasi inf = new Informasi();
            inf.setGolsatu(akirgolsatu);
            inf.setGoldua(akirgoldua);
            inf.setGoltiga(akirgoltiga);
            inf.setMaks(maksim);
            inf.setMins(minim);
            info.add(inf);
            for (int i = 0; i < pivotjenisbaru.size(); i++) {
            
                //cari masuk golongan mana utk golumur 3 kelompok
               if (pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah()>= pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah()  ) {
                   if (pivotjenisbaru.get(i).getUmurnormal() >= minim && pivotjenisbaru.get(i).getUmurnormal() <= akirgolsatu) {
                       golsatuanak.add(pivotjenisbaru.get(i));
                   }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgolsatu && pivotjenisbaru.get(i).getUmurnormal() <= akirgoldua) {
                       golduaanak.add(pivotjenisbaru.get(i));                    
                   }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoldua && pivotjenisbaru.get(i).getUmurnormal() <= akirgoltiga) {
                       goltigaanak.add(pivotjenisbaru.get(i));                    
                   }
               }else if (pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah()  ) {
                   if (pivotjenisbaru.get(i).getUmurnormal() >= minim && pivotjenisbaru.get(i).getUmurnormal() <= akirgolsatu) {
                       golsatuwanita.add(pivotjenisbaru.get(i));                    
                   }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgolsatu && pivotjenisbaru.get(i).getUmurnormal() <= akirgoldua) {
                       golduawanita.add(pivotjenisbaru.get(i));                                        
                   }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoldua && pivotjenisbaru.get(i).getUmurnormal() <= akirgoltiga) {
                       goltigawanita.add(pivotjenisbaru.get(i));                                        
                   }
               }else if (pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah()  ) {
                   if (pivotjenisbaru.get(i).getUmurnormal() >= minim && pivotjenisbaru.get(i).getUmurnormal() <= akirgolsatu) {
                       golsatupria.add(pivotjenisbaru.get(i));                                        
                   }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgolsatu && pivotjenisbaru.get(i).getUmurnormal() <= akirgoldua) {
                       golduapria.add(pivotjenisbaru.get(i));                                                            
                   }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoldua && pivotjenisbaru.get(i).getUmurnormal() <= akirgoltiga) {
                       goltigapria.add(pivotjenisbaru.get(i));                                                            
                   }
               }else if (pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah()  ) {
                   if (pivotjenisbaru.get(i).getUmurnormal() >= minim && pivotjenisbaru.get(i).getUmurnormal() <= akirgolsatu) {
                       golsatusepatu.add(pivotjenisbaru.get(i));                                                            
                   }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgolsatu && pivotjenisbaru.get(i).getUmurnormal() <= akirgoldua) {
                       golduasepatu.add(pivotjenisbaru.get(i));                                                            
                   }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoldua && pivotjenisbaru.get(i).getUmurnormal() <= akirgoltiga) {
                       goltigasepatu.add(pivotjenisbaru.get(i));                                                            
                   }
               }

            }
            
            //kalau 3 gol
            for (int i = 0; i < 12; i++) {
                ClusterManual cm = new ClusterManual(); 
                if (i==0) {
                    cm.setNamacluster("Golongan Satu Anak");
                    cm.setPivotjenis(golsatuanak);
                }else if (i==1) {
                    cm.setNamacluster("Golongan Satu Wanita");
                    cm.setPivotjenis(golsatuwanita);
                }else if (i==2) {
                    cm.setNamacluster("Golongan Satu Pria");
                    cm.setPivotjenis(golsatupria);                
                }else if (i==3) {
                    cm.setNamacluster("Golongan Satu Sepatu");
                    cm.setPivotjenis(golsatusepatu);
                }else if (i==4) {
                    cm.setNamacluster("Golongan Dua Anak");
                    cm.setPivotjenis(golduaanak);
                }else if (i==5) {
                    cm.setNamacluster("Golongan Dua Wanita");
                    cm.setPivotjenis(golduawanita);
                }else if (i==6) {
                    cm.setNamacluster("Golongan Dua Pria");
                    cm.setPivotjenis(golduapria);
                }else if (i==7) {
                    cm.setNamacluster("Golongan Dua Sepatu");
                    cm.setPivotjenis(golduasepatu);
                }else if (i==8) {
                    cm.setNamacluster("Golongan Tiga Anak");
                    cm.setPivotjenis(goltigaanak);
                }else if (i==9) {
                    cm.setNamacluster("Golongan Tiga Wanita");
                    cm.setPivotjenis(goltigawanita);
                }else if (i==10) {
                    cm.setNamacluster("Golongan Tiga Pria");
                    cm.setPivotjenis(goltigapria);
                }else if (i==11) {
                    cm.setNamacluster("Golongan Tiga Sepatu");
                    cm.setPivotjenis(goltigasepatu);
                }
                clustermanual.add(cm);
            }
        }else{
            //kalauempat
            Double bagiempat = diff / 4;
            Double akirgolsatu = minim + bagiempat;
            Double akirgoldua = akirgolsatu + bagiempat;
            Double akirgoltiga = akirgoldua + bagiempat;
            Double akirgolempat = akirgoltiga + bagiempat;
            info = new ArrayList<>();
            Informasi inf = new Informasi();
            inf.setGolsatu(akirgolsatu);
            inf.setGoldua(akirgoldua);
            inf.setGoltiga(akirgoltiga);
            inf.setGolempat(akirgolempat);
            inf.setMaks(maksim);
            inf.setMins(minim);
            info.add(inf);
            for (int i = 0; i < pivotjenisbaru.size(); i++) {
            
                //utk 4 golongan
                if (pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah()>= pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah()  ) {
                    if (pivotjenisbaru.get(i).getUmurnormal() >= minim && pivotjenisbaru.get(i).getUmurnormal() <= akirgolsatu) {
                        golsatuanak.add(pivotjenisbaru.get(i));
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgolsatu && pivotjenisbaru.get(i).getUmurnormal() <= akirgoldua) {
                        golduaanak.add(pivotjenisbaru.get(i));                    
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoldua && pivotjenisbaru.get(i).getUmurnormal() <= akirgoltiga) {
                        goltigaanak.add(pivotjenisbaru.get(i));                    
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoltiga && pivotjenisbaru.get(i).getUmurnormal() <= akirgolempat) {
                        golempatanak.add(pivotjenisbaru.get(i));                    
                    }
                }else if (pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah()  ) {
                    if (pivotjenisbaru.get(i).getUmurnormal() >= minim && pivotjenisbaru.get(i).getUmurnormal() <= akirgolsatu) {
                        golsatuwanita.add(pivotjenisbaru.get(i));                    
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgolsatu && pivotjenisbaru.get(i).getUmurnormal() <= akirgoldua) {
                        golduawanita.add(pivotjenisbaru.get(i));                                        
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoldua && pivotjenisbaru.get(i).getUmurnormal() <= akirgoltiga) {
                        goltigawanita.add(pivotjenisbaru.get(i));                                        
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoltiga && pivotjenisbaru.get(i).getUmurnormal() <= akirgolempat) {
                        golempatwanita.add(pivotjenisbaru.get(i));                    
                    }
                }else if (pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah()  ) {
                    if (pivotjenisbaru.get(i).getUmurnormal() >= minim && pivotjenisbaru.get(i).getUmurnormal() <= akirgolsatu) {
                        golsatupria.add(pivotjenisbaru.get(i));                                        
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgolsatu && pivotjenisbaru.get(i).getUmurnormal() <= akirgoldua) {
                        golduapria.add(pivotjenisbaru.get(i));                                                            
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoldua && pivotjenisbaru.get(i).getUmurnormal() <= akirgoltiga) {
                        goltigapria.add(pivotjenisbaru.get(i));                                                            
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoltiga && pivotjenisbaru.get(i).getUmurnormal() <= akirgolempat) {
                        golempatpria.add(pivotjenisbaru.get(i));                    
                    }
                }else if (pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(1).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(2).getJumlah() && pivotjenisbaru.get(i).getJenispivot().get(3).getJumlah() >= pivotjenisbaru.get(i).getJenispivot().get(0).getJumlah()  ) {
                    if (pivotjenisbaru.get(i).getUmurnormal() >= minim && pivotjenisbaru.get(i).getUmurnormal() <= akirgolsatu) {
                        golsatusepatu.add(pivotjenisbaru.get(i));                                                            
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgolsatu && pivotjenisbaru.get(i).getUmurnormal() <= akirgoldua) {
                        golduasepatu.add(pivotjenisbaru.get(i));                                                            
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoldua && pivotjenisbaru.get(i).getUmurnormal() <= akirgoltiga) {
                        goltigasepatu.add(pivotjenisbaru.get(i));                                                            
                    }else if (pivotjenisbaru.get(i).getUmurnormal() > akirgoltiga && pivotjenisbaru.get(i).getUmurnormal() <= akirgolempat) {
                        golempatsepatu.add(pivotjenisbaru.get(i));                    
                    }
               }
            }
            
            for (int i = 0; i < 16; i++) {
                ClusterManual cm = new ClusterManual(); 
                if (i==0) {
                    cm.setNamacluster("Golongan Satu Anak");
                    cm.setPivotjenis(golsatuanak);
                }else if (i==1) {
                    cm.setNamacluster("Golongan Satu Wanita");
                    cm.setPivotjenis(golsatuwanita);
                }else if (i==2) {
                    cm.setNamacluster("Golongan Satu Pria");
                    cm.setPivotjenis(golsatupria);                
                }else if (i==3) {
                    cm.setNamacluster("Golongan Satu Sepatu");
                    cm.setPivotjenis(golsatusepatu);
                }else if (i==4) {
                    cm.setNamacluster("Golongan Dua Anak");
                    cm.setPivotjenis(golduaanak);
                }else if (i==5) {
                    cm.setNamacluster("Golongan Dua Wanita");
                    cm.setPivotjenis(golduawanita);
                }else if (i==6) {
                    cm.setNamacluster("Golongan Dua Pria");
                    cm.setPivotjenis(golduapria);
                }else if (i==7) {
                    cm.setNamacluster("Golongan Dua Sepatu");
                    cm.setPivotjenis(golduasepatu);
                }else if (i==8) {
                    cm.setNamacluster("Golongan Tiga Anak");
                    cm.setPivotjenis(goltigaanak);
                }else if (i==9) {
                    cm.setNamacluster("Golongan Tiga Wanita");
                    cm.setPivotjenis(goltigawanita);
                }else if (i==10) {
                    cm.setNamacluster("Golongan Tiga Pria");
                    cm.setPivotjenis(goltigapria);
                }else if (i==11) {
                    cm.setNamacluster("Golongan Tiga Sepatu");
                    cm.setPivotjenis(goltigasepatu);
                }else if (i==12) {
                    cm.setNamacluster("Golongan Empat Anak");
                    cm.setPivotjenis(golempatanak);
                }else if (i==13) {
                    cm.setNamacluster("Golongan Empat Wanita");
                    cm.setPivotjenis(golempatwanita);
                }else if (i==14) {
                    cm.setNamacluster("Golongan Empat Pria");
                    cm.setPivotjenis(golempatpria);
                }else if (i==15) {
                    cm.setNamacluster("Golongan Empat Sepatu");
                    cm.setPivotjenis(golempatsepatu);
                }
                clustermanual.add(cm);
            }

        }
//        int total = 0;
//        for (int i = 0; i < 12; i++) {
//            total = total + clustermanual.get(i).getPivotjenis().size();
//        }            
//        System.out.println("Manual total" + total);

        }catch(Exception e){
             e.printStackTrace();
        }
        return SUCCESS;
    }

    /**
     * @return the infonotatunai
     */
    public ArrayList<InfoNotaTunai> getInfonotatunai() {
        return infonotatunai;
    }

    /**
     * @param infonotatunai the infonotatunai to set
     */
    public void setInfonotatunai(ArrayList<InfoNotaTunai> infonotatunai) {
        this.infonotatunai = infonotatunai;
    }

    /**
     * @return the infonotabon
     */
    public ArrayList<InfoNotaBon> getInfonotabon() {
        return infonotabon;
    }

    /**
     * @param infonotabon the infonotabon to set
     */
    public void setInfonotabon(ArrayList<InfoNotaBon> infonotabon) {
        this.infonotabon = infonotabon;
    }

    /**
     * @return the infopelanggan
     */
    public ArrayList<InfoPelanggan> getInfopelanggan() {
        return infopelanggan;
    }

    /**
     * @param infopelanggan the infopelanggan to set
     */
    public void setInfopelanggan(ArrayList<InfoPelanggan> infopelanggan) {
        this.infopelanggan = infopelanggan;
    }

    /**
     * @return the infostrip
     */
    public ArrayList<InfoStrip> getInfostrip() {
        return infostrip;
    }

    /**
     * @param infostrip the infostrip to set
     */
    public void setInfostrip(ArrayList<InfoStrip> infostrip) {
        this.infostrip = infostrip;
    }

    /**
     * @return the infotoko
     */
    public ArrayList<InfoToko> getInfotoko() {
        return infotoko;
    }

    /**
     * @param infotoko the infotoko to set
     */
    public void setInfotoko(ArrayList<InfoToko> infotoko) {
        this.infotoko = infotoko;
    }

    /**
     * @return the potongselect
     */
    public String getPotongselect() {
        return potongselect;
    }

    /**
     * @param potongselect the potongselect to set
     */
    public void setPotongselect(String potongselect) {
        this.potongselect = potongselect;
    }

    /**
     * @return the idUsr
     */
    public String getIdUsr() {
        return idUsr;
    }

    /**
     * @param idUsr the idUsr to set
     */
    public void setIdUsr(String idUsr) {
        this.idUsr = idUsr;
    }

    /**
     * @return the pivotjenisbaru2
     */
    public ArrayList<PivotJenis_Table> getPivotjenisbaru2() {
        return pivotjenisbaru2;
    }

    /**
     * @param pivotjenisbaru2 the pivotjenisbaru2 to set
     */
    public void setPivotjenisbaru2(ArrayList<PivotJenis_Table> pivotjenisbaru2) {
        this.pivotjenisbaru2 = pivotjenisbaru2;
    }
    
    
    
}
