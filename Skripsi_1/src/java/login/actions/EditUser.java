/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.actions;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author hadoop
 */
public class EditUser {
    private String userId;
    private String userEdit;
    private String passwordEdit;
    private String jenis_role;
    

    public String editUser() throws SQLException{
        
            DBConnector connector = new DBConnector();
            Connection conn = connector.getConn();
            String sql = "UPDATE `users` SET username = ?, password= ?, role = ? WHERE id = ? ";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, this.getUserEdit());
            ps.setString(2, this.getPasswordEdit());
            if (this.getJenis_role().equals("A")) {
            ps.setInt(3, 0);
            }else{
            ps.setInt(3, 1);
            }
            ps.setString(4, this.getUserId());
            ResultSet rs = ps.executeQuery();
            return SUCCESS;
        
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the userEdit
     */
    public String getUserEdit() {
        return userEdit;
    }

    /**
     * @param userEdit the userEdit to set
     */
    public void setUserEdit(String userEdit) {
        this.userEdit = userEdit;
    }

    /**
     * @return the passwordEdit
     */
    public String getPasswordEdit() {
        return passwordEdit;
    }

    /**
     * @param passwordEdit the passwordEdit to set
     */
    public void setPasswordEdit(String passwordEdit) {
        this.passwordEdit = passwordEdit;
    }

    /**
     * @return the jenis_role
     */
    public String getJenis_role() {
        return jenis_role;
    }

    /**
     * @param jenis_role the jenis_role to set
     */
    public void setJenis_role(String jenis_role) {
        this.jenis_role = jenis_role;
    }
}
