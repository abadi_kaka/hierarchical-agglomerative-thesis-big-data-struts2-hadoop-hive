/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mabadi.algorithm;

import java.io.Serializable;

/**
 *
 * @author hadoop
 */
public class DistanceMap implements Serializable {
    
    private Double distances = null;
    
    private String namaBon = null;
    
    public void setNamaBon(String namaBon){
        this.namaBon = namaBon;
    }
    
    public String getNamaBon(){
        return this.namaBon;
    }
    
    public void setDistances(Double distances){
        this.distances = distances;
    }
    
    public Double getDistances(){
        return this.distances;
    }
}
