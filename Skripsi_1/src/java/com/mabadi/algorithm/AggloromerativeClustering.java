/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mabadi.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import models.Strip;

/**
 *
 * @author hadoop
 */

public class AggloromerativeClustering implements Serializable{

    private ClusteringType type;
    private Double distanceBetweenThem = null;
    private double[][] distanceMap = null;
    
    public AggloromerativeClustering(ClusteringType type) {
       this.type = type;
    }
    
    public Cluster induceClusters(String[] names, double[][] distances, double[] ages, double[] totalAnak, double[] totalWanita, double[] totalPria, double[] totalSepatu) {
      //Collection<ClusteringItem> items
      distanceBetweenThem = null;
      distanceMap = null;
      // Create initial clusters of size 1, one for each item
      List<Cluster> currentClusters = new ArrayList<Cluster>();
      //HashMap tes = new HashMap();

      /*for (ClusteringItem item : items) {
         currentClusters.add(item);
      }*/
      double[][] distMap = new double[names.length][names.length];
      distMap = distances;
      //distanceMap = distances;
      
      Integer number = new Integer(0);
      Double counter = Double.MAX_VALUE;
      //int number = 0;
      for (int i = 0; i < names.length; i++) {
        for (int j = 0; j < names.length; j++) {
            if (distances[i][j] < counter) {
                counter = distances[i][j]; 
            }
        }
      }
      

      for (String nama : names){
          ClusteringItem item = new ClusteringItem(){};
          item.setName(nama);
          item.setPosisi(number);
          item.setUmur(ages[number]);
          item.setTotalAnak(totalAnak[number]);
          item.setTotalWanita(totalWanita[number]);
          item.setTotalPria(totalPria[number]);
          item.setTotalSepatu(totalSepatu[number]);
          item.setClusterMinDist(counter);
          currentClusters.add(item);
          //tes.put(item, nama);
          number++;
      }
      
     
      int i =1;
      
      // Keep merging the two closest clusters, until there is only one cluster left
      while (currentClusters.size() > 1) {
         ClusterPair closestClusters = findClosestClusters(currentClusters, distMap);

         //System.out.println("Closest clusters are "+ closestClusters.first +" and " + closestClusters.second);

//         System.out.println("Iterasi dalam while : " + currentClusters.size());
          
         Cluster newCluster = new SuperCluster(closestClusters.first, closestClusters.second);
         newCluster.setName(Integer.toString(i));
         newCluster.setClusterMinDist(distanceBetweenThem);
         currentClusters.add(newCluster);
         //tes.put(closestClusters.first, newCluster);
         //tes.put(closestClusters.second, newCluster);
         currentClusters.remove(closestClusters.first);
         currentClusters.remove(closestClusters.second);
//         System.out.println("Distance-" + i + " : " + distanceBetweenThem);
         i++;
         distanceBetweenThem = 0.0;
      }
     
      return currentClusters.get(0);
   }

   private ClusterPair findClosestClusters(List<Cluster> currentClusters, double[][] distMap) {
      Cluster firstCandidate = null;
      Cluster secondCandidate = null;
      distanceBetweenThem = Double.MAX_VALUE;
      for (int i = 0; i < currentClusters.size(); i++) {
         Cluster clusterI = currentClusters.get(i);
         for (int j = i + 1; j < currentClusters.size(); j++) {
            Cluster clusterJ = currentClusters.get(j);
            double distance = getDistanceBetween(clusterI, clusterJ, distMap);
            if (distance <= distanceBetweenThem) {
               firstCandidate = clusterI;
               secondCandidate = clusterJ;
               distanceBetweenThem = distance;
            }
         }
//         System.out.println("Iterasi dalam closest : " + i);
      }
      return new ClusterPair(firstCandidate, secondCandidate);
   }
   
   private double getDistanceBetween(Cluster i, Cluster j, double[][] distMap) {
       switch(type){
          case COMPLETE_LINKAGE:
            return completeLinkage(i, j, distMap);
          case SINGLE_LINKAGE:
            return singleLinkage(i, j, distMap);
       }
       return Double.MAX_VALUE;
   }
   
   private Double singleLinkage(Cluster i, Cluster j, double[][] distMap){
       double shortdistance =  Double.MAX_VALUE;
       //double shortdistance =  distanceBetweenThem;
       for (ClusteringItem item : i.getItems()) {
         int posisiI = item.getPosisi();
         for (ClusteringItem otherItem : j.getItems()) {            
            int posisiJ = otherItem.getPosisi();
            double distance = distMap[posisiI][posisiJ];
            if (distance < shortdistance) {
                shortdistance = distance;
            }
         }
        }
       return shortdistance;
   }
   
   private Double completeLinkage(Cluster i, Cluster j, double[][] distMap){
       double longdistance =  Double.NEGATIVE_INFINITY;
       for (ClusteringItem item : i.getItems()) {
         int posisiI = item.getPosisi();
         for (ClusteringItem otherItem : j.getItems()) {            
            int posisiJ = otherItem.getPosisi();
            double distance = distMap[posisiI][posisiJ];
            if (distance > longdistance) {
                longdistance = distance;
            }
         }
        }
       return longdistance;
   }
   
    private static class ClusterPair {

       public Cluster first;

       public Cluster second;

       public ClusterPair(Cluster first, Cluster second) {
          this.first = first;
          this.second = second;
       }

    }

   public static enum ClusteringType {
      SINGLE_LINKAGE, COMPLETE_LINKAGE;
   }
    
}
