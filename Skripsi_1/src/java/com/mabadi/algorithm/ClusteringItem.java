/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mabadi.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a cluster containing only one item
 * 
 * @author hadoop
 */

public abstract class ClusteringItem extends Cluster implements Serializable{
   
   private String namaItem = null; 
   private Integer posisi = null;
   private Double clusterMinDist = null;
   private Double umur = null;
   private Double totalAnak = null;
   private Double totalWanita = null;
   private Double totalPria = null;
   private Double totalSepatu = null;
      
//private Double[][] distMap = null;
    //private List<DistanceMap> distMap = null;
   
   @Override
   public void setName(String name){
       this.setNamaItem(name);
   }
   
   @Override
   public String getName(){
       return this.getNamaItem();
   }
   
   
   public void setUmur(Double umur){
       this.umur = umur;
   }
   
   public Double getUmur(){
       return this.umur;
   }
   
   public void setTotalAnak(Double totalAnak){
       this.totalAnak = totalAnak;
   }
   
   public Double getTotalAnak(){
       return this.totalAnak;
   }
   public void setTotalWanita(Double totalWanita){
       this.totalWanita = totalWanita;
   }
   
   public Double getTotalWanita(){
       return this.totalWanita;
   }
   public void setTotalPria(Double totalPria){
       this.totalPria = totalPria;
   }
   
   public Double getTotalPria(){
       return this.totalPria;
   }
   public void setTotalSepatu(Double totalSepatu){
       this.totalSepatu = totalSepatu;
   }
   
   public Double getTotalSepatu(){
       return this.totalSepatu;
   }
   
   @Override
   public List<ClusteringItem> getItems() {
      ArrayList<ClusteringItem> result = new ArrayList<>();
      result.add(this);
      return result;
   }

   /** {@inheritDoc} */
   @Override
   public List<Cluster> getChildren() {
      return new ArrayList<>();
   }
   @Override
    public Double getClusterMinDist(){
       return this.clusterMinDist;
    }

    @Override
    public void setClusterMinDist(Double clusterMinDist){
       this.clusterMinDist = clusterMinDist;
    }
   
   public void setNamaItem(String namaItem){
       this.namaItem = namaItem;
   }
   
   public String getNamaItem(){
       return this.namaItem;
   }
   
//   public void setDistMap(Double[][] distMap){
//       this.distMap = distMap;
//   }
   
//   public Double[][] getDistMap(){
//       return this.distMap;
//   }
   
   public void setPosisi(Integer posisi){
       this.posisi = posisi;
   }
   
   public Integer getPosisi(){
       return this.posisi;
   }
   
   /*public void setDistMap(List<DistanceMap> distMap){
       this.distMap = distMap;
   }
   
   public List<DistanceMap> getDistMap(){
       return this.distMap;
   }*/
   
}
