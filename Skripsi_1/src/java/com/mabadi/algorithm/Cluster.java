/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mabadi.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hadoop
 */

public abstract class Cluster implements Serializable{
   public abstract List<ClusteringItem> getItems();

   public abstract List<Cluster> getChildren();
   
   public abstract String getName();
   public abstract void setName(String name);
   public abstract Double getClusterMinDist();
   public abstract void setClusterMinDist(Double clusterMinDist);
   
   private List<ClusteringItem> items = null;
   private List<Cluster> children = null; 
   private String name = null;
   //private Double clusterMinDist = null;
   private boolean isLeaf = false;
   private boolean isNode = false;
   
//   public List<ClusteringItem> getItems() {
//      ArrayList<ClusteringItem> result = new ArrayList<>();
//      result.add((ClusteringItem) this);
//      return this.items;
//   } 
//   
//   public List<Cluster> getSubClusters() {
//      return this.subClusters;
//   }
   public void setIsLeaf(boolean isLeaf){
       this.isLeaf = isLeaf;
   }
   
   public void setIsNode(boolean isNode){
       this.isNode = isNode;
   }
   
   public boolean getIsLeaf(){
       return this.getChildren().size() == 0;
   }
   
   public boolean getIsNode(){
       return this.getChildren().size() != 0;
   }
   
   
   
   
   //public abstract List<ClusteringItem> getItems();

   //public abstract List<Cluster> getSubClusters();
   
   
}
