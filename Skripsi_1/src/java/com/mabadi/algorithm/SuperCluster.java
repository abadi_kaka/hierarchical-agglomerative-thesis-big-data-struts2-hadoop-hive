/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mabadi.algorithm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hadoop
 */
public class SuperCluster extends Cluster implements Serializable{
    private List<Cluster> children = new ArrayList<>();
    private Double clusterMinDist = null;
    private String name = null;
   public SuperCluster() {

   }
   
   public SuperCluster(Cluster first, Cluster second) {
      children.add(first);
      children.add(second);
   }
   
   public void setName(String name){
       String nama = "Cluster #" + name;
       this.name = nama;
   
   }
   
   public String getName(){
       return this.name;
   }
    public List<ClusteringItem> getItems() {
      List<ClusteringItem> result = new ArrayList<>();
      for (Cluster subCluster : children) {
         result.addAll(subCluster.getItems());
      }
      return result;
    }
    
    public List<Cluster> getChildren() {
      return children;
    }
      
    @Override
    public Double getClusterMinDist(){
       return this.clusterMinDist;
    }

    @Override
    public void setClusterMinDist(Double clusterMinDist){
       this.clusterMinDist = clusterMinDist;
    }

    
}
