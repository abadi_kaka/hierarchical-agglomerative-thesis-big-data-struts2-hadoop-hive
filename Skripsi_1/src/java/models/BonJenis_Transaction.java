/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author hadoop
 */
public class BonJenis_Transaction implements Serializable{

    private String no_bon;
    private String tanggal;
    private String kd_customer;
    private Double umur;
    private String id_toko;
    private String kel_jns;
    private Double jumlah;
    private Double jumlahnormal;
    private Double umurnormal;
    
    public String getNo_bon() {
        return no_bon;
    }
    public void setNo_bon(String no_bon) {
        this.no_bon = no_bon;
    }
    public String getTanggal() {
        return tanggal;
    }
    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
    public String getKd_customer() {
        return kd_customer;
    }
    public void setKd_customer(String kd_customer) {
        this.kd_customer = kd_customer;
    }
    public Double getUmur() {
        return umur;
    }
    public void setUmur(Double umur) {
        this.umur = umur;
    }
    public Double getUmurnormal() {
        return umurnormal;
    }
    public void setUmurnormal(Double umurnormal) {
        this.umurnormal = umurnormal;
    }
    public String getId_toko() {
        return id_toko;
    }
    public void setId_toko(String id_toko) {
        this.id_toko = id_toko;
    }
    public String getKel_jns() {
        return kel_jns;
    }
    public void setKel_jns(String kel_jns) {
        this.kel_jns = kel_jns;
    }
    public Double getJumlah() {
        return jumlah;
    }
    public void setJumlah(Double jumlah) {
        this.jumlah = jumlah;
    }
    
    public Double getJumlahnormal() {
        return jumlahnormal;
    }
    public void setJumlahnormal(Double jumlahnormal) {
        this.jumlahnormal = jumlahnormal;
    }
    

}
