/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import weka.gui.streams.SerialInstanceListener;

/**
 *
 * @author hadoop
 */
public class Bon_Distance implements Serializable{
    private String no_bon;
    private Double jumlahdistance;
    
    public String getNo_bon(){
        return no_bon;
    }
    
    public void setNo_bon(String no_bon){
        this.no_bon = no_bon;
    }
    
    public Double getJumlahdistance(){
        return jumlahdistance;
    }
    
    public void setJumlahdistance(Double jumlahdistance){
        this.jumlahdistance = jumlahdistance;
    }
}
