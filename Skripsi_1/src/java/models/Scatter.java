/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author hadoop
 */
public class Scatter {
    
    private String no_bon;
    private Double umur;
    private ArrayList<Strip> strip;
    private String jenis_kelamin;
    

    public String getJenis_kelamin(){
        return jenis_kelamin;
    }
    public void setJenis_kelamin(String jenis_kelamin){
        this.jenis_kelamin = jenis_kelamin;
    }

    public Double getUmur(){
        return umur;
    }
    
    public ArrayList<Strip> getStrip(){
        return strip;
    }
    
    
    public String getNo_bon(){
        return no_bon;
    }
    public void setNo_bon(String no_bon){
        this.no_bon = no_bon;
    }
    
    public void setUmur(Double umur){
        this.umur = umur;
    }
    
    public void setStrip(ArrayList<Strip> strip){
        this.strip = strip;
    }
}
