/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author hadoop
 */
public class ClusterManual implements Serializable{
    
    private String namacluster;
    private ArrayList<PivotJenis_Table> pivotjenis;
    
    public String getNamacluster(){
        return namacluster;
    }
    
    public void setNamacluster(String namacluster){
        this.namacluster = namacluster;
    }
    
    public ArrayList<PivotJenis_Table> getPivotjenis() {
        return pivotjenis;
    }
    
    public void setPivotjenis(ArrayList<PivotJenis_Table> pivotjenis) {
        this.pivotjenis = pivotjenis;
    }
}
