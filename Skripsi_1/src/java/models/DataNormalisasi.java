/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author hadoop
 */
public class DataNormalisasi {
    private String id;
    private String nama;
    private Object object;
    private String modelfakta;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    
    public Object getObject() {
        return object;
    }
    public void setObject(Object object) {
        this.object = object;
    }
    public String getModelfakta() {
        return modelfakta;
    }
    public void setModelfakta(String modelfakta) {
        this.modelfakta = modelfakta;
    }
}
