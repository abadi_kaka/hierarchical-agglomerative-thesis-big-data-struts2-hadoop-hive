/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author hadoop
 */
public class InfoPelanggan {
    private String kd_customer;
    private String nama;
    private String tgl_lahir;
    private String jns_kelamin;

    /**
     * @return the kd_customer
     */
    public String getKd_customer() {
        return kd_customer;
    }

    /**
     * @param kd_customer the kd_customer to set
     */
    public void setKd_customer(String kd_customer) {
        this.kd_customer = kd_customer;
    }

    /**
     * @return the nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama the nama to set
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return the tgl_lahir
     */
    public String getTgl_lahir() {
        return tgl_lahir;
    }

    /**
     * @param tgl_lahir the tgl_lahir to set
     */
    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    /**
     * @return the jns_kelamin
     */
    public String getJns_kelamin() {
        return jns_kelamin;
    }

    /**
     * @param jns_kelamin the jns_kelamin to set
     */
    public void setJns_kelamin(String jns_kelamin) {
        this.jns_kelamin = jns_kelamin;
    }
    
}
