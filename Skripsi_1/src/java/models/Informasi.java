/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author hadoop
 */
public class Informasi implements Serializable{
    private Double maks;
    private Double mins;
    private Double golsatu;
    private Double goldua;
    private Double goltiga;
    private Double golempat;
    
    public Double getGolsatu() {
            return golsatu;
    }
    public Double getGoldua() {
            return goldua;
    }
    public Double getGoltiga() {
            return goltiga;
    }
    public Double getGolempat() {
            return golempat;
    }
    public Double getMaks() {
            return maks;
    }
    public Double getMins() {
            return mins;
    }
    
    public void setGolsatu(Double golsatu) {
            this.golsatu = golsatu;
    }
    public void setGoldua(Double goldua) {
            this.goldua = goldua;
    }
    public void setGoltiga(Double goltiga) {
            this.goltiga = goltiga;
    }
    public void setGolempat(Double golempat) {
            this.golempat = golempat;
    }
    public void setMaks(Double maks) {
            this.maks = maks;
    }
    
    public void setMins(Double mins) {
            this.mins = mins;
    }
}
