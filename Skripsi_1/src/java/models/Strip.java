/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;

/**
 *
 * @author hadoop
 */
public class Strip implements Serializable{
    
    public String namastrip;
    public Double jumlah;
    public Double jumlahasli;
    public String kel_jns;
    
    public String getNamastrip() {
        return namastrip;
    }
    public void setNamastrip(String namastrip) {
        this.namastrip = namastrip;
    }
    public String getKel_jns() {
        return kel_jns;
    }
    public void setKel_jns(String kel_jns) {
        this.kel_jns = kel_jns;
    }
    public Double getJumlah() {
        return jumlah;
    }
    public void setJumlah(Double jumlah) {
        this.jumlah = jumlah;
    }
    public Double getJumlahasli() {
        return jumlahasli;
    }
    public void setJumlahasli(Double jumlahasli) {
        this.jumlahasli = jumlahasli;
    }
    
}
