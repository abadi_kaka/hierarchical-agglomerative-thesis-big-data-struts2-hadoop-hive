/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author hadoop
 */
public class InfoNotaTunai {
    private String no_tunai;
    private String tanggal;
    private String kd_strip;
    private String kd_lokasi;
    private String kd_customer;
    private String kel_jenis;
    private String umur;
    private String jumlah;

    /**
     * @return the no_tunai
     */
    public String getNo_tunai() {
        return no_tunai;
    }

    /**
     * @param no_tunai the no_tunai to set
     */
    public void setNo_tunai(String no_tunai) {
        this.no_tunai = no_tunai;
    }

    /**
     * @return the tanggal
     */
    public String getTanggal() {
        return tanggal;
    }

    /**
     * @param tanggal the tanggal to set
     */
    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    /**
     * @return the kd_strip
     */
    public String getKd_strip() {
        return kd_strip;
    }

    /**
     * @param kd_strip the kd_strip to set
     */
    public void setKd_strip(String kd_strip) {
        this.kd_strip = kd_strip;
    }

    /**
     * @return the kd_lokasi
     */
    public String getKd_lokasi() {
        return kd_lokasi;
    }

    /**
     * @param kd_lokasi the kd_lokasi to set
     */
    public void setKd_lokasi(String kd_lokasi) {
        this.kd_lokasi = kd_lokasi;
    }

    /**
     * @return the kd_customer
     */
    public String getKd_customer() {
        return kd_customer;
    }

    /**
     * @param kd_customer the kd_customer to set
     */
    public void setKd_customer(String kd_customer) {
        this.kd_customer = kd_customer;
    }

    /**
     * @return the kel_jenis
     */
    public String getKel_jenis() {
        return kel_jenis;
    }

    /**
     * @param kel_jenis the kel_jenis to set
     */
    public void setKel_jenis(String kel_jenis) {
        this.kel_jenis = kel_jenis;
    }

    /**
     * @return the umur
     */
    public String getUmur() {
        return umur;
    }

    /**
     * @param umur the umur to set
     */
    public void setUmur(String umur) {
        this.umur = umur;
    }

    /**
     * @return the jumlah
     */
    public String getJumlah() {
        return jumlah;
    }

    /**
     * @param jumlah the jumlah to set
     */
    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
}
