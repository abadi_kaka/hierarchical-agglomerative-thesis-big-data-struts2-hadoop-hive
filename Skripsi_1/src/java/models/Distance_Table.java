/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author hadoop
 */
public class Distance_Table implements Serializable{
    
    private String no_bon;
    private ArrayList<Bon_Distance> distancebon;
    //private Double umurasli;
        
    //private String jumlahnormal;
    
    public ArrayList<Bon_Distance> getDistancebon() {
        return distancebon;
    }
    public void setDistancebon(ArrayList<Bon_Distance> distancebon) {
        this.distancebon = distancebon;
    }
    
    public String getNo_bon(){
        return no_bon;
    }
    public void setNo_bon(String no_bon){
        this.no_bon = no_bon;
    }
    
//    public Double getUmurasli(){
//        return umurasli;
//    }
//    public void setUmurasli(Double umurasli){
//        this.umurasli = umurasli;
//    }
}
