/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author hadoop
 */
public class InfoStrip {
    private String kd_strip;
    private String nama_strip;
    private String kel_jenis;

    /**
     * @return the kd_strip
     */
    public String getKd_strip() {
        return kd_strip;
    }

    /**
     * @param kd_strip the kd_strip to set
     */
    public void setKd_strip(String kd_strip) {
        this.kd_strip = kd_strip;
    }

    /**
     * @return the nama_strip
     */
    public String getNama_strip() {
        return nama_strip;
    }

    /**
     * @param nama_strip the nama_strip to set
     */
    public void setNama_strip(String nama_strip) {
        this.nama_strip = nama_strip;
    }

    /**
     * @return the kel_jenis
     */
    public String getKel_jenis() {
        return kel_jenis;
    }

    /**
     * @param kel_jenis the kel_jenis to set
     */
    public void setKel_jenis(String kel_jenis) {
        this.kel_jenis = kel_jenis;
    }
    
    
}
