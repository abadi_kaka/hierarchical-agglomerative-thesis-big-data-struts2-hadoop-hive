/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author hadoop
 */
public class Pivot_Table implements Serializable{
    private String no_bon;
    private ArrayList<Strip> strippivot;
    //private String jumlahnormal;
    private Double umurnormal;
    private Double umurasli;
    private Double jumlahtotal;
    
    public String getNo_bon() {
        return no_bon;
    }
    public void setNo_bon(String no_bon) {
        this.no_bon = no_bon;
    }
    public ArrayList<Strip> getStrippivot() {
        return strippivot;
    }
    public void setStrippivot(ArrayList<Strip> strippivot) {
        this.strippivot = strippivot;
    }
    public Double getUmurnormal() {
        return umurnormal;
    }
    public void setUmurnormal(Double umurnormal) {
        this.umurnormal = umurnormal;
    }
    public Double getUmurasli() {
        return umurasli;
    }
    public void setUmurasli(Double umurasli) {
        this.umurasli = umurasli;
    }
        
    public Double getJumlahtotal() {
        return jumlahtotal;
    }
    public void setJumlahtotal(Double jumlahtotal) {
        this.jumlahtotal = jumlahtotal;
    }
//    
//    public String getJumlahnormal() {
//        return jumlahnormal;
//    }
//    public void setJumlahnormal(String jumlahnormal) {
//        this.jumlahnormal = jumlahnormal;
//    }
}
