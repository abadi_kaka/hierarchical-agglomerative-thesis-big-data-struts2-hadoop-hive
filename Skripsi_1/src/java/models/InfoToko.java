/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author hadoop
 */
public class InfoToko {
    private String kd_lokasi;
    private String nama_toko;

    /**
     * @return the kd_lokasi
     */
    public String getKd_lokasi() {
        return kd_lokasi;
    }

    /**
     * @param kd_lokasi the kd_lokasi to set
     */
    public void setKd_lokasi(String kd_lokasi) {
        this.kd_lokasi = kd_lokasi;
    }

    /**
     * @return the nam_toko
     */
    public String getNama_toko() {
        return nama_toko;
    }

    /**
     * @param nam_toko the nam_toko to set
     */
    public void setNama_toko(String nama_toko) {
        this.nama_toko = nama_toko;
    }
    
}
